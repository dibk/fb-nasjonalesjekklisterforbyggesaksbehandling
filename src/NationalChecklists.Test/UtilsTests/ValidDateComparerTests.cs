﻿using NationalChecklists.Utils;
using System;
using Xunit;

namespace NationalChecklists.Test.UtilsTests
{
    public class ValidDateComparerTests
    {
        [Fact]
        public void DateBeforeFromAndToDates_returnFalse()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            var dateBefore = new DateTime(2022, 1, 1);

            var isValid = ValidDateComparer.IsValidOnDate(dateBefore, validFromDate, validToDate);

            Assert.False(isValid);
        }

        [Fact]
        public void DateBetweenFromAndToDates_returnTrue()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            var dateBetweenFromAndTo = new DateTime(2022, 7, 10);

            var isValid = ValidDateComparer.IsValidOnDate(dateBetweenFromAndTo, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void DateAfterFromAndToDates_returnFalse()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            var dateBetweenFromAndTo = new DateTime(2023, 7, 10);

            var isValid = ValidDateComparer.IsValidOnDate(dateBetweenFromAndTo, validFromDate, validToDate);

            Assert.False(isValid);
        }

        [Fact]
        public void DateOnFromDate_returnTrue()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            DateTime dateOnFromDate = validFromDate.Value;

            var isValid = ValidDateComparer.IsValidOnDate(dateOnFromDate, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void DateOnToDate_returnTrue()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            DateTime dateOnToDate = validToDate.Value;

            var isValid = ValidDateComparer.IsValidOnDate(dateOnToDate, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void DateOnToDateWithTimeComponent_returnTrue()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = new DateTime(2023, 6, 30);

            var dateOnToDateWithTimeComponent = new DateTime(2023, 6, 30).AddHours(23).AddMinutes(59).AddSeconds(59);

            var isValid = ValidDateComparer.IsValidOnDate(dateOnToDateWithTimeComponent, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void DateBeforeToDate_FromDateIsNull_returnTrue()
        {
            DateTime? validFromDate = null;
            DateTime? validToDate = new DateTime(2023, 6, 30);

            DateTime dateBeforeToDate = new DateTime(2022, 7, 10);

            var isValid = ValidDateComparer.IsValidOnDate(dateBeforeToDate, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void DateBeforeFromDate_ToDateIsNull_returnFalse()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = null;

            var dateBeforeFromDate = new DateTime(2022, 1, 1);

            var isValid = ValidDateComparer.IsValidOnDate(dateBeforeFromDate, validFromDate, validToDate);

            Assert.False(isValid);
        }

        [Fact]
        public void DateAfterFromDate_ToDateIsNull_returnTrue()
        {
            DateTime? validFromDate = new DateTime(2022, 7, 1);
            DateTime? validToDate = null;

            var dateAfterFromDate = new DateTime(2022, 7, 10);

            var isValid = ValidDateComparer.IsValidOnDate(dateAfterFromDate, validFromDate, validToDate);

            Assert.True(isValid);
        }

        [Fact]
        public void SomeDate_FromAndToDateIsNull_returnTrue()
        {
            DateTime? validFromDate = null;
            DateTime? validToDate = null;

            var someDate = new DateTime(2022, 7, 10);

            var isValid = ValidDateComparer.IsValidOnDate(someDate, validFromDate, validToDate);

            Assert.True(isValid);
        }

    }
}
