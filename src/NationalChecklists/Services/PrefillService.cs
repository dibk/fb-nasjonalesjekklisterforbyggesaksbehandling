using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NationalChecklists.Configuration;
using NationalChecklists.Middleware.Models.Prefill;
using NationalChecklists.Models;
using NationalChecklists.ViewModels;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace NationalChecklists.Services
{
    public interface IPrefillService
    {
        Task<List<PrefillActivityAction>> GetPrefill(List<Activity> currentActivities, PrefillChecklistInput validationResult);

        Task<List<PrefillActivityAction>> GetPrefill(string processCategory, List<string> enterpriseTerms, string form);

        void AddorUpdateValidationIds(List<ActivityActionValidationRulesViewModel> activities);

        List<ActivityAndValidationIdsViewModel> GetActivitiesAndValidationRuleIds(ValidationRuleFilter varidationRuleFilter);

        Task<List<PrefillActivityActionDemo>> GetPrefillDemo(string processCategory);

        void DeleteActionValidationRuleById(int actionValidationRuleId);

        Task<PrefillInformationApiModel> GetPrefillInformation(string processCategory);

        Task<List<Activity>> GetActivitiesAndActions(string processCategory);
    }

    public class PrefillService : IPrefillService
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IChecklistFilterService _checklistFilterService;
        private readonly IMetadataService _metadataService;
        private readonly ILogger<PrefillService> _logger;
        private readonly IOptions<CheckpointConfig> _options;

        public PrefillService(ApplicationDbContext dbContext, IChecklistFilterService checklistFilterService, IMetadataService metadataService, ILogger<PrefillService> logger, IOptions<CheckpointConfig> options)
        {
            _dbContext = dbContext;
            _checklistFilterService = checklistFilterService;
            _metadataService = metadataService;
            _logger = logger;
            _options = options;
        }

        public async Task<List<PrefillActivityAction>> GetPrefill(List<Activity> currentActivities, PrefillChecklistInput validationResult)
        {
            var currentActionValidationRules = GetCurrentActionValidationRules(validationResult.ProcessCategory, validationResult.EnterpriseTerms);
            var subActivities = new List<Activity>();
            var prefillActivities = new List<PrefillActivityAction>();

            var dataFormatId = !string.IsNullOrEmpty(validationResult.DataFormatId) ? validationResult.DataFormatId + "." : string.Empty;
            var dataFormatVersion = !string.IsNullOrEmpty(validationResult.DataFormatVersion) ? validationResult.DataFormatVersion + "." : string.Empty;

            foreach (var actionValidationRules in currentActionValidationRules)
            {
                var missingValidationRuleExecution = !actionValidationRules.ValidationRules.Any(rule =>
                    validationResult.ExecutedValidations.Exists(v =>
                        dataFormatId + dataFormatVersion + rule.ValidationRuleId == v));

                if (!missingValidationRuleExecution)
                {
                    var prefillActivity = new PrefillActivityAction
                    {
                        ChecklistReference = actionValidationRules.Action.Activity.ReferenceId,
                        ChecklistQuestion = actionValidationRules.Action.Activity.Name,
                        YesNo = !actionValidationRules.Action.ActionValue,
                        SupportingDataValidationRuleId = actionValidationRules.ValidationRules.Select(rule => dataFormatId + dataFormatVersion + rule.ValidationRuleId).ToList()
                    };

                    var hasErrorOrWarning = actionValidationRules.ValidationRules.Any(rule =>
                        validationResult.Errors.Exists(error =>
                            dataFormatId + dataFormatVersion + rule.ValidationRuleId == error) ||
                        validationResult.Warnings.Exists(warning =>
                            dataFormatId + dataFormatVersion + rule.ValidationRuleId == warning));

                    if (hasErrorOrWarning)
                    {
                        prefillActivity.YesNo = actionValidationRules.Action.ActionValue;
                    }

                    if (actionValidationRules.Action.Activity.ParentReferenceId != null)
                    {
                        subActivities.Add(actionValidationRules.Action.Activity);
                    }

                    prefillActivities.Add(prefillActivity);
                }
            }

            // Fjern prefull fra underpunkt om de ikke skal vises ut fra hovedpunktets utfall - Temp løsning så lenge vi bruker valideringsrapport fra alfa.
            foreach (var subActivity in subActivities)
            {
                var parentPrefill = prefillActivities.FirstOrDefault(a => a.ChecklistReference.Equals(subActivity.ParentReferenceId));

                if (parentPrefill == null)
                {
                    // Fjernes fordi parent ikke er preutfylt
                    var subActivityPrefill = prefillActivities.FirstOrDefault(a => a.ChecklistReference.Equals(subActivity.ReferenceId));
                    prefillActivities.Remove(subActivityPrefill);
                }
                else
                {
                    var parentActivity = _dbContext.Activity
                        .Include(i => i.Actions)
                        .FirstOrDefault(a => a.Id == subActivity.Activity_Id);

                    // Sjekk om utfall på parent fører til visning av underpunkt..
                    if (parentActivity.Actions.Exists(a => a.ActionValue != parentPrefill.YesNo))
                    {
                        // Fjernes fordi parent sitt utfall ikke fører til visning av underpunkt.
                        var subActivityPrefill = prefillActivities.FirstOrDefault(a => a.ChecklistReference.Equals(subActivity.ReferenceId));
                        prefillActivities.Remove(subActivityPrefill);
                    }
                }
            }

            return prefillActivities;
        }

        public async Task<List<PrefillActivityAction>> GetPrefill(string processCategory, List<string> enterpriseTerms, string form)
        {
            var prefillActivities = new List<PrefillActivityAction>();
            var metadataTypePrefillXpath = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillXpathMetadataTypeName);
            var metadataTypePrefillSelectedCodeValue = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillSelectedCodeValueMetadataTypeName);
            //var metadataTypePrefillHasValue = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillHasValueMetadataTypeName);
            ArgumentNullException.ThrowIfNull(metadataTypePrefillXpath, nameof(metadataTypePrefillXpath));

            _logger.LogInformation("Get activities with selected metadataType");
            var currentActivities = GetCurrentCheckpoint(processCategory, metadataTypePrefillXpath.Id);
            if (currentActivities == null)
            {
                _logger.LogInformation($"currentActivities is null");
                return prefillActivities;
            }

            _logger.LogInformation($"Number of activities found {currentActivities?.Count}");

            if (enterpriseTerms != null && enterpriseTerms.Any())
            {
                _logger.LogInformation($"Filter on enterpriseTerms {string.Join(", ", enterpriseTerms)}");

                // Filtrere på tiltakstyper - Gå igjennom listen med currentCheckpoints og finn de elementene som inneholder verdier fra inputparameteret "enterpriseTerms" i currentCheckponint elementet sin List<string>enterpriseTersm
                currentActivities = currentActivities.Where(c =>
                    c.EnterpriseTerms.Select(e => e.Code).Intersect(enterpriseTerms).Any()
                ).ToList();
                _logger.LogInformation($"Number of activities found after filter on enterpriseTerms {currentActivities.Count}");
            }

            try
            {
                // Erstatt escape-sekvenser med opprinnelige tegn i XML-strengen
                var xmlString = UnescapeSpecialCharacters(form);

                _logger.LogInformation($"Parse form xml-string to xDocument");
                var xmlDoc = XDocument.Parse(xmlString);

                foreach (var activityToPrefill in currentActivities)
                {
                    // hent prefillXpath
                    var xpath = activityToPrefill.Metadata
                        .FirstOrDefault(m => m.MetadataTypeId == metadataTypePrefillXpath.Id)
                        ?.MetadataItem?.Value;

                    var validValues = new List<string>();

                    _logger.LogInformation("Activity to prefill: {activityToPrefill}, from xpath: {xpath}",
                        new { activityToPrefill.ReferenceId, xpath });

                    // Hent verdi fra xmlDoc ved hjelp av xpath.
                    if (string.IsNullOrEmpty(xpath))
                    {
                        _logger.LogInformation("Missing xpath on activity to prefill: {activityToPrefill}",
                            new { activityToPrefill.ReferenceId });
                        continue;
                    }

                    // local-name()-funksjonen i XPath sammen med en wildcard (*) for å matche alle elementer uavhengig av prefix.
                    var updatedXPath = string.Join("/", xpath.Split('/').Select(node => $"*[local-name()='{node}']"));

                    var result = xmlDoc.XPathSelectElements(updatedXPath);

                    bool? prefillYesNo = null;
                    foreach (var element in result)
                    {
                        if (string.IsNullOrEmpty(element.Value)) continue;

                        // Sjekk om prefill skal settes ut fra true/false
                        if (bool.TryParse(element.Value, out bool yesNo))
                        {
                            prefillYesNo = yesNo;
                        }
                        // Sjekk om prefill skal settes ut fra utfylt felt i søknaden
                        else
                        {
                            if (metadataTypePrefillSelectedCodeValue == null) continue;

                            var prefillSelectedCodeValue = activityToPrefill.Metadata.Where(m =>
                                m.MetadataTypeId == metadataTypePrefillSelectedCodeValue.Id);
                            //var prefillHasValueMetadata = activityToPrefill.Metadata.Exists(m => m.MetadataTypeId == metadataTypePrefillHasValue.Id);

                            if (prefillSelectedCodeValue.Any())
                            {
                                var currentValues = prefillSelectedCodeValue.Select(m => m.MetadataItem.Value).ToList();
                                prefillYesNo = currentValues.Contains(element.Value);
                                validValues = currentValues;

                                if (prefillYesNo == true) break;
                            }
                            //// Sjekk om prefill skal settes ut fra rett kodeverdi
                            //else if (prefillHasValueMetadata)
                            //{
                            //    prefillYesNo = !string.IsNullOrWhiteSpace(result);
                            //}
                        }
                    }

                    if (prefillYesNo == null) continue;
                    var prefillActivity = new PrefillActivityAction
                    {
                        ChecklistReference = activityToPrefill.ReferenceId,
                        ChecklistQuestion = activityToPrefill.Name,
                        YesNo = prefillYesNo.Value,
                        SupportingDataXpathField = xpath,
                        SupportingDataValues = validValues
                    };

                    _logger.LogInformation("{prefillActivity}", prefillActivity);
                    prefillActivities.Add(prefillActivity);
                }
            }
            catch (XmlException e)
            {
                _logger.LogError("Error parsing xml-string to xDocument", e);
            }

            _logger.LogInformation($"Number of prefilled activities {prefillActivities.Count}");

            return prefillActivities;
        }

        private List<Activity> GetCurrentCheckpoint(string processCategory, int metadataTypeId)
        {
            var activityWithSelectedMetadataTypeId = _dbContext.ActivityMetadatas
                .Where(a => a.MetadataTypeId == metadataTypeId)
                .Select(m => m.ActivityId)
                .ToList();

            var activities = _dbContext.Activity
                .Where(a => activityWithSelectedMetadataTypeId.Contains(a.Id)
                            && a.Status == StatusForActivity.Published
                            && a.Processcategory == processCategory)
                .Include(m => m.Actions)
                .Include(a => a.Metadata)
                .ThenInclude(m => m.MetadataItem)
                .Include(a => a.EnterpriseTerms).ToList();

            return activities;
        }

        // Erstatt escape-sekvenser med opprinnelige tegn i en streng
        private static string UnescapeSpecialCharacters(string value)
        {
            value = value.Replace("\\\\", "\\")
                .Replace("\\\"", "\"")
                .Replace("\\/", "/")
                .Replace("\\n", "\n")
                .Replace("\\t", "\t")
                .Replace("\\r", "\r")
                .Replace("\\b", "\b")
                .Replace("\\f", "\f");

            return value;
        }

        public void AddorUpdateValidationIds(List<ActivityActionValidationRulesViewModel> activityActionValidationRulesViewModel)
        {
            foreach (var activityActionValidationRule in activityActionValidationRulesViewModel)
            {
                var activity = _dbContext.Activity
                                    .Where(a => a.Id == activityActionValidationRule.ActivityId)
                                    .Include(i => i.Actions)
                                    .ThenInclude(a => a.SupportiveValidationRules)
                                    .ThenInclude(v => v.ValidationRules)
                                    .FirstOrDefault();

                var action = activity?.Actions.FirstOrDefault(a => a.Id == activityActionValidationRule.ActionId);

                if (action != null)
                {
                    foreach (var newRules in activityActionValidationRule.ValidationRules)
                    {
                        var newValidationRules = new ActionValidationRules(newRules.ValidationRuleIds, newRules.Id);

                        if (newValidationRules.Id != 0)
                        {
                            var supportiveValidationRulesToUpdate = action.SupportiveValidationRules.FirstOrDefault(r => r.Id == newRules.Id);

                            if (supportiveValidationRulesToUpdate != null)
                            {
                                DeleteActionValidationRulesById(newRules.Id);
                                supportiveValidationRulesToUpdate.ValidationRules = newValidationRules.ValidationRules;
                            }
                            else
                            {
                                _logger.LogError($"Trying to update activityActionValidationRules - ValidationRuleId {newValidationRules.Id} on activity {activity.Id} and {action.Id} does not exist");
                            }
                        }
                        else
                        {
                            action.SupportiveValidationRules.Add(newValidationRules);
                        }
                    }
                }
            }

            _dbContext.SaveChanges();
        }

        public List<ActivityAndValidationIdsViewModel> GetActivitiesAndValidationRuleIds(ValidationRuleFilter validationRuleFilter)
        {
            var viewModel = new List<ActivityAndValidationIdsViewModel>();

            var query = _dbContext.Action.Include(a => a.Activity)
                                         .ThenInclude(a => a.EnterpriseTerms)
                                         .Include(a => a.SupportiveValidationRules)
                                         .ThenInclude(s => s.ValidationRules)
                                         .Where(a => a.SupportiveValidationRules.Any());

            if (!string.IsNullOrEmpty(validationRuleFilter.ProcessCategory))
            {
                query = query.Where(a => a.Activity.Processcategory == validationRuleFilter.ProcessCategory);
            }

            query = query.OrderBy(a => a.Activity.Processcategory);

            var actions = query.AsSplitQuery().ToList();

            if (validationRuleFilter.ValidDate != null)
            {
                actions = actions.Where(a => _checklistFilterService.IsValidOnDate(a.Activity, validationRuleFilter.ValidDate.Value)).ToList();
            }

            foreach (var action in actions)
            {
                var activityActionValidationRulesViewModel = new ActivityAndValidationIdsViewModel()
                {
                    ActivityId = action.ActivityId,
                    ActionValue = action.ActionValue,
                    ActionId = action.Id,
                    ProcessCategory = action.Activity.Processcategory,
                    ChecklistReference = action.Activity.ReferenceId,
                    EnterpriseTerms = action.Activity.EnterpriseTerms.Select(e => e.Code).ToList(),
                    ValidFrom = action.Activity.ValidFrom,
                    ValidTo = action.Activity.ValidTo
                };

                foreach (var validationRules in action.SupportiveValidationRules)
                {
                    activityActionValidationRulesViewModel.SupportingDataValidationRuleId.Add(new ValidationRulesViewModel
                    {
                        Id = validationRules.Id,
                        ValidationRuleIds = validationRules.ValidationRules
                            .Select(v => v.ValidationRuleId).ToList()
                    });
                }
                viewModel.Add(activityActionValidationRulesViewModel);
            }

            return viewModel;
        }

        public async Task<List<PrefillActivityActionDemo>> GetPrefillDemo(string processCategory)
        {
            var prefillActivities = new List<PrefillActivityActionDemo>();
            var prefillActivitiesByValidationRules = await PrefillByValidationRules(processCategory);
            var prefillActivitiesByXpath = await PrefillByXpath(processCategory);

            if (prefillActivitiesByValidationRules != null) prefillActivities.AddRange(prefillActivitiesByValidationRules);
            if (prefillActivitiesByXpath != null) prefillActivities.AddRange(prefillActivitiesByXpath);

            return prefillActivities;
        }

        private async Task<List<PrefillActivityActionDemo>> PrefillByXpath(string processCategory)
        {
            var metadataTypePrefillXpath = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillXpathMetadataTypeName);
            if (metadataTypePrefillXpath == null) { return null; }

            var metadataTypePrefillSelectedCodeValue = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillSelectedCodeValueMetadataTypeName);

            var prefillActivities = new List<PrefillActivityActionDemo>();

            _logger.LogInformation("Get activities with selected metadataType");
            var currentActivities = GetCurrentCheckpoint(processCategory, metadataTypePrefillXpath.Id);

            foreach (var activity in currentActivities)
            {
                var action = activity.Actions?.FirstOrDefault()!;
                if (action == null) continue;
                var supportingXpath = activity.Metadata
                    .FirstOrDefault(m => m.MetadataTypeId == metadataTypePrefillXpath.Id)
                    ?.MetadataItem?.Value;

                var supportingMetadataItemCodeValue = new List<string>();
                if (metadataTypePrefillSelectedCodeValue != null)
                {
                    supportingMetadataItemCodeValue = activity.Metadata
                        .Where(m => m.MetadataTypeId == metadataTypePrefillSelectedCodeValue.Id)
                        .Select(m => m.MetadataItem?.Value).ToList();
                }

                var prefillActivity = new PrefillActivityActionDemo
                {
                    ChecklistReference = activity.ReferenceId,
                    ChecklistQuestion = activity.Name,
                    YesNo = !action.ActionValue,
                    ActionTypeCode = action.ActionTypeCode,
                    ParentActivityAction = activity.ParentReferenceId,
                    SupportingDataXpathField = supportingXpath,
                    SupportingDataValues = supportingMetadataItemCodeValue
                };

                prefillActivities.Add(prefillActivity);
            }

            return prefillActivities;
        }

        private async Task<List<PrefillActivityActionDemo>> PrefillByValidationRules(string processCategory)
        {
            var currentActionValidationRules = GetCurrentActionValidationRules(processCategory, null);
            var prefillActivities = new List<PrefillActivityActionDemo>();

            foreach (var actionValidationRules in currentActionValidationRules)
            {
                var prefillActivity = new PrefillActivityActionDemo
                {
                    ChecklistReference = actionValidationRules.Action.Activity.ReferenceId,
                    ChecklistQuestion = actionValidationRules.Action.Activity.Name,
                    YesNo = !actionValidationRules.Action.ActionValue,
                    ActionTypeCode = actionValidationRules.Action.ActionTypeCode,
                    ParentActivityAction = actionValidationRules.Action.Activity.ParentReferenceId,
                    SupportingDataValidationRuleId = actionValidationRules.ValidationRules.Select(rule => rule.ValidationRuleId).ToList()
                };

                prefillActivities.Add(prefillActivity);
            }

            return prefillActivities;
        }

        // TODO se metode DeleteActionValidationRulesById
        public void DeleteActionValidationRuleById(int actionValidationRuleId)
        {
            try
            {
                var actionalidationRuleToRemove = _dbContext.ActionValidationRules.Include(v => v.ValidationRules).FirstOrDefault(v => v.Id == actionValidationRuleId);
                if (actionalidationRuleToRemove == null) return;

                foreach (var validationRule in actionalidationRuleToRemove.ValidationRules)
                {
                    _dbContext.Remove((object)validationRule);
                }
                _dbContext.Remove((object)actionalidationRuleToRemove);
                _dbContext.SaveChanges();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void DeleteActionValidationRulesById(int actionValidationRuleId)
        {
            try
            {
                var actionalidationRuleToRemove = _dbContext.ActionValidationRules.Include(v => v.ValidationRules).FirstOrDefault(v => v.Id == actionValidationRuleId);
                if (actionalidationRuleToRemove == null) return;

                foreach (var validationRule in actionalidationRuleToRemove.ValidationRules)
                {
                    _dbContext.Update((object)validationRule);
                    _dbContext.Remove((object)validationRule);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<PrefillInformationApiModel> GetPrefillInformation(string processCategory)
        {
            var activitiesAndValidationRuleIds = GetActivitiesAndValidationRuleIds(new ValidationRuleFilter() { ProcessCategory = processCategory });
            var activitiesPrefilXpath = await GetActivitiesAndPrefillXpath(processCategory);

            return new PrefillInformationApiModel(activitiesAndValidationRuleIds, activitiesPrefilXpath, processCategory);
        }

        public async Task<List<Activity>> GetActivitiesAndActions(string processCategory)
        {
            var activities = _dbContext.Activity
                .Include(a => a.Actions)
                .ThenInclude(a => a.SupportiveValidationRules)
                .ThenInclude(s => s.ValidationRules)
                .Where(a => a.Status == StatusForActivity.Published);

            if (!string.IsNullOrEmpty(processCategory))
            {
                activities = activities.Where(a => a.Processcategory == processCategory);
            }

            return activities.AsSplitQuery().ToList();
        }

        private async Task<List<ActivityAndPrefillXpathViewModel>> GetActivitiesAndPrefillXpath(string processCategory)
        {
            var viewModel = new List<ActivityAndPrefillXpathViewModel>();
            var metadataTypePrefillXpath = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillXpathMetadataTypeName);
            var metadataTypeSelectedCodeValue = await _metadataService.GetMetadataTypeByName(_options.Value.PrefillSelectedCodeValueMetadataTypeName);
            var currentActivities = GetCurrentCheckpoint(processCategory, metadataTypePrefillXpath.Id);

            foreach (var activity in currentActivities)
            {
                var xpath = activity.Metadata
                    .FirstOrDefault(m => m.MetadataTypeId == metadataTypePrefillXpath.Id)
                    ?.MetadataItem?.Value;

                if (string.IsNullOrWhiteSpace(xpath)) continue;

                var codeValues = activity.Metadata
                    .Where(m => m.MetadataTypeId == metadataTypeSelectedCodeValue.Id).Select(m => m.MetadataItem.Value).ToList();

                var activityActionValidationRulesViewModel = new ActivityAndPrefillXpathViewModel()
                {
                    ActivityId = activity.Id,
                    ActionValue = true,
                    ProcessCategory = activity.Processcategory,
                    ChecklistReference = activity.ReferenceId,
                    EnterpriseTerms = activity.EnterpriseTerms.Select(e => e.Code).ToList(),
                    ValidFrom = activity.ValidFrom,
                    ValidTo = activity.ValidTo,
                    PrefillXpath = xpath,
                    PrefillSelectedCodeValues = codeValues.Any() ? codeValues : null
                };

                viewModel.Add(activityActionValidationRulesViewModel);
            }

            return viewModel;
        }

        private List<ActionValidationRules> GetCurrentActionValidationRules(string processCategory, List<string> enterpriseTerms)
        {
            var currentValidationRules = _dbContext.ActionValidationRules
                .Include(a => a.Action.Activity.EnterpriseTerms)
                .Include(a => a.ValidationRules)
                .Where(v => v.Action.Activity.Processcategory == processCategory)
                .AsSplitQuery()
                .ToList();

            if (enterpriseTerms == null || enterpriseTerms.Count == 0)
            {
                return currentValidationRules;
            }

            return currentValidationRules
                .Where(validationRule => validationRule.Action.Activity.EnterpriseTerms
                    .Any(e => enterpriseTerms.Contains(e.Code)))
                .ToList();
        }
    }
}