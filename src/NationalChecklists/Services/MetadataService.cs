﻿using Microsoft.EntityFrameworkCore;
using NationalChecklists.Middleware.Models;
using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;
using NationalChecklists.ViewModels;

namespace NationalChecklists.Services
{
    public interface IMetadataService
    {
        Task<List<MetadataType>> GetMetadataTypesAsync(MetadataFilter metadataFilter = null, bool? showAsFacetFilter = null);
        Task AddMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypeViewModels);
        Task UpdateMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypeViewModels);
        Task<List<Metadata>> AddOrUpdateMetadataAsync(IEnumerable<MetadataViewModel> modelMetadata, int activityId);
        Task<List<ActivityMetadata>> AddOrUpdateMetadataAsync(IEnumerable<MetadataViewModel> metadataViewmodelList);
        List<ActivityMetadata> MapToActivityMetadata(List<Metadata> metadata);
        Task DeleteMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypesViewModel);
        Task UpdateStaticMetadatasAsync(List<StaticMetadata> staticMetadatas);
        Task AddMetadatasAsync(List<Metadata> metadatas);
        Task UpdateMetadatasAsync(List<Metadata> metadatas);
        Task DeleteMetadatasAsync(List<int> metadataIds);
        List<Metadata> GetMetadatas(List<int> metadataIds);
        List<Metadata> GetMetadataByName(List<string> metadataValues);
        Task<MetadataType> GetMetadataTypeByName(string name);
        List<MetadataType> GetMetadataTypes(List<int> metadataTypeId);
    }

    public class MetadataService : IMetadataService
    {
        private readonly ApplicationDbContext _dbContext;
        private ILogger<MetadataService> _logger;

        public MetadataService(ApplicationDbContext dbContext, ILogger<MetadataService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task<List<MetadataType>> GetMetadataTypesAsync(MetadataFilter metadataFilter, bool? showAsFacetFilter = null)
        {
            var metadataTypes = await GetMetadataTypesAsync();
            if (metadataTypes == null) throw new ArgumentNullException(nameof(metadataTypes));

            metadataTypes
                .ForEach(metadataType => metadataType.MetadataValues = metadataType.MetadataValues
                    .OrderBy(metadataValue => metadataValue?.OrderNumber)
                    .ToList());

            _logger.LogDebug($"Metadata values order by ordernumber.");

            if (showAsFacetFilter != null)
            {
                metadataTypes = metadataTypes.Where(m => showAsFacetFilter != null && m.Facet == showAsFacetFilter.Value).ToList();
                _logger.LogDebug($"MetadataTypes filtered by showAsFacetFilter - {showAsFacetFilter.Value}");
            }

            if (metadataFilter != null)
            {
                if (metadataFilter.CanUpdateValues != null)
                {
                    metadataTypes = metadataTypes.Where(m => m.CanUpdateValues == metadataFilter.CanUpdateValues)
                        .ToList();
                    _logger.LogDebug($"MetadataTypes filtered by CanUpdateValues - {metadataFilter.CanUpdateValues}");
                }

                if (metadataFilter.MetadataTypeId != null)
                {
                    metadataTypes = metadataTypes.Where(m => m.Id == metadataFilter.MetadataTypeId).ToList();
                    _logger.LogDebug($"MetadataTypes filtered by MetadataTypeId - {metadataFilter.MetadataTypeId}");
                }
            }

            _logger.LogDebug($"MetadataTypes ordered by OrderNumber");
            return metadataTypes.OrderBy(o => o.OrderNumber).ToList();
        }

        public async Task<List<MetadataType>> GetMetadataTypesAsync()
        {
            try
            {
                var metadataTypes = await _dbContext.MetadataTypes.Include(type => type.MetadataValues).ToListAsync();
                _logger.LogDebug("Get metadata types and values.");
                return metadataTypes;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Get metadata types and values");
                return new List<MetadataType>();
            }
        }

        public async Task AddMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypeViewModels)
        {
            var metadataTypes = new MetadataType().Map(metadataTypeViewModels);

            foreach (var metadataType in metadataTypes)
            {
                await AddMetadataTypeAsync(metadataType);
            }
        }

        public async Task UpdateMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypes)
        {
            foreach (var metadataType in metadataTypes)
            {
                await UpdateMetadataTypeAsync(metadataType);
            }
        }

        public async Task AddMetadatasAsync(List<Metadata> metadatas)
        {
            await _dbContext.Metadatas.AddRangeAsync(metadatas);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateMetadatasAsync(List<Metadata> metadatas)
        {
            foreach (var metadata in metadatas)
            {
                var existing = await _dbContext.Metadatas.SingleOrDefaultAsync(mdata => mdata.Id == metadata.Id);
                existing.Update(metadata);
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteMetadatasAsync(List<int> metadataIds)
        {
            var toDelete = await _dbContext.Metadatas
                .Where(metadata => metadataIds.Contains(metadata.Id))
                .ToListAsync();

            _dbContext.Metadatas.RemoveRange(toDelete);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<Metadata>> AddOrUpdateMetadataAsync(IEnumerable<MetadataViewModel> metadataViewmodelList, int activityId)
        {
            var metadataList = new List<Metadata>();
            var currentMetadata = GetMetadataTypes(metadataViewmodelList);

            foreach (var metadataViewmodel in metadataViewmodelList)
            {
                var metadataType = currentMetadata.SingleOrDefault(m => m.Id == metadataViewmodel.MetadataTypeId);
                if (metadataType == null) continue;

                // Er det lov å legge til metadata på denne typen?
                if (metadataType.CanUpdateValues)
                {
                    var activity = await GetActivity(activityId);

                    if (activity != null)
                    {
                        var activityMetadataByMetadataTypeId = activity.Metadata.Where(m => m.MetadataTypeId == metadataType.Id).ToList();
                        _logger.LogInformation($"Get current activity metadataType to update: {activityMetadataByMetadataTypeId}");

                        // Skal legges til som ny metadata
                        if (CanAddMetadata(metadataViewmodel))
                        {
                            _logger.LogInformation($"Start - Add metadata values to metadataType {metadataType.Id} - {metadataType.Name}");

                            if (activityMetadataByMetadataTypeId.Count < metadataType.MaxCountOfValues || metadataType.MaxCountOfValues == 0)
                            {
                                _logger.LogInformation($"Start - Create new metadata with value {metadataViewmodel.MetadataValue}, for metadataType {metadataViewmodel.MetadataTypeId}");
                                var newMetadata = new Metadata() { Value = metadataViewmodel.MetadataValue, TypeId = metadataViewmodel.MetadataTypeId };

                                _dbContext.Attach(metadataType).State = EntityState.Modified;
                                metadataType.MetadataValues.Add(newMetadata);
                                metadataList.Add(newMetadata);
                                await _dbContext.SaveChanges();
                                _logger.LogInformation($"Finished - {newMetadata} was added to {activity.Id} created.");
                            }
                            else
                            {
                                foreach (var existingMetadata in activityMetadataByMetadataTypeId)
                                {
                                    _logger.LogInformation($"Update value on existing MetadataItem Id:{existingMetadata.MetadataId} {existingMetadata.CommonValue}");
                                    existingMetadata.MetadataItem.Value = metadataViewmodel.MetadataValue;
                                    metadataList.Add(existingMetadata.MetadataItem);
                                }
                            }

                        }
                        else
                        {
                            _logger.LogInformation($"Start - Update metadata values on metadataType {metadataType.Id} - {metadataType.Name}");

                            var metadataToUpdate = metadataType.MetadataValues.FirstOrDefault(m => m.Id == metadataViewmodel.MetadataId.Value);
                            _logger.LogInformation($"Metadata to update: {metadataToUpdate} - new metadata value: {metadataViewmodel.MetadataValue}");

                            if (metadataToUpdate != null) metadataToUpdate.Value = metadataViewmodel.MetadataValue;
                            metadataList.Add(metadataToUpdate);
                            await _dbContext.SaveChanges();
                            _logger.LogInformation($"Finished - Update metadata values on metadataType {metadataType.Id} - {metadataType.Name}");

                        }
                    }
                }
                else
                {
                    _logger.LogInformation($"Add new metadata: {metadataViewmodel.MetadataValue}");
                    metadataList.Add(new Metadata() { Value = metadataViewmodel.MetadataValue, TypeId = metadataViewmodel.MetadataTypeId, Id = metadataViewmodel.MetadataId.Value });
                }
            }

            return metadataList;
        }

        private List<MetadataType> GetMetadataTypes(IEnumerable<MetadataViewModel> metadataViewmodelList)
        {
            var idMetadataTypes = new List<int>();
            try
            {
                _logger.LogInformation($"Get current metadataTypes");
                idMetadataTypes.AddRange(metadataViewmodelList.Select(metadataViwModel => metadataViwModel.MetadataTypeId));

                return _dbContext.MetadataTypes.Include(m => m.MetadataValues)
                    .Where(m => idMetadataTypes.Contains(m.Id)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not find any metadatatypes with metadataTypeId {string.Join(",", idMetadataTypes)}");
                return null;
            }
        }

        private async Task<MetadataType> AddMetadataTypeAsync(MetadataType metadataType)
        {
            if (metadataType == null)
                return null;

            _dbContext.MetadataTypes.Add(metadataType);

            await _dbContext.SaveChangesAsync();
            return metadataType;
        }

        public async Task<List<ActivityMetadata>> AddOrUpdateMetadataAsync(IEnumerable<MetadataViewModel> metadataViewmodelList)
        {
            var metadataList = new List<Metadata>();

            foreach (var metadataViewmodel in metadataViewmodelList)
            {
                var metadataType = GetMetadataTypeById(metadataViewmodel.MetadataTypeId);
                if (metadataType != null)
                {
                    _logger.LogDebug($"Update metadata type - Id {metadataType.Id}, name {metadataType.Name}");
                    // Er det lov å legge til metadata på denne typen?
                    if (metadataType.CanUpdateValues)
                    {
                        // Skal legges til som ny metadata
                        if (CanAddMetadata(metadataViewmodel))
                        {
                            var newMetadata = new Metadata() { Value = metadataViewmodel.MetadataValue, TypeId = metadataViewmodel.MetadataTypeId };

                            try
                            {
                                _dbContext.Attach(metadataType).State = EntityState.Modified;
                                metadataType.MetadataValues.Add(newMetadata);
                                metadataList.Add(newMetadata);
                                _logger.LogDebug($"Add metadata", newMetadata);

                                await _dbContext.SaveChanges();
                            }
                            catch (Exception e)
                            {
                                _logger.LogError(e, $"error saving {newMetadata}");
                            }
                        }
                    }
                    else
                    {
                        if (metadataViewmodel.MetadataId == null) continue;

                        var metadataId = metadataViewmodel.MetadataId.Value;
                        var selectedMetadata = metadataType.MetadataValues.FirstOrDefault(m => m.Id == metadataId);
                        _logger.LogDebug($"Get selected metadata {selectedMetadata}");
                        metadataList.Add(new Metadata() { Value = selectedMetadata.Value, TypeId = metadataViewmodel.MetadataTypeId, Id = selectedMetadata.Id });
                    }
                }
            }

            return MapToActivityMetadata(metadataList);
        }

        private bool CanAddMetadata(MetadataViewModel metadataViewModel)
        {
            return metadataViewModel.MetadataId is null or 0 && metadataViewModel.MetadataTypeId > 0;
        }

        private async Task<Activity> GetActivity(int activityId)
        {
            try
            {
                _logger.LogInformation($"Get Activity by activityId {activityId}");
                return await _dbContext.Activity.Include(m => m.Metadata).FirstOrDefaultAsync(a => a.Id == activityId);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error getting Activity by Id {activityId}");
                return null;
            }

        }

        public List<ActivityMetadata> MapToActivityMetadata(List<Metadata> metadataList)
        {
            var activityMetadataList = new List<ActivityMetadata>();
            foreach (var metadata in metadataList)
            {
                metadata.Type ??= GetMetadataTypeById(metadata.TypeId);
                activityMetadataList.Add(new ActivityMetadata() { MetadataId = metadata.Id, MetadataTypeId = metadata.TypeId, CommonValue = metadata.Type.CommonValue });
            }

            return activityMetadataList;
        }

        public async Task DeleteMetadataTypesAsync(List<MetadataTypeViewModel> metadataTypesViewModel)
        {
            foreach (var metadataType in metadataTypesViewModel)
            {
                var metadataTypeToDelete = await _dbContext.MetadataTypes.FindAsync(metadataType.Id);

                if (metadataTypeToDelete != null)
                    _dbContext.Entry(metadataTypeToDelete).State = EntityState.Deleted;
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateStaticMetadatasAsync(List<StaticMetadata> staticMetadatas)
        {
            foreach (var staticMetadataType in staticMetadatas)
            {
                await UpdateStaticMetadataType(staticMetadataType);
            }
        }

        public List<Metadata> GetMetadatas(List<int> metadataIds)
        {
            try
            {
                return metadataIds.Select(metadataId => _dbContext.Metadatas.First(m => m.Id == metadataId)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError($"Could not find any metadata at given metadataId {string.Join(", ", metadataIds)} ");
                return new List<Metadata>();
            }
        }

        public List<Metadata> GetMetadataByName(List<string> metadataValues)
        {
            try
            {
                return metadataValues.Select(metadataValue => _dbContext.Metadatas.First(m => m.Value == metadataValue)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError($"Could not find any metadata at given metadataId {string.Join(", ", metadataValues)} ");
                return new List<Metadata>();
            }
        }

        public async Task<MetadataType> GetMetadataTypeByName(string name)
        {
            return _dbContext.MetadataTypes.FirstOrDefault(m => m.Name.Equals(name));
        }

        public List<MetadataType> GetMetadataTypes(List<int> metadataTypeId)
        {
            try
            {
                return metadataTypeId.Select(id => _dbContext.MetadataTypes.First(m => m.Id == id)).ToList();
            }
            catch (Exception e)
            {
                _logger.LogError($"Could not find any metadataTypes at given metadataTypeId {string.Join(", ", metadataTypeId)} ");
                return new List<MetadataType>();
            }
        }

        private async Task UpdateStaticMetadataType(StaticMetadata staticMetadata)
        {
            if (staticMetadata == null) return;

            var metadataTypeInDatabase = _dbContext.StaticMetadata.Find(staticMetadata.Id);
            if (metadataTypeInDatabase == null) return;

            _dbContext.Attach(metadataTypeInDatabase).State = EntityState.Modified;

            metadataTypeInDatabase.Name = staticMetadata.Name;
            metadataTypeInDatabase.Description = staticMetadata.Description;
            metadataTypeInDatabase.OrderNumber = staticMetadata.OrderNumber;
            metadataTypeInDatabase.ShowAsFacetFilter = staticMetadata.ShowAsFacetFilter;
            metadataTypeInDatabase.ShowOnDetailPage = staticMetadata.ShowOnDetailPage;

            await _dbContext.SaveChangesAsync();
        }

        private async Task UpdateMetadataTypeAsync(MetadataTypeViewModel metadataType)
        {
            if (metadataType == null) return;

            var metadataTypeInDatabase = _dbContext.MetadataTypes.Find(metadataType.Id);
            if (metadataTypeInDatabase == null) return;

            _dbContext.Attach(metadataTypeInDatabase).State = EntityState.Modified;

            if (metadataType.MetadataValues != null)
            {
                foreach (var metadataValue in metadataType.MetadataValues)
                {
                    var metadataInDatabase = GetMetadataById(metadataValue.Id);

                    if (metadataInDatabase == null && metadataValue.Id == 0)
                    {
                        metadataTypeInDatabase.MetadataValues.Add(metadataValue);
                    }
                    else if (metadataInDatabase != null)
                    {
                        _dbContext.Attach(metadataTypeInDatabase).State = EntityState.Modified;
                        if (metadataValue.Value != null) metadataInDatabase.Value = metadataValue.Value;
                        if (metadataValue.MetadataCode != null) metadataInDatabase.MetadataCode = metadataValue.MetadataCode;
                        metadataInDatabase.OrderNumber = metadataValue.OrderNumber;
                        if (metadataValue.ParentId != null) metadataInDatabase.ParentId = metadataValue.ParentId;
                        metadataInDatabase.TypeId = metadataValue.TypeId;
                    }
                }
            }
            if (!string.IsNullOrWhiteSpace(metadataType.Name)) metadataTypeInDatabase.Name = metadataType.Name;
            if (metadataType.OrderNumber != null) metadataTypeInDatabase.OrderNumber = metadataType.OrderNumber.Value;
            metadataTypeInDatabase.ViewType = metadataType.ViewType;
            if (metadataType.CanUpdateValues != null) metadataTypeInDatabase.CanUpdateValues = metadataType.CanUpdateValues.Value;
            if (metadataType.MaxCountOfValues != null) metadataTypeInDatabase.MaxCountOfValues = metadataType.MaxCountOfValues.Value;
            metadataTypeInDatabase.CommonValue = metadataType.CommonValue;
            metadataTypeInDatabase.Facet = metadataType.Facet;

            await _dbContext.SaveChangesAsync();
        }

        private MetadataType GetMetadataTypeById(int metadataTypeId)
        {
            try
            {
                _logger.LogInformation($"Get metadataType by Id: {metadataTypeId}");
                return _dbContext.MetadataTypes.Include(m => m.MetadataValues)
                    .FirstOrDefault(m => m.Id == metadataTypeId);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Could not find any metadatatypes with metadataTypeId {metadataTypeId}");
                return null;
            }
        }

        private Metadata GetMetadataById(int metadataId)
        {
            return _dbContext.Metadatas.FirstOrDefault(m => m.Id == metadataId);
        }

    }
}
