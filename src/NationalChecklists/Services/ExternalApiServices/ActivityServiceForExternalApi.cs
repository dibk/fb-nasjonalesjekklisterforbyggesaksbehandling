﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NationalChecklists.Configuration;
using NationalChecklists.Models;
using NationalChecklists.Models.ExternalApiModels;
using NationalChecklists.ViewModels;

namespace NationalChecklists.Services.ExternalApiServices
{
    public interface IActivityServiceForExternalApi
    {
        List<Sjekk> ToCheckList(List<Activity> activities);

        string[] GetActivityEnterpriseTerms(string referenceId, string processcategory);

        Task<List<Activity>> GetActivities(string prosesskategori, List<string> tiltakstyper, string milepel, SearchFilterViewModel searchFilterModel);

        Activity GetActivity(string pkt, string prosesskategori);
    }

    public class ActivityServiceForExternalApi : IActivityServiceForExternalApi
    {
        private readonly IChecklistFilterService _checklistFilterService;
        private readonly IMetadataService _metadataService;
        private ApplicationDbContext _dbContext;
        private readonly IOptions<CheckpointConfig> _options;

        public ActivityServiceForExternalApi(IChecklistFilterService checklistFilterService, ApplicationDbContext dbContext, IOptions<CheckpointConfig> options, IMetadataService metadataService)
        {
            _checklistFilterService = checklistFilterService;
            _dbContext = dbContext;
            _options = options;
            _metadataService = metadataService;
        }

        public async Task<List<Activity>> GetActivities(string processcategory, List<string> enterpriseTerms, string milestone, SearchFilterViewModel searchFilterModel)
        {
            var activities = new List<Activity>();

            if (searchFilterModel != null)
            {
                var activitiesQueryable = processcategory == null ? GetPublishedActivities() : GetPublishedActivitiesFilteredByProcessCategory(processcategory);

                activitiesQueryable = activitiesQueryable
                    .Include(l => l.LawReferences)
                    .Include(e => e.EnterpriseTerms)
                    .Include(a => a.Actions)
                    .Include(a => a.Metadata)
                    .ThenInclude(mi => mi.MetadataItem)
                    .ThenInclude(t => t.Type)
                    .OrderBy(o => o.OrderNumber);

                activities = await activitiesQueryable.AsSplitQuery().ToListAsync();

                if (searchFilterModel.OppdatertEtter != null) activities = _checklistFilterService.FilterActivitiesByLastUpdated(activities, searchFilterModel.OppdatertEtter.Value);

                activities = _checklistFilterService.FilterActivitiesByValidDate(activities, searchFilterModel.ValidDate);

                if (searchFilterModel.Eier.Any())
                    activities = _checklistFilterService.FilterActivitiesByOwner(activities, searchFilterModel.Eier);

                if (searchFilterModel.Tiltakstyper?.Count > 0)
                    activities = _checklistFilterService.FilterActivitiesByEnterpriseTerms(activities, searchFilterModel.Tiltakstyper);

                if (milestone != null && !string.IsNullOrEmpty(milestone))
                    activities = _checklistFilterService.FilterActivitiesByMilestone(activities, new List<string> { milestone });

                if (searchFilterModel.MetadataTypeId.Any())
                {
                    var selectedMetadataType = _metadataService.GetMetadataTypes(searchFilterModel.MetadataTypeId);
                    activities = _checklistFilterService.FilterActivitiesByMetadataTypeId(activities, selectedMetadataType);
                }

                if (searchFilterModel.MetadataId.Any())
                {
                    var selectedMetadata = _metadataService.GetMetadatas(searchFilterModel.MetadataId);
                    activities = _checklistFilterService.FilterActivitiesByMetadataId(activities, selectedMetadata);
                }

                if (searchFilterModel.MetadataValue.Any())
                {
                    var selectedMetadata = _metadataService.GetMetadataByName(searchFilterModel.MetadataValue);
                    activities = _checklistFilterService.FilterActivitiesByMetadataId(activities, selectedMetadata);
                }
            }

            SetupActivityRelationships(activities);

            return activities;
        }

        private void SetupActivityRelationships(List<Activity> activities)
        {
            activities.RemoveAll(a => a.ParentActivity != null);
        }

        private IQueryable<Activity> GetPublishedActivities()
        {
            var queryResult = from a in _dbContext.Activity
                              where a.Status == StatusForActivity.Published
                              orderby a.OrderNumber
                              select a;

            return queryResult;
        }

        private IQueryable<Activity> GetPublishedActivitiesFilteredByProcessCategory(string processcategory)
        {
            var query = _dbContext.Activity
                .Where(a => a.Processcategory == processcategory &&
                            a.Status == StatusForActivity.Published);

            return query;
        }

        /// <summary>
        /// Convert list of Activity to list of Sjekk
        /// </summary>
        /// <param name="activities"></param>
        /// <returns></returns>
        public List<Sjekk> ToCheckList(List<Activity> activities)
        {
            return activities.Select(activity => new Sjekk(activity)).ToList();
        }

        public string[] GetActivityEnterpriseTerms(string referenceId, string processcategory)
        {
            var result = new List<string>();
            var a = GetActivity(referenceId, processcategory);

            if (a?.EnterpriseTerms != null)
                result.AddRange(a.EnterpriseTerms.Select(tiltak => tiltak.Code));

            return result.ToArray();
        }

        public Activity GetActivity(string referenceId, string processcategory)
        {
            var queryResult = from a in _dbContext.Activity
                    .Include(s => s.SubActivities)
                    .Include(p => p.ParentActivity)
                    .Include(l => l.LawReferences)
                    .Include(a => a.Actions)
                    .Include(e => e.EnterpriseTerms)
                    .Include(m => m.Metadata).ThenInclude(mi => mi.MetadataItem).ThenInclude(t => t.Type)
                              where a.Processcategory == processcategory && a.ReferenceId == referenceId && a.Status == StatusForActivity.Published
                              orderby a.OrderNumber
                              select a;

            return queryResult.AsSplitQuery().FirstOrDefault();
        }
    }
}