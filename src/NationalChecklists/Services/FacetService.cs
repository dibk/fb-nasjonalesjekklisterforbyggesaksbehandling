﻿using Microsoft.EntityFrameworkCore;
using NationalChecklists.Middleware.Models;
using NationalChecklists.Models;
using NationalChecklists.Models.Facets;
using NationalChecklists.ViewModels;
using ActivityType = NationalChecklists.Models.Facets.ActivityType;

namespace NationalChecklists.Services
{
    public interface IFacetService
    {
        Task AddFacetsAsync(IEnumerable<Facet> facets);
        Task UpdateFacetsAsync<TFacet>(List<TFacet> facets, int staticMetadataId) where TFacet : Facet;
        Task DeleteFacetsAsync(List<string> facetIds);
        Task<ChecklistSearchViewModel> MapIdToDescriptionAsync(ChecklistSearchViewModel searchModel);
        Task<List<StaticMetadata>> GetStaticMetadataFiltersAsync();
        Task<List<StaticMetadata>> GetStaticMetadataTypesAsync(bool? showAsFacetFilter = null, bool? showOnDetailPage = null);
        Task<bool> ProcessCategoryExistsAsync(string processCategory);
    }

    public class FacetService : IFacetService
    {
        private readonly ApplicationDbContext _dbContext;
        private ILogger<FacetService> _logger;

        public FacetService(
            ApplicationDbContext dbContext, 
            ILogger<FacetService> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddFacetsAsync(IEnumerable<Facet> facets)
        {
            foreach (var facet in facets)
                await AddFacetAsync(facet);
        }

        public async Task UpdateFacetsAsync<TFacet>(List<TFacet> facets, int staticMetadataId)
            where TFacet : Facet
        {
            foreach (var facet in facets)
                await UpdateFacetAsync<TFacet>(facet, staticMetadataId);
        }

        public async Task DeleteFacetsAsync(List<string> facetIds)
        {
            foreach (var facetId in facetIds)
                await DeleteFacetAsync(facetId);
        }

        private async Task AddFacetAsync(Facet facet)
        {
            switch (facet)
            {
                case ProcessCategory processCategory:
                    processCategory.StaticMetadataId = StaticMetadata.ProcessCategory;
                    await _dbContext.ProcessCategories.AddAsync(processCategory);
                    break;
                case Milestone milestone:
                    milestone.StaticMetadataId = StaticMetadata.Milestone;
                    await _dbContext.Milestones.AddAsync(milestone);
                    break;
                case EnterpriseTermFacet enterpriseTerm:
                    enterpriseTerm.StaticMetadataId = StaticMetadata.EnterpriceTerms;
                    await _dbContext.EnterpriseTerms.AddAsync(enterpriseTerm);
                    break;
                case Category category:
                    category.StaticMetadataId = StaticMetadata.Category;
                    await _dbContext.Categories.AddAsync(category);
                    break;
                case ActivityType activityType:
                    activityType.StaticMetadataId = StaticMetadata.ActivityType;
                    await _dbContext.ActivityTypes.AddAsync(activityType);
                    break;
                case Owner owner:
                    owner.StaticMetadataId = StaticMetadata.Owner;
                    await _dbContext.AddAsync(owner);
                    break;
                default:
                    return;
            }

            await _dbContext.SaveChangesAsync();
        }

        private async Task UpdateFacetAsync<TFacet>(Facet facet, int staticMetadataId) where TFacet : Facet
        {
            if (facet?.Id == null)
                return;

            var facetInDatabase = await _dbContext.Facets.OfType<TFacet>().SingleOrDefaultAsync(fac => fac.Id == facet.Id);

            if (facetInDatabase == null)
                return;

            _dbContext.Attach(facetInDatabase).State = EntityState.Modified;

            facetInDatabase.Description = facet.Description;
            facetInDatabase.Text = facet.Text;
            facetInDatabase.OrderNumber = facet.OrderNumber;
            facetInDatabase.StaticMetadataId = staticMetadataId;

            await _dbContext.SaveChangesAsync();
        }

        private async Task DeleteFacetAsync(string facetId)
        {
            var facetToDelete = await _dbContext.Facets.FindAsync(facetId);

            if (facetToDelete == null) 
                return;

            _dbContext.Entry(facetToDelete).State = EntityState.Deleted;
            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<StaticMetadata>> GetStaticMetadataTypesAsync(bool? showAsFacetFilter = null, bool? showOnDetailPage = null)
        {
            var staticMetadatas = await GetStaticMetadataFiltersAsync();

            if (showAsFacetFilter != null)
            {
                staticMetadatas = staticMetadatas.Where(s => s.ShowAsFacetFilter).ToList();
                _logger.LogDebug($"Get staticmetadata for facet filter", staticMetadatas);
            }

            if (showOnDetailPage != null)
            {
                staticMetadatas = staticMetadatas.Where(s => s.ShowOnDetailPage).ToList();
                _logger.LogDebug($"Get staticmetadata for detail page", staticMetadatas);
            }
            
            return staticMetadatas;
        }

        public async Task<List<StaticMetadata>> GetStaticMetadataFiltersAsync()
        {
            try
            {
                var staticMetadata = await _dbContext.StaticMetadata
                    .OrderBy(metadata => metadata.OrderNumber)
                    .Include(metadata => metadata.Facets)
                    .ToListAsync();

                staticMetadata.ForEach(metadata => metadata.Facets = metadata.Facets.OrderBy(facet => facet.OrderNumber).ToList());

                return staticMetadata;
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error - Get staticmetadata filter");
                return new List<StaticMetadata>();
            }
        }

        public async Task<ChecklistSearchViewModel> MapIdToDescriptionAsync(ChecklistSearchViewModel searchModel)
        {
            searchModel.Category = await MapCategoryIdToValueAsync(searchModel.Category);
            searchModel.ActivityType = await MapActivityTypeIdToValueAsync(searchModel.ActivityType);

            return searchModel;
        }

        public async Task<bool> ProcessCategoryExistsAsync(string processCategory)
        {
            return (await _dbContext.ProcessCategories.FindAsync(processCategory)) != null;
        }

        private async Task<List<string>> MapActivityTypeIdToValueAsync(List<string> activityTypeId)
        {
            var categories = new List<string>();

            if (activityTypeId == null || !activityTypeId.Any())
                return categories;

            foreach (var id in activityTypeId)
            {
                var activityType = await _dbContext.ActivityTypes.FindAsync(id);
                
                if (activityType != null) 
                    categories.Add(activityType.Description);
            }

            return categories;
        }

        private async Task<List<string>> MapCategoryIdToValueAsync(List<string> categoryId)
        {
            List<string> categories = new List<string>();

            if (categoryId != null)
            {
                foreach (var id in categoryId)
                {
                    var category = await _dbContext.Categories.FindAsync(id);
                    
                    if (category != null) 
                        categories.Add(category.Description);
                }
            }

            return categories;
        }
    }
}
