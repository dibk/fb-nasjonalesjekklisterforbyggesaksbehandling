using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using NationalChecklists.Configuration;
using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;
using NationalChecklists.Models.Facets;
using NationalChecklists.ViewModels;
using static System.Array;
using Action = NationalChecklists.Models.Action;

namespace NationalChecklists.Services
{
    public interface IActivityServiceForInternalApi
    {
        Task<Activity> CreateDraftForActivity(int id);

        Task<List<Activity>> CreateDraftsForNewCheckpoit(CreateActivityViewModel model);

        Task<Activity> UpdateDraftActivity(Activity updatedActivity);

        Task<Activity> GetActivity(int? id);

        Task<Dictionary<string, List<Activity>>> GetActivities(ChecklistSearchModel searchModel, string processCategory, bool isAuthenticated);

        Task<IEnumerable<ActionTypeCodesWithValuesViewModel>> GetActions();

        Task DeleteDraft(int id);

        Task<Activity> UpdatePublishedActivity(int draftActivityId);

        public Task<List<Activity>> GetDraftActivities();

        List<string> GetProcessCategoriesOnReferenceId(string referenceId);

        Category GetCategoryByReferenceId(string parentReferenceId);
    }

    public class ActivityServiceForInternalApi : IActivityServiceForInternalApi
    {
        private ApplicationDbContext _dbContext;
        private readonly IChecklistFilterService _checklistUtilityService;
        private readonly IMetadataService _metadataService;
        private readonly ILogger<ActivityServiceForInternalApi> _logger;
        private readonly IOptions<CheckpointConfig> _options;

        public ActivityServiceForInternalApi(ApplicationDbContext dbContext, IChecklistFilterService checklistUtilityService, ILogger<ActivityServiceForInternalApi> logger, IMetadataService metadataService, IOptions<CheckpointConfig> options)
        {
            _dbContext = dbContext;
            _checklistUtilityService = checklistUtilityService;
            _metadataService = metadataService;
            _logger = logger;
            _options = options;
        }

        public async Task<Dictionary<string, List<Activity>>> GetActivities(ChecklistSearchModel searchModel, string processCategory, bool isAuthenticated)
        {
            List<Activity> activities = new List<Activity>();
            List<Activity> draftActivities = new List<Activity>();

            if (searchModel != null)
            {
                if (!processCategory.IsNullOrEmpty())
                {
                    var query = GetAllActivitiesFilteredByProcessCategory(processCategory);

                    if (searchModel.EnterpriseTerms?.Count > 0)
                        query = query.Include(a => a.EnterpriseTerms);/*.Where(a => a.EnterpriseTerms.Any(term => searchModel.EnterpriseTerms.Contains(term.Code)));                 */

                    activities = await query.ToListAsync();

                    // Drafts
                    if (isAuthenticated)
                    {
                        var newActivities = GetUnpublishedActivitiesFilteredByProcessCategory(processCategory).ToList();
                        draftActivities = await GetDraftActivities(processCategory);

                        foreach (var activity in newActivities)
                        {
                            activities.Add(activity);
                        }
                    }

                    activities = activities.OrderBy(a => a.OrderNumber).ToList();

                    var validDate = searchModel.ValidDate ?? DateTime.Now;
                    activities = _checklistUtilityService.FilterActivitiesByValidDate(activities, validDate);

                    if (searchModel.Milestone?.Count > 0)
                        activities = _checklistUtilityService.FilterActivitiesByMilestone(activities, searchModel.Milestone);

                    if (searchModel.EnterpriseTerms?.Count > 0)
                        activities = _checklistUtilityService.FilterActivitiesByEnterpriseTerms(activities, searchModel.EnterpriseTerms);

                    if (searchModel.Category?.Count > 0)
                        activities = _checklistUtilityService.FilterActivitiesByCategory(activities, searchModel.Category);

                    if (searchModel.ActivityType?.Count > 0)
                        activities = _checklistUtilityService.FilterActivitiesByActivityType(activities, searchModel.ActivityType);

                    if (searchModel.Owner?.Count > 0)
                    {
                        activities = _checklistUtilityService.FilterActivitiesByOwner(activities, searchModel.Owner);
                    }
                    //else if (_options.Value.DefaultOwnerFilter.Any())
                    //{
                    //    activities = _checklistUtilityService.FilterActivitiesByOwner(activities, _options.Value.DefaultOwnerFilter);
                    //}

                    if (searchModel.MetadataId.Any())
                    {
                        var selectedMetadata = _metadataService.GetMetadatas(searchModel.MetadataId);
                        activities = _checklistUtilityService.FilterActivitiesByMetadataId(activities, selectedMetadata);
                    }
                }
            }

            List<Activity> activitiesWithDrafts = new List<Activity>();
            ConnectActivitiesWithDraftActivities(activities, draftActivities, activitiesWithDrafts);

            if (_options.Value.GroupByCategory)
            {
                try
                {
                    return activitiesWithDrafts.GroupBy(a => a.Category?.TrimEnd(':', ',')).ToDictionary(x => x.Key, x => x.ToList());
                }
                catch (Exception)
                {
                    _logger.LogError("Error grouping checkpoint by category.");
                }
            }
            return new Dictionary<string, List<Activity>>() { { String.Empty, activitiesWithDrafts } };
        }

        private Activity GetDraftActivity(int id)
        {
            var query = _dbContext.Activity
                .Include(a => a.Actions)
                .Where(a => a.PublishedActivity_Id == id && a.Status == StatusForActivity.Draft);

            return query.FirstOrDefault();
        }

        private async Task<List<Activity>> GetDraftActivities(string processCategory)
        {
            return await _dbContext.Activity.Include(a => a.Actions).Where(a => a.Status == StatusForActivity.Draft && a.Processcategory == processCategory).ToListAsync();
        }

        public async Task<List<Activity>> GetDraftActivities()
        {
            return await _dbContext.Activity.Include(a => a.Actions).Where(a => a.Status == StatusForActivity.Draft).ToListAsync();
        }

        public List<string> GetProcessCategoriesOnReferenceId(string referenceId)
        {
            var queryResult = from a in _dbContext.Activity
                              where a.ReferenceId == referenceId
                              select a.Processcategory;

            return queryResult.Distinct().ToList();
        }

        public Category GetCategoryByReferenceId(string referenceId)
        {
            var queryResult = from a in _dbContext.Activity
                              where a.ReferenceId == referenceId
                              select a.Category;

            var categoryValue = queryResult.FirstOrDefault();

            return GetCategoryByValue(categoryValue);
        }

        private Category GetCategoryByValue(string categoryValue)
        {
            var queryResult = from a in _dbContext.Categories
                              where a.Description == categoryValue
                              select a;

            return queryResult.FirstOrDefault();
        }

        private static void ConnectActivitiesWithDraftActivities(List<Activity> activities, List<Activity> draftActivities, List<Activity> activitiesWithDrafts)
        {
            foreach (var activity in activities)
            {
                if (activity.SubActivities != null)
                    ConnectActivitiesWithDraftActivities(activity.SubActivities, draftActivities, activitiesWithDrafts);

                activity.DraftActivity = draftActivities.FirstOrDefault(da => da.PublishedActivity_Id == activity.Id);
                if (activity.DraftActivity != null) { activity.DraftActivity.SubActivities = activity.SubActivities; }

                if (activity.ParentReferenceId.IsNullOrEmpty())
                    activitiesWithDrafts.Add(activity);
            }
        }

        private IQueryable<Activity> GetAllActivitiesFilteredByProcessCategory(string processCategory)
        {
            var query = _dbContext.Activity
                .Include(a => a.Actions)
                .Include(a => a.Metadata).ThenInclude(mi => mi.MetadataItem).ThenInclude(t => t.Type)
                .Where(a => a.Processcategory == processCategory &&
                            a.Status == StatusForActivity.Published);

            return query;
        }

        private IQueryable<Activity> GetUnpublishedActivitiesFilteredByProcessCategory(string processCategory)
        {
            var query = _dbContext.Activity
                .Include(a => a.Actions)
                .Where(a => a.Processcategory == processCategory &&
                            a.Status == StatusForActivity.Draft && a.PublishedActivity_Id == null);

            return query;
        }

        //todo move to Actions service
        public async Task<IEnumerable<ActionTypeCodesWithValuesViewModel>> GetActions()
        {
            var actions = _dbContext.Action.AsEnumerable()
                .GroupBy(c => c.ActionTypeCode)
                .Select(g => g.First())
                .Select(ac => new ActionTypeCodesWithValuesViewModel()
                { ActionType = ac.ActionType.Trim(), ActionTypeCode = ac.ActionTypeCode.Trim() }).ToList();

            return actions;
        }

        public async Task DeleteDraft(int id)
        {
            var draftToDelete = await GetActivity(id);

            if (draftToDelete.Status == StatusForActivity.Draft)
            {
                _dbContext.Activity.Remove(draftToDelete);
                await _dbContext.SaveChangesAsync();
                _logger.LogInformation("{@DeletedDraft}", new
                {
                    draftToDelete.Id,
                    draftToDelete.ReferenceId,
                    draftToDelete.Name,
                    ProcessCategory = draftToDelete.Processcategory,
                    ParentReferenceId = draftToDelete.Activity_Id,
                    draftToDelete.PublishedActivity_Id,
                });
            }
        }

        public async Task<Activity> UpdatePublishedActivity(int draftActivityId)
        {
            var draftActivity = await GetActivity(draftActivityId);
            UpdateIdsAndForeignKeys(draftActivity);

            if (draftActivity != null)
            {
                var activitiesToUpdate = await GetDraftActivitiesIdByReferenceId(draftActivity.ReferenceId);

                foreach (var activityId in activitiesToUpdate)
                {
                    var draftActivityToUpdate = await GetActivity(activityId);
                    var publishedActivityToUpdate = await GetActivity(draftActivityToUpdate.PublishedActivity_Id);

                    _dbContext.Activity.Attach(draftActivityToUpdate).State = EntityState.Modified;

                    if (publishedActivityToUpdate != null)
                    {
                        _dbContext.Activity.Attach(publishedActivityToUpdate).State = EntityState.Modified;
                        await AddArchivedActivity(publishedActivityToUpdate);

                        var draftMetadata = draftActivityToUpdate.Metadata;
                        var publishedActivitySubactivities = publishedActivityToUpdate.SubActivities;
                        var publishedActivityId = publishedActivityToUpdate.Id;

                        var updatedActivity = publishedActivityToUpdate.DeepCopy(draftActivityToUpdate);
                        _dbContext.Activity.Remove(draftActivityToUpdate);

                        updatedActivity.SubActivities = publishedActivitySubactivities;
                        updatedActivity.Id = publishedActivityId;
                        updatedActivity.Status = StatusForActivity.Published;
                        updatedActivity.PublishedActivity_Id = null;
                        if (!string.IsNullOrWhiteSpace(draftActivityToUpdate.ParentReferenceId))
                        {
                            updatedActivity.Activity_Id = publishedActivityToUpdate.Activity_Id;
                        }

                        _dbContext.Entry(publishedActivityToUpdate).CurrentValues.SetValues(updatedActivity);

                        publishedActivityToUpdate.UpdatePublishedAction(updatedActivity.Actions);

                        publishedActivityToUpdate.EnterpriseTerms = updatedActivity.EnterpriseTerms;
                        publishedActivityToUpdate.LawReferences = updatedActivity.LawReferences;
                        publishedActivityToUpdate.Metadata = new List<ActivityMetadata>();

                        foreach (var metadata in draftMetadata)
                        {
                            publishedActivityToUpdate.Metadata.Add(new ActivityMetadata()
                            {
                                ActivityId = publishedActivityToUpdate.Id,
                                MetadataId = metadata.MetadataId,
                                MetadataTypeId = metadata.MetadataTypeId
                            }
                            );
                        }

                        await _dbContext.SaveChangesAsync();
                        _logger.LogDebug("Oppdatert aktivitet {aktivitetId}", activityId);
                    }
                    else
                    {
                        draftActivityToUpdate.Status = StatusForActivity.Published;
                        draftActivityToUpdate.Updated = DateTime.Now;
                        await _dbContext.SaveChangesAsync();
                        _logger.LogDebug("Lagre ny aktivitet {aktivitetId}", activityId);
                    }
                }

                var publishedDraftId = draftActivity.PublishedActivity_Id ?? draftActivityId;
                return await _dbContext.Activity.FirstOrDefaultAsync(a => a.Id == publishedDraftId);
            }

            return null;
        }

        private async Task AddArchivedActivity(Activity publishedActivityToUpdate)
        {
            var archivedActivity = new Activity().DeepCopy(publishedActivityToUpdate);
            var metadataToUpdate = publishedActivityToUpdate.Metadata;
            archivedActivity.Status = StatusForActivity.Archived;
            archivedActivity.PublishedActivity_Id = publishedActivityToUpdate.Id;
            archivedActivity.SubActivities = null;
            archivedActivity.Metadata = new List<ActivityMetadata>();
            publishedActivityToUpdate.Metadata = null;
            archivedActivity.Actions = null;

            foreach (var metadata in metadataToUpdate)
            {
                archivedActivity.Metadata.Add(new ActivityMetadata
                {
                    MetadataTypeId = metadata.MetadataTypeId,
                    MetadataId = metadata.MetadataId,
                });
            }

            UpdateIdsAndForeignKeys(archivedActivity);

            _dbContext.Activity.Add(archivedActivity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<Activity> UpdateDraftActivity(Activity updatedActivity)
        {
            if (updatedActivity.ReferenceId != null)
            {
                var activitiesToUpdate = await GetDraftActivitiesIdByReferenceId(updatedActivity.ReferenceId);

                _logger.LogDebug($"Number of activities to update {activitiesToUpdate.Count}");
                var count = 1;
                foreach (var activityId in activitiesToUpdate)
                {
                    _logger.LogDebug($"Start - Update draft {count}/{activitiesToUpdate.Count}");

                    var activityToModify = await GetActivity(activityId);
                    if (activityToModify != null && activityToModify.Status == StatusForActivity.Draft)
                    {
                        _logger.LogInformation("{@DraftToModify}", new
                        {
                            activityToModify.Id,
                            activityToModify.ReferenceId,
                            activityToModify.Name,
                            ProcessCategory = activityToModify.Processcategory,
                            ParentReferenceId = activityToModify.Activity_Id,
                            activityToModify.PublishedActivity_Id,
                        });

                        _dbContext.Attach(activityToModify).State = EntityState.Modified;

                        var thisUpdatedActivity = updatedActivity.DeepCopy(updatedActivity);
                        thisUpdatedActivity.Id = activityToModify.Id;
                        thisUpdatedActivity.PublishedActivity_Id = activityToModify.PublishedActivity_Id;
                        thisUpdatedActivity.Processcategory = activityToModify.Processcategory;

                        thisUpdatedActivity.ParentReferenceId = activityToModify.ParentReferenceId;
                        thisUpdatedActivity.Activity_Id = activityToModify.Activity_Id;

                        LogUpdates(activityToModify, thisUpdatedActivity);

                        _dbContext.Entry(activityToModify).CurrentValues.SetValues(thisUpdatedActivity);

                        UpdateIdsAndForeignKeys(thisUpdatedActivity);

                        activityToModify.UpdateLawReferences(thisUpdatedActivity.LawReferences);

                        if (activityId == updatedActivity.Id || activityToModify.Processcategory == updatedActivity.Processcategory)
                        {
                            activityToModify.UpdateActions(thisUpdatedActivity.Actions);
                            activityToModify.UpdateMetadata(thisUpdatedActivity.Metadata, updatedActivity.Id);
                            activityToModify.UpdateEnterpriseTerms(updatedActivity.EnterpriseTerms);
                            thisUpdatedActivity.OrderNumber = activityToModify.OrderNumber;
                        }

                        await _dbContext.SaveChangesAsync();
                    }
                }
            }
            else
            {
                var activityToModify = await GetActivity(updatedActivity.Id);
                if (activityToModify != null && activityToModify.Status == StatusForActivity.Draft)
                {
                    _dbContext.Attach(activityToModify).State = EntityState.Modified;
                    _dbContext.Entry(activityToModify).CurrentValues.SetValues(updatedActivity);

                    activityToModify.UpdateActions(updatedActivity.Actions);
                    activityToModify.UpdateLawReferences(updatedActivity.LawReferences);
                    activityToModify.UpdateEnterpriseTerms(updatedActivity.EnterpriseTerms);

                    await _dbContext.SaveChangesAsync();
                }
            }

            return await GetActivity(updatedActivity.Id);
        }

        private void LogUpdates(Activity activityToModify, Activity thisUpdatedActivity)
        {
            //LawRef
            var activityToModifyLawRefList = activityToModify.LawReferences.Select(m => $"Description: {m.LawReferenceDescription} Url: {m.LawReferenceUrl}").ToList();
            var thisUpdatedActivityLawRefList = thisUpdatedActivity.LawReferences.Select(m => $"Description: {m.LawReferenceDescription} Url: {m.LawReferenceUrl}").ToList();
            var lawRefChanges = activityToModifyLawRefList.Except(thisUpdatedActivityLawRefList).Any();

            //EnterpriseTerms
            var activityToModifyEnterpriseTermsList = activityToModify.EnterpriseTerms.Select(m => $"Id: {m.Id} Description: {m.Code}").ToList();
            var thisUpdatedActivityentErpriseTermsList = thisUpdatedActivity.EnterpriseTerms.Select(m => $"Id: {m.Id} Description: {m.Code}").ToList();
            var enterpriseTermsChanges = activityToModifyEnterpriseTermsList.Except(thisUpdatedActivityentErpriseTermsList).Any();

            //Actions
            var activityToModifyActionList = activityToModify.Actions.Select(a => $"{a.ActionValue.ToString()}: {a.ActionTypeCode} - {a.ActionType}").ToList();
            var thisUpdatedActivityActionList = thisUpdatedActivity.Actions.Select(a => $"{a.ActionValue.ToString()}: {a.ActionTypeCode} - {a.ActionType}").ToList();
            var ActionChanges = activityToModifyActionList.Except(thisUpdatedActivityActionList).Any();

            // Metadata
            var activityToModifyMetadataList = activityToModify.Metadata.Select(m => $"{m.MetadataTypeId}-{m.MetadataId}").ToList();
            var thisUpdatedActivityMetadataList = thisUpdatedActivity.Metadata.Select(m => $"{m.MetadataTypeId}-{m.MetadataId}").ToList();
            var metadataChanges = activityToModifyMetadataList.Except(thisUpdatedActivityMetadataList).Any();

            //if (activityToModify.Actions != thisUpdatedActivity.Actions) updates.Add(thisUpdatedActivity.Actions);
            //if (activityToModify.Metadata != thisUpdatedActivity.Metadata) updates.Add(thisUpdatedActivity.Metadata);

            _logger.LogInformation("{@CheckpointChanges}", new
            {
                Id = activityToModify.Id,
                ReferenceId = activityToModify.ReferenceId,
                Name = activityToModify.Name != thisUpdatedActivity.Name ? thisUpdatedActivity.Name : null,
                NameNynorsk = activityToModify.NameNynorsk != thisUpdatedActivity.NameNynorsk ? thisUpdatedActivity.NameNynorsk : null,
                Municipality = activityToModify.Municipality != thisUpdatedActivity.Municipality ? thisUpdatedActivity.Municipality : null,
                ActivityType = activityToModify.ActivityType != thisUpdatedActivity.ActivityType ? thisUpdatedActivity.ActivityType : null,
                Description = activityToModify.Description != thisUpdatedActivity.Description ? thisUpdatedActivity.Description : null,
                DescriptionNynorsk = activityToModify.DescriptionNynorsk != thisUpdatedActivity.DescriptionNynorsk ? thisUpdatedActivity.DescriptionNynorsk : null,
                Documentation = activityToModify.Documentation != thisUpdatedActivity.Documentation ? thisUpdatedActivity.Documentation : null,
                DocumentationNynorsk = activityToModify.DocumentationNynorsk != thisUpdatedActivity.DocumentationNynorsk ? thisUpdatedActivity.DocumentationNynorsk : null,
                Image = activityToModify.Image?.ToString() != thisUpdatedActivity.Image?.ToString() ? thisUpdatedActivity.Image.ToString() : null,
                ImageDescription = activityToModify.ImageDescription != thisUpdatedActivity.ImageDescription ? thisUpdatedActivity.ImageDescription : null,
                Milestone = activityToModify.Milestone != thisUpdatedActivity.Milestone ? thisUpdatedActivity.Milestone : null,
                Rule = activityToModify.Rule != thisUpdatedActivity.Rule ? thisUpdatedActivity.Rule : null,
                ValidFrom = activityToModify.ValidFrom != thisUpdatedActivity.ValidFrom ? thisUpdatedActivity.ValidFrom?.ToString("dd/MM/yyyy") : null,
                ValidTo = activityToModify.ValidTo != thisUpdatedActivity.ValidTo ? thisUpdatedActivity.ValidTo?.ToString("dd/MM/yyyy") : null,
                OrderNumber = activityToModify.OrderNumber != thisUpdatedActivity.OrderNumber ? thisUpdatedActivity.OrderNumber.ToString() : null,
                LawReference = lawRefChanges ? thisUpdatedActivityLawRefList : null,
                EnterpriseTerms = enterpriseTermsChanges ? thisUpdatedActivityentErpriseTermsList : null,
                Actions = ActionChanges ? thisUpdatedActivityActionList : null,
                Metadata = metadataChanges ? thisUpdatedActivityMetadataList : null,
            });
        }

        public async Task<Activity> GetActivity(int? activityId)
        {
            var queryResult = from a in _dbContext.Activity
                              .Include("SubActivities")
                              .Include("LawReferences")
                              .Include(a => a.Actions)
                              .Include("EnterpriseTerms")
                              .Include(a => a.Metadata)
                              .ThenInclude(m => m.MetadataItem)
                              .ThenInclude(m => m.Type)
                              where a.Id == activityId
                              orderby a.OrderNumber
                              select a;

            return await queryResult.AsSplitQuery().SingleOrDefaultAsync();
        }

        public async Task<string> GetActivityReferenceId(int? activityId)
        {
            try
            {
                var queryResult = from a in _dbContext.Activity
                                  where a.Id == activityId
                                  select a.ReferenceId;

                return await queryResult.SingleOrDefaultAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Get activity referenceId by Id {activityId}");
                return null;
            }
        }

        private async Task<List<Activity>> GetPublishedActivitiesByReferenceId(string referenceId)
        {
            try
            {
                var queryResult = from a in _dbContext.Activity
                        .Include("SubActivities")
                        .Include("LawReferences")
                        .Include("Actions")
                        .Include("EnterpriseTerms")
                        .Include(a => a.Metadata)
                        .ThenInclude(m => m.MetadataItem)
                        .ThenInclude(m => m.Type)
                                  where a.ReferenceId == referenceId
                                        && a.Status == StatusForActivity.Published
                                  select a;

                return await queryResult.AsSplitQuery().ToListAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Get published activities by ReferenceId {referenceId}");
                return new List<Activity>();
            }
        }

        private async Task<List<int>> GetDraftActivitiesIdByReferenceId(string referenceId)
        {
            try
            {
                var queryResult = from a in _dbContext.Activity
                                  where a.ReferenceId == referenceId && a.Status == StatusForActivity.Draft
                                  select a.Id;

                return await queryResult.ToListAsync();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Get draft activities id by referenceId {referenceId}");
                return new List<int>();
            }
        }

        public async Task<Activity> CreateDraftForActivity(int id)
        {
            try
            {
                var activityReferenceIdToCreateDraftFrom = await GetActivityReferenceId(id);
                var relatedActivitiesToCreateDraftFrom = await GetPublishedActivitiesByReferenceId(activityReferenceIdToCreateDraftFrom);

                _logger.LogDebug($"Number of activities with the same referenceId {activityReferenceIdToCreateDraftFrom}: {relatedActivitiesToCreateDraftFrom?.Count}");

                foreach (var activity in relatedActivitiesToCreateDraftFrom)
                {
                    _logger.LogDebug($"Start - Create draft for activity Id: {activity.Id}, ReferenceId: {activity.ReferenceId}");
                    if (activity.PublishedActivity_Id == null && activity.Status != StatusForActivity.Draft && !DraftAlreadyExists(activity.Id))
                    {
                        var draft = activity.DeepCopy(activity);

                        UpdateDraftFields(draft, activity.Id, activity.Actions);
                        _dbContext.Activity.Add(draft);
                        await _dbContext.SaveChangesAsync();

                        _logger.LogInformation("{@Draft}", new
                        {
                            draft.Id,
                            draft.ReferenceId,
                            draft.Name,
                            ProcessCategory = draft.Processcategory,
                            ParentReferenceId = draft.Activity_Id,
                            draft.PublishedActivity_Id,
                        });
                    }
                    else
                    {
                        _logger.LogDebug($"Draft already exists");
                    }
                }
                return GetDraftActivity(id);
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Creating draft for activity - Id: {id}");
                return null;
            }
        }

        public async Task<List<Activity>> CreateDraftsForNewCheckpoit(CreateActivityViewModel model)
        {
            var activities = new List<Activity>();

            var newReferenceId = CreateNewReferenceId(model);

            _logger.LogDebug($"Start- Create drafts. Number of drafts to create: {model.ProcessCategories.Count}");
            var countDrafts = 1;
            foreach (var processCategory in model.ProcessCategories)
            {
                _logger.LogDebug($"Start - Create draft nr. {countDrafts}/{model.ProcessCategories.Count}. ReferenceId {newReferenceId}, ProcessCategory {processCategory}");

                var draft = new Activity(model);
                draft.Processcategory = processCategory;
                draft.OrderNumber = GetOrderNumber(model.Category, processCategory);

                if (draft.ParentReferenceId != null)
                {
                    var parent = GetCurrentActiviy(draft.ParentReferenceId, processCategory);
                    draft.Activity_Id = parent.Id;
                    _logger.LogDebug($"Set parent - draft.Activity_Id:  {parent.Id}");
                }
                draft.ReferenceId = newReferenceId;
                _logger.LogDebug($"Draft - ");
                draft.Metadata = await _metadataService.AddOrUpdateMetadataAsync(model.Metadata);

                _dbContext.Activity.Add(draft);

                await _dbContext.SaveChangesAsync();

                _logger.LogDebug($"Finished - Create draft nr. {countDrafts}/{model.ProcessCategories.Count}. ReferenceId {newReferenceId}, ProcessCategory {processCategory}");

                _logger.LogInformation("{@DraftNewCheckpoint}", new
                {
                    draft.ReferenceId,
                    draft.Name,
                    ProcessCategory = draft.Processcategory,
                    ParentReferenceId = draft.Activity_Id,
                });

                activities.Add(draft);

                countDrafts++;
            }

            return activities;
        }

        private int GetOrderNumber(Category category, string processCategory)
        {
            var orderNumbers = new List<int>();

            if (_options.Value.GroupByCategory)
            {
                var queryResult = from a in _dbContext.Activity
                                  where a.Category == category.Description && a.Processcategory == processCategory
                                  select a.OrderNumber;

                orderNumbers = queryResult.ToList();
            }
            else
            {
                var queryResult = from a in _dbContext.Activity
                                  where a.Processcategory == processCategory
                                  select a.OrderNumber;

                orderNumbers = queryResult.ToList();
            }

            orderNumbers.Sort();
            int latestOrderNumber = orderNumbers.LastOrDefault();

            return latestOrderNumber + 1;
        }

        private Activity GetCurrentActiviy(string referenceId, string processCategory)
        {
            int[] customOrder = { 1, 0, 2 };

            var queryResult = from a in _dbContext.Activity
                              where a.ReferenceId == referenceId &&
                                    a.Processcategory == processCategory
                              select a;

            var orderedQueryable = queryResult.ToList().OrderBy(i => IndexOf(customOrder, i.Id));

            return orderedQueryable.FirstOrDefault();
        }

        private string CreateNewReferenceId(CreateActivityViewModel model)
        {
            _logger.LogDebug("Start - Create new referenceId");
            var referenceIdBasedOnCategory = _options.Value?.CheckpointReferenceIdBasedOnCategory;
            if (referenceIdBasedOnCategory != null && referenceIdBasedOnCategory == true)
            {
                _logger.LogDebug($"Create new referenceId based on category");
                return CreateNewReferenceId(model.Category);
            }

            // Else - Løpenummer..
            try
            {
                _logger.LogDebug("Create new referenceId from serial number.");
                var queryResult = _dbContext.Activity.Select(a => new KeyValuePair<int, string>(a.Id, a.ReferenceId));

                var idAndReferenceIds = queryResult.ToList();
                var listReferenceIds = new List<int>();

                foreach (var item in idAndReferenceIds)
                {
                    try
                    {
                        var referenceIdNumber = int.Parse(item.Value);
                        listReferenceIds.Add(referenceIdNumber);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, $"Error - Parsing referenceId to int: Activity Id - {item.Key} - ReferenceId {item.Value}");
                    }
                }

                var latestUsedReferenceIdSerialNumber = listReferenceIds.OrderBy(o => o).LastOrDefault();
                var newReferenceId = latestUsedReferenceIdSerialNumber + 1;

                _logger.LogDebug($"Creating new referenceId - Latest used referenceId serial number: {latestUsedReferenceIdSerialNumber}, new ReferenceId: {newReferenceId}");

                return newReferenceId.ToString();
            }
            catch (Exception e)
            {
                _logger.LogError(e, $"Error creating new referenceId");
                throw;
            }
        }

        private string CreateNewReferenceId(Category category)
        {
            if (category?.Id == null)
            {
                _logger.LogError($"Error creating new referenceId - Category Id is null");
                return null;
            } // Todo default tema, id? Og hva om category.id ikke er satt..

            _logger.LogInformation($"Creating new referenceId based on category Id: {category.Id} Description: {category.Description}");

            // Henter alle referenceId p� activities som har et gitt tema.
            var queryResult = from a in _dbContext.Activity
                              where a.Category.Contains(category.Description)
                              select a.ReferenceId;

            var result = queryResult.Distinct().ToList();
            if (!result.Any()) return category.Id + "." + 1;

            var serialNumberUsed = new List<int>();
            foreach (var referenceId in result)
            {
                var resultSplit = referenceId.Split(".");
                if (resultSplit[0] == category.Id)
                {
                    serialNumberUsed.Add(int.Parse(resultSplit[1]));
                }
            }

            serialNumberUsed.Sort();
            var latestUsedSerialNumber = serialNumberUsed.LastOrDefault();

            var newReferenceId = category.Id + "." + (latestUsedSerialNumber + 1);

            if (_dbContext.Activity.Any(a => a.ReferenceId == newReferenceId))
            {
                _logger.LogError("Creating new referenceId " + newReferenceId + " - failed");
            }
            else
            {
                _logger.LogInformation($"New referenceId {newReferenceId}");
            }

            return newReferenceId;
        }

        private bool DraftAlreadyExists(int id)
        {
            var activity = _dbContext.Activity.Where(a => a.PublishedActivity_Id == id && a.Status == StatusForActivity.Draft).FirstOrDefault();
            return activity != null;
        }

        private void UpdateDraftFields(Activity draft, int publishedActivityId, List<Action> actions)
        {
            UpdateIdsAndForeignKeys(draft);
            draft.Updated = DateTime.Now;
            draft.PublishedActivity_Id = publishedActivityId;
            draft.Status = StatusForActivity.Draft;
            draft.SubActivities = null;

            foreach (var draftAction in draft.Actions)
            {
                draftAction.PublishedAction_Id = actions
                    .Where(a => a.ActionValue == draftAction.ActionValue &&
                                a.ActionTypeCode == draftAction.ActionTypeCode).Select(a => a.Id).FirstOrDefault();
            }

            foreach (var metadata in draft.Metadata)
            {
                metadata.MetadataItem = null;
            }

            draft.Id = 0;
        }

        private void UpdateIdsAndForeignKeys(Activity activity)
        {
            _dbContext.DetachEntity(activity);
            if (activity.Actions != null)
                _dbContext.DetachEntities(activity.Actions);
            if (activity.LawReferences != null)
                _dbContext.DetachEntities(activity.LawReferences);
            if (activity.EnterpriseTerms != null)
                _dbContext.DetachEntities(activity.EnterpriseTerms);
            if (activity.Metadata != null)
            {
                _dbContext.DetachEntities(activity.Metadata);

                foreach (var metadata in activity.Metadata)
                {
                    if (metadata.MetadataItem != null)
                    {
                        _dbContext.DetachEntity(metadata.MetadataItem);
                        if (metadata.MetadataItem.Type != null)
                        {
                            _dbContext.DetachEntity(metadata.MetadataItem.Type);
                        }
                    }
                }
            }
        }
    }
}