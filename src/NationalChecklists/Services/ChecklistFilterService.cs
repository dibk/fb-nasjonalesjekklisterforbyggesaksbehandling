using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;

namespace NationalChecklists.Services
{
    public interface IChecklistFilterService
    {
        bool IsEnterpriseTermsListNullOrEmpty(List<string> enterpriseTerms);
        List<Activity> FilterActivitiesByMilestone(List<Activity> activities, List<string> milestone);
        List<Activity> FilterActivitiesByEnterpriseTerms(List<Activity> activities, List<string> enterpriseTerms);
        List<Activity> FilterActivitiesByCategory(List<Activity> activities, List<string> categories);
        List<Activity> FilterActivitiesByActivityType(List<Activity> activities, List<string> activityTypes);
        List<Activity> FilterActivitiesByValidDate(List<Activity> activities, DateTime validDate);
        List<Activity> FilterActivitiesByOwner(List<Activity> activities, List<string> owner);
        List<Activity> FilterActivitiesByMetadataId(List<Activity> activities, List<Metadata> metadata);
        List<Activity> FilterActivitiesByMetadataTypeId(List<Activity> activities, List<MetadataType> metadataTypes);
        bool IsValidOnDate(Activity activity, DateTime validDate);
        List<Activity> FilterActivitiesByLastUpdated(List<Activity> activities, DateTime updatedAfterDate);
    }

    public class ChecklistFilterService : IChecklistFilterService
    {

        public bool IsEnterpriseTermsListNullOrEmpty(List<string> enterpriseTerms)
        {
            return enterpriseTerms == null || (enterpriseTerms != null && !enterpriseTerms.Any()) || enterpriseTerms == null;
        }

        public List<Activity> FilterActivitiesByMilestone(List<Activity> activities, List<string> milestone)
        {
            var filteredActivities = new List<Activity>();

            foreach (var activity in activities)
                if (HasMilestone(activity, milestone))
                    filteredActivities.Add(activity);

            return filteredActivities;
        }

        private bool HasMilestone(Activity activity, List<string> milestones)
        {
            if (activity.SubActivities != null)
                activity.SubActivities = FilterActivitiesByMilestone(activity.SubActivities, milestones);

            return milestones.Any(m => activity.Milestone != null && m.Equals(activity.Milestone.TrimEnd(':', ','), StringComparison.CurrentCultureIgnoreCase)) || activity.SubActivities != null && activity.SubActivities.Any();
        }

        public List<Activity> FilterActivitiesByEnterpriseTerms(List<Activity> activities, List<string> enterpriseTerms)
        {
            var filteredActivities = new List<Activity>();

            foreach (var activity in activities)
                if (HasEnterpriseTerm(activity, enterpriseTerms) && !filteredActivities.Contains(activity))
                    filteredActivities.Add(activity);

            return filteredActivities;
        }

        private bool HasEnterpriseTerm(Activity activity, List<string> enterpriseTerms)
        {
            var enterpriseTermsFromActivity = new List<EnterpriseTerm>();

            if (activity.SubActivities != null)
                activity.SubActivities = FilterActivitiesByEnterpriseTerms(activity.SubActivities, enterpriseTerms);

            foreach (var enterpriseTerm in enterpriseTerms)
            {
                var enterpriseTermFromActivity = activity.EnterpriseTerms?.FirstOrDefault(et => et.Code.Equals(enterpriseTerm));
                if (enterpriseTermFromActivity != null)
                    enterpriseTermsFromActivity.Add(enterpriseTermFromActivity);
            }

            return enterpriseTermsFromActivity.Any() || activity.SubActivities != null && activity.SubActivities.Any();
        }

        public List<Activity> FilterActivitiesByCategory(List<Activity> activities, List<string> categories)
        {
            var filteredActivities = new List<Activity>();

            foreach (var activity in activities)
                if (HasCategory(activity, categories))
                    filteredActivities.Add(activity);

            return filteredActivities;
        }

        public List<Activity> FilterActivitiesByOwners(List<Activity> activities, List<string> owners)
        {
            return activities.Where(activity => HasOwner(activity, owners)).ToList();
        }

        private bool HasCategory(Activity activity, List<string> categories)
        {
            if (categories.Any())
            {
                if (activity.SubActivities?.Count > 0)
                    activity.SubActivities = FilterActivitiesByCategory(activity.SubActivities, categories);

                return categories.Any(c => activity.Category != null && c.Equals(activity.Category.TrimEnd(':', ','), StringComparison.CurrentCultureIgnoreCase)) || activity.SubActivities != null && activity.SubActivities.Any();
            }
            return false;
        }

        private bool HasOwner(Activity activity, List<string> owners)
        {
            if (!owners.Any()) return false;
            if (activity.SubActivities != null && activity.SubActivities.Any())
                activity.SubActivities = FilterActivitiesByOwners(activity.SubActivities, owners);

            return owners.Any(c => activity.Municipality != null && c.Equals(activity.Municipality, StringComparison.CurrentCultureIgnoreCase)) || activity.SubActivities != null && activity.SubActivities.Any();
        }

        private bool HasMetadata(Activity activity, List<Metadata> metadata)
        {
            if (activity.SubActivities != null)
                activity.SubActivities = FilterActivitiesByMetadataId(activity.SubActivities, metadata);

            var metadatas = metadata.Select(m => activity.Metadata?
                .Find(activityMetadata => activityMetadata.MetadataId.Equals(m.Id)))
                .Where(metadataFromActivity => metadataFromActivity != null)
                .ToList();


            return metadatas.Any() || activity.SubActivities != null && activity.SubActivities.Any();
        }

        private bool HasMetadataType(Activity activity, List<MetadataType> metadataTypes)
        {
            if (activity.SubActivities != null)
                activity.SubActivities = FilterActivitiesByMetadataTypeId(activity.SubActivities, metadataTypes);

            var result = metadataTypes.Select(m => activity.Metadata?
                    .Find(activityMetadata => activityMetadata.MetadataTypeId.Equals(m.Id)))
                .Where(metadataFromActivity => metadataFromActivity != null)
                .ToList();

            return result.Any() || activity.SubActivities != null && activity.SubActivities.Any();
        }

        
        public List<Activity> FilterActivitiesByActivityType(List<Activity> activities, List<string> activityTypes)
        {
            return activities.Where(activity => HasActivityType(activity, activityTypes)).ToList();
        }

        public List<Activity> FilterActivitiesByOwner(List<Activity> activities, List<string> owners)
        {
            return activities.Where(activity => HasOwner(activity, owners)).ToList();
        }

        public List<Activity> FilterActivitiesByMetadataId(List<Activity> activities, List<Metadata> metadata)
        {
            var metadataGroupedByType = metadata.GroupBy(m => m.TypeId);

            foreach (var metadataGroup in metadataGroupedByType)
            {
                activities = activities?.Where(activity => HasMetadata(activity, metadataGroup.Select(g => g).ToList())).ToList();
            }

            return activities;
        }

        public List<Activity> FilterActivitiesByMetadataTypeId(List<Activity> activities, List<MetadataType> metadataTypes)
        {
            activities = activities?.Where(activity => HasMetadataType(activity, metadataTypes)).ToList();

            return activities;
        }


        public List<Activity> FilterActivitiesByValidDate(List<Activity> activities, DateTime validDate)
        {
            var filteredActivities = new List<Activity>();

            foreach (var activity in activities)
            {
                if (activity.SubActivities != null)
                {
                    var validSubActivities = FilterActivitiesByValidDate(activity.SubActivities, validDate);
                    activity.SubActivities = validSubActivities;
                }

                bool isValidOnDate = IsValidOnDate(activity, validDate);

                if (isValidOnDate)
                {
                    filteredActivities.Add(activity);
                }
            }

            return filteredActivities;
        }

        public List<Activity> FilterActivitiesByLastUpdated(List<Activity> activities, DateTime updatedAfter)
        {
            var filteredActivities = new List<Activity>();

            foreach (var activity in activities)
            {
                if (activity.SubActivities != null)
                {
                    var updatedSubActivities = FilterActivitiesByLastUpdated(activity.SubActivities, updatedAfter);
                    activity.SubActivities = updatedSubActivities;
                }

                var isUpdatedAfterSelectedDate = activity.Updated.CompareTo(updatedAfter) > 0;

                if (isUpdatedAfterSelectedDate)
                {
                    filteredActivities.Add(activity);
                }
            }

            return filteredActivities;
        }

        public bool IsValidOnDate(Activity activity, DateTime validDate)
        {
            // Compare date 
            // 0 = Selected date
            // 0 < = earlier than selected date
            // 0 > = later than selected date
            var valueValidFrom = activity.ValidFrom == null ? 0 : DateTime.Compare(validDate.Date, activity.ValidFrom.Value.Date);
            var valueValidTo = activity.ValidTo == null ? 0 : DateTime.Compare(validDate.Date, activity.ValidTo.Value.Date);

            var isValidOnDate = false;

            if (activity.ValidTo == null)
            {
                if (activity.ValidFrom == null)
                {
                    isValidOnDate = true;
                }
                else
                {
                    if (valueValidFrom >= 0)
                    {
                        isValidOnDate = true;
                    }
                }
            }
            else
            {
                if (activity.ValidFrom == null)
                {
                    if (valueValidTo <= 0)
                    {
                        isValidOnDate = true;
                    }
                }
                else
                {
                    if (valueValidFrom >= 0 && valueValidTo <= 0)
                    {
                        isValidOnDate = true;
                    }
                }
            }

            return isValidOnDate;
        }

        private bool HasActivityType(Activity activity, List<string> activityTypes)
        {
            if (activity.SubActivities != null && activity.SubActivities.Any())
                activity.SubActivities = FilterActivitiesByActivityType(activity.SubActivities, activityTypes);

            return activityTypes.Any(at => at.Equals(activity.ActivityType?.Trim(), StringComparison.CurrentCultureIgnoreCase)) || activity.SubActivities != null && activity.SubActivities.Any();
        }
    }
}
