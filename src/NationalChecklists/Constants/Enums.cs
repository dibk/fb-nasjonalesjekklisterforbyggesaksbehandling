﻿namespace NationalChecklists.Constants
{
    public enum ContentType
    {
        Text = 1,
        Html = 2
    }
}
