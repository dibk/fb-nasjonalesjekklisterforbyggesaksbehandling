﻿namespace NationalChecklists.Constants
{
    public static class LanguageCode
    {
        public static string NorskBokmål = "nb";
        public static string NorskNynorsk = "nn";
    }
}
