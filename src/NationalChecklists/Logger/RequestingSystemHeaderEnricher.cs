﻿using Serilog.Core;
using Serilog.Events;

namespace NationalChecklists.Logger
{
    public class RequestingSystemHeaderEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly Dictionary<string, string> _headerKeys;

        public RequestingSystemHeaderEnricher(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;

            _headerKeys = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
            {
                { "foretaknavn", "foretaknavn" },
                { "foretakorgnr", "foretakorgnr" },
                { "system", "system" },
                { "kommunenummer", "kommunenummer" },
            };
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (httpContextAccessor.HttpContext == null)
                return;

            if (httpContextAccessor.HttpContext.Request.Path.StartsWithSegments(new PathString("/api/sjekkliste"), System.StringComparison.OrdinalIgnoreCase)
                || (httpContextAccessor.HttpContext.Request.Path.StartsWithSegments(new PathString("/api/tiltakstyper"), System.StringComparison.OrdinalIgnoreCase))
                || (httpContextAccessor.HttpContext.Request.Path.StartsWithSegments(new PathString("/api/sjekkpunkt"), System.StringComparison.OrdinalIgnoreCase)))
            {
                foreach (var item in _headerKeys)
                {
                    var headerValue = GetHeaderValue(item.Key);

                    var headerValueProperty = propertyFactory.CreateProperty(item.Value, headerValue);

                    logEvent.AddPropertyIfAbsent(headerValueProperty);
                }
            }
        }

        private string GetHeaderValue(string key)
        {
            var headerValue = string.Empty;

            if (httpContextAccessor.HttpContext.Request.Headers.TryGetValue(key, out var values))
            {
                headerValue = values.FirstOrDefault();
            }

            if (string.IsNullOrEmpty(headerValue))
                headerValue = "Ikke oppgitt";

            return headerValue;
        }
    }
}