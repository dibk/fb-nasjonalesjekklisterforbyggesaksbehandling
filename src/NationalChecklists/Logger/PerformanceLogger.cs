﻿using System.Diagnostics;

namespace NationalChecklists.Logger
{
    public class PerformanceTimeLogger : IDisposable
    {
        private readonly string _context;
        private readonly ILogger _logger;
        private Stopwatch _timer;

        public PerformanceTimeLogger(ILogger logger, string context)
        {
            this._timer = new Stopwatch();
            this._timer.Start();            
            this._logger = logger;
            _context = context;
        }

        public void Dispose()
        {
            this._timer.Stop();
            var elapsedMilliseconds = this._timer.ElapsedMilliseconds;

            _logger.LogDebug("{SourceContext} {context} used {ElapsedMilliseconds}", "PerformanceTimeLogger", _context, elapsedMilliseconds);
        }
    }
}
