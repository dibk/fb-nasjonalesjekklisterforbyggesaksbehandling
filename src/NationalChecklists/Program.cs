using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NationalChecklists;
using NationalChecklists.Configuration;
using NationalChecklists.Logger;
using NationalChecklists.Middleware;
using NationalChecklists.Services;
using NationalChecklists.Services.ExternalApiServices;
using Serilog;
using System.Text.Json;
using System.Text.Json.Serialization;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);

ConfigureServices(builder.Services, builder.Configuration);

builder.Host.UseSerilog();

WebApplication app = builder.Build();

//Kun for at 20200820110554_FirstMigration.cs skal funke
DbConnectionstringProvider.ConnectionString = app.Configuration.GetConnectionString("DefaultConnection");

Configure();

app.Run();

void ConfigureServices(IServiceCollection services, IConfiguration configuration)
{
    services.AddAuthentication("Bearer").AddJwtBearer("Bearer", options =>
    {
        options.Authority = configuration["Authentication:Authority"] ?? "http://localhost:5000";
        options.RequireHttpsMetadata = false;
        options.Audience = "DIBK.Checklist.Api.Internal";
    });

    services.AddControllers()
                            .AddJsonOptions(options =>
                            {
                                options.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles;
                                options.JsonSerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
                                options.JsonSerializerOptions.DictionaryKeyPolicy = JsonNamingPolicy.CamelCase;
                                options.JsonSerializerOptions.DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull;
                            });

    services.AddHealthChecks()
        .AddSqlServer(configuration.GetConnectionString("DefaultConnection"))
        .AddDbContextCheck<ApplicationDbContext>();

    services.AddSwaggerGen(options =>
    {
        options.SwaggerDoc("v1", new OpenApiInfo { Title = "DiBK Nasjonale sjekklister", Version = "v1" });

        var assemblies = AppDomain.CurrentDomain.GetAssemblies()
            .Where(x => x.GetName().Name?.StartsWith("NationalChecklists") ?? false);

        foreach (var assembly in assemblies)
        {
            var xml_path = Path.Combine(AppContext.BaseDirectory, $"{assembly.GetName().Name}.xml");
            if (File.Exists(xml_path))
            {
                options.IncludeXmlComments(xml_path);
            }
        }
    });

    services.AddCors(options =>
    {
        options.AddPolicy("AllowAll",
            policyBuilder =>
            {
                policyBuilder.AllowAnyOrigin()
                             .AllowAnyHeader()
                             .AllowAnyMethod();
            });
    });

    services.AddCors(options =>
    {
        options.AddPolicy("Internal",
            policyBuilder =>
            {
                var allowedOrigins = configuration.GetValue<string>("Cors:AllowedOrigins").Split(';', System.StringSplitOptions.RemoveEmptyEntries);

                policyBuilder.WithOrigins(allowedOrigins)
                             .AllowAnyHeader()
                             .AllowAnyMethod()
                             .AllowCredentials();
            });
    });

    services.AddDbContext<ApplicationDbContext>(options =>
        options.UseSqlServer(new Microsoft.Data.SqlClient.SqlConnection(configuration.GetConnectionString("DefaultConnection")),
        sqlServerOptions => sqlServerOptions.CommandTimeout(60))
    );

    //Add application services
    services.AddOptions();
    services.AddHttpContextAccessor();
    services.AddTransient<RequestingSystemHeaderEnricher>();
    services.AddTransient<IActivityServiceForInternalApi, ActivityServiceForInternalApi>();
    services.AddTransient<IActivityServiceForExternalApi, ActivityServiceForExternalApi>();
    services.AddTransient<IChecklistFilterService, ChecklistFilterService>();
    services.AddTransient<IFacetService, FacetService>();
    services.AddTransient<IMetadataService, MetadataService>();
    services.AddTransient<IPrefillService, PrefillService>();

    services.Configure<CheckpointConfig>(configuration.GetSection(CheckpointConfig.SectionName));

    services.AddAllElasticApm();
    services.AddLogging(loggingBuilder =>
    {
        loggingBuilder.AddSerilog();
    });
}

void Configure()
{
    using (var serviceScope = app.Services.GetService<IServiceScopeFactory>().CreateScope())
    {
        var context = serviceScope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
        context.Database.Migrate(); //TODO: Does not handle migrations, use Migrate() instead
    }

    app.UseMiddleware<ExceptionMiddleware>();

    app.UseRouting();
    app.UseCors();

    if (app.Environment.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "DiBK Nasjonale sjekklister");
        });
    }

    app.UseAuthentication();
    app.UseAuthorization();

    Logging.ConfigureLogging(builder.Configuration);
    app.UseSerilogRequestLogging();
    app.UseHealthChecks(new PathString("/health"));
    app.MapControllers();
}