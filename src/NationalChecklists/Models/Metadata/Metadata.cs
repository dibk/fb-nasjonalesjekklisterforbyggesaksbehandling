﻿namespace NationalChecklists.Middleware.Models.Metadata
{
    [Serializable]
    public class Metadata
    {
        public int Id { get; set; }

        // evt Kodeverdi
        // IA, HA, VK, SK... 
        // True, False
        public string MetadataCode { get; set; }

        /// <summary>
        /// Hentes enten fra en kodeliste, en definert liste med lovlige verdier, eller fritekst... 
        /// </summary>
      
        // "Ja", "Nei"
        // Rammesøknad
        // Stor trapp
        // Liten trapp
        public string Value { get; set; }

        public int TypeId { get; set; }

        // Fra config, definert liste over lovlige kategorier eller typer... - "Type regel", "Sjekket i FTB.. "
        // soknadstype
        public virtual MetadataType Type { get; set; }

        public int? OrderNumber { get; set; }

        // Hierarki.. 
        public string ParentId { get; set; }

        public void Update(Metadata updated)
        {
            if (Value != updated.Value)
                Value = updated.Value;

            if (MetadataCode != updated.MetadataCode)
                MetadataCode = updated.MetadataCode;

            if (OrderNumber != updated.OrderNumber)
                OrderNumber = updated.OrderNumber;
        }
    }
}
