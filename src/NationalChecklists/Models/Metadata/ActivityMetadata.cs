﻿using NationalChecklists.ViewModels;
using System.ComponentModel.DataAnnotations.Schema;

namespace NationalChecklists.Middleware.Models.Metadata
{
    [Serializable]
    public class ActivityMetadata
    {
        public int ActivityId { get; set; }
        public int MetadataId { get; set; }
        public int MetadataTypeId { get; set; }
        public virtual Metadata MetadataItem { get; set; }
        //public virtual MetadataType MetadataType { get; set; }

        [NotMapped]
        public bool CommonValue { get; set; }

        internal ActivityMetadata Map(int activityId, MetadataViewModel metadataViewModel)
        {
            return new ActivityMetadata
            {
                ActivityId = activityId,
                MetadataId = metadataViewModel.MetadataId.Value,
                MetadataTypeId = metadataViewModel.MetadataTypeId
            };
        }
    }
}
