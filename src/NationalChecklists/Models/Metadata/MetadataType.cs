﻿namespace NationalChecklists.Middleware.Models.Metadata
{
    /// <summary>
    /// Liste med Metadatatyper definert i databasen/Config
    /// </summary>
    [Serializable]
    public class MetadataType
    {
        // "type regel"
        // "soknadstype"
        // "trapp"
        // "tripptrapp"
        public int Id { get; set; }

        // "Type regel"
        public string Name { get; set; }

        // 1
        public int? OrderNumber { get; set; }

        // Liste
        public string ViewType { get; set; }

        // skal metadata verdiene kunne oppdateres av bruker, eller skal det kun velges fra en gitt liste med verdier.
        // Eks om det er et felt med beskrivelse, så skal dette kunne endres.
        // Eks er det er felt med "Er sjekket" - true/false, så skal dette kanskje ikke kunne endres.
        public bool CanUpdateValues { get; set; }

        // Dersom det er en grense på hvor mange verdier som kan settes pr sjekkpunkt av denne typen.
        // Default er ubegrenset.
        public int MaxCountOfValues { get; set; }

        // Om feltet gjelder for sjekkpunkt på tvers av ProcessCategories (Søknadstyper)
        public bool CommonValue { get; set; }

        // om feltet skal ligge som en fasett.
        public bool Facet { get; set; }

        // Liste med lovlige verdier... Kodeliste?... string/metadataTypeValues... ?
        public List<Metadata> MetadataValues { get; set; }


        public List<MetadataType> Map(List<MetadataTypeViewModel> metadataTypesViewModel)
        {
            var metadataTypes = new List<MetadataType>();
            foreach (var metadataTypeViewModel in metadataTypesViewModel)
            {
                var metadataType = new MetadataType();
                metadataType.Id = metadataTypeViewModel.Id;
                metadataType.MetadataValues = metadataTypeViewModel.MetadataValues;
                if (metadataTypeViewModel.CanUpdateValues != null) metadataType.CanUpdateValues = metadataTypeViewModel.CanUpdateValues.Value;
                if (metadataTypeViewModel.MaxCountOfValues != null) metadataType.MaxCountOfValues = metadataTypeViewModel.MaxCountOfValues.Value;
                if (metadataTypeViewModel.OrderNumber != null) metadataType.OrderNumber = metadataTypeViewModel.OrderNumber.Value;
                metadataType.ViewType = metadataTypeViewModel.ViewType;
                metadataType.Name = metadataTypeViewModel.Name;
                metadataType.CommonValue = metadataTypeViewModel.CommonValue;
                metadataType.Facet = metadataTypeViewModel.Facet;

                metadataTypes.Add(metadataType);
            }

            return metadataTypes;
        }

    }

    public class MetadataTypeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int? OrderNumber { get; set; }

        public string ViewType { get; set; }

        public bool? CanUpdateValues { get; set; }

        public int? MaxCountOfValues { get; set; }
        public bool CommonValue { get; set; }
        public bool Facet { get; set; }
        
        public List<Metadata> MetadataValues { get; set; }
        
    }


    public enum ViewType {
        Textbox,
        Checkbox,
        Dropdownlist,
        Radiobutton
    }
}
