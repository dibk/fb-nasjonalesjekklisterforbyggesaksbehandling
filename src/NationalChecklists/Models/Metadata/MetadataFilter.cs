﻿namespace NationalChecklists.Middleware.Models.Metadata
{
    public class MetadataFilter
    {
        public int? MetadataTypeId { get; set; }
        public bool? CanUpdateValues { get; set; }
        public bool? ShowAsFacetFilter { get; set; }
    }
}
