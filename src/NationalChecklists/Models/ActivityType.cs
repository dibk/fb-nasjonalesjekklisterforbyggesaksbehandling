﻿namespace NationalChecklists.Models
{
    public class ActivityType : Facet
    {
        public ActivityType()
        {
        }

        public ActivityType(string id, string description)
        {
            Id = id;
            Description = description;
        }
    }
}
