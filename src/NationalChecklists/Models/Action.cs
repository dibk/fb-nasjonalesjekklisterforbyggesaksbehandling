﻿namespace NationalChecklists.Models
{
    [Serializable]
    public class Action
    {
        public int Id { get; set; }
        /// <summary>
        /// Om svaret er Ja/nei-true/false
        /// </summary>
        public bool ActionValue { get; set; }

        /// <summary>
        /// Utfall type som passer til verdi  
        /// </summary>
        public string ActionType { get; set; }

        public string ActionTypeCode { get; set; }
        public string TitleNb { get; set; }
        public string TitleNn { get; set; }
        public string DescriptionNb { get; set; }
        public string DescriptionNn { get; set; }
        public string ContentType { get; set; }
        public List<ActionValidationRules> SupportiveValidationRules { get; set; }
        public int? PublishedAction_Id { get; set; }


        public int ActivityId { get; set; }

        public virtual Activity Activity { get; set; }
    }

    public class ActionValidationRules
    {
        public ActionValidationRules()
        {
            ValidationRules = new List<ValidationRule>();
        }

        public ActionValidationRules(List<string> validationRules, int id)
        {
            if (id != 0)
            {
                Id = id;
            }

            foreach (var validationRule in validationRules)
            {
                ValidationRules.Add(new ValidationRule(validationRule));
            }
        }

        public int Id { get; set; }
        public List<ValidationRule> ValidationRules { get; set; } = new List<ValidationRule>();
        public virtual Action Action { get; set; }
    }

    public class ValidationRule
    {
        public ValidationRule(string validationRuleId)
        {
            ValidationRuleId = validationRuleId;
        }

        public int Id { get; set; }
        public string ValidationRuleId { get; set; }
    }
}
