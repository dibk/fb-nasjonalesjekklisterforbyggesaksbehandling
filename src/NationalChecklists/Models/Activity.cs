﻿using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.ViewModels;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace NationalChecklists.Models
{
    [Serializable]
    public class Activity
    {
        /// <summary>
        /// Intern id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Ekstern Id som gjelder felles på flere søknadsstyper (ID i Excel regneark)
        /// </summary>
        public string ReferenceId { get; set; }

        /// <summary>
        /// Referanse til ID (ReferenceId) for overordnet aktivitet
        /// </summary>
        /// </summary>
        public string ParentReferenceId { get; set; }

        /// <summary>
        /// Gjelder evt kommune. Ikke angitt betyr gjelder alle
        /// </summary>
        public string Municipality { get; set; }

        /// <summary>
        /// Sjekkpunkt
        /// </summary>
        public string Name { get; set; }
        public string NameNynorsk { get; set; }

        ///<summary>
        ///Kategorisering av sjekk. Samme som sjekpunktene i eByggesak
        /// </summary>
        public string ActivityType { get; set; }

        /// <summary>
        /// Veiledning/hjelpetekst
        /// </summary>
        public string Description { get; set; }
        public string DescriptionNynorsk { get; set; }

        /// <summary>
        /// Dokumentasjon - markdown
        /// </summary>
        public string Documentation { get; set; }

        /// <summary>
        /// Dokumentasjon - markdown nynorsk
        /// </summary>
        public string DocumentationNynorsk { get; set; }

        /// <summary>
        /// Image
        /// </summary>
        public byte[] Image { get; set; }

        /// <summary>
        /// Image mimeType
        /// </summary>
        public string ImageMimeType { get; set; }

        /// <summary>
        /// Image mimeType
        /// </summary>
        public string ImageDescription { get; set; }

        /// <summary>
        /// Tema
        /// </summary>
        public string Category { get; set; }


        private List<LawReference> _lawReferences;
        /// <summary>
        /// Lovhjemmel
        /// </summary>

        public List<LawReference> LawReferences
        {
            get => _lawReferences;
            set => _lawReferences = value;
        }

        /// <summary>
        /// Prosesskategori/Søknadstype
        /// </summary>
        public string Processcategory { get; set; }

        /// <summary>
        /// Milepel sjekkpunkt hører til
        /// </summary>
        public string Milestone { get; set; }

        ///<summary>
        /// Sjekkpunkt status: kladd, publisert eller
        /// arkivert
        /// </summary>
        public StatusForActivity Status { get; set; }

        /// <summary>
        /// Om maskinlesbar regel er definert i Rule feltet
        /// </summary>
        public bool HasRule { get; set; }

        ///<summary>
        /// Tiltakstyper for sjekkpunktet
        /// </summary>
        public List<EnterpriseTerm> EnterpriseTerms { get; set; }

        /// <summary>
        /// Regel i bpel eller dmn, scematron, ocl? true/false ? 
        /// Eks. Sjekke vedlegg, hvis x=true så må også y=notEmpty()
        /// Eks. Hvis tiltakstype=påbygg/tilbygg så er bygningsnr påkrevd
        /// Eks. Hvis avfallsplan kreves - sjekk om vedleggstype avfallsplan er med
        /// </summary>
        public string Rule { get; set; }

        /// <summary>
        /// Aksjon/Utfall hvis rule=false, feks registrer mangel, lag dokument, ...
        /// </summary>

        public List<Action> Actions { get; set; }

        //Parent Activity Id
        public int? Activity_Id { get; set; }
        public virtual Activity ParentActivity { get; set; }

        /// <summary>
        /// Id til et sjekkpunkt med status "publisert" 
        /// </summary>
        public int? PublishedActivity_Id { get; set; }

        [NotMapped]
        public Activity DraftActivity { get; set; }

        public List<Activity> SubActivities { get; set; }

        public DateTime? ValidFrom { get; set; }

        public DateTime? ValidTo { get; set; }

        public DateTime Updated { get; set; }

        /// <summary>
        /// Rekkefølge på sjekk - samme som radnr i excelarket
        /// </summary>
        public int OrderNumber { get; set; }

        public List<ActivityMetadata> Metadata { get; set; }

        public Activity()
        {
            Updated = DateTime.Now;
            Status = StatusForActivity.Draft;
            SubActivities = null;
            Id = 0;
        }

        public Activity(string parentReferenceId)
        {
            Updated = DateTime.Now;
            Status = StatusForActivity.Draft;
            SubActivities = null;
            Id = 0;
            ParentReferenceId = parentReferenceId;
        }

        public Activity(CreateActivityViewModel input)
        {
            ParentReferenceId = input.ParentReferenceId;
            Name = input.Name;
            NameNynorsk = input.NameNynorsk;
            Processcategory = "";
            Category = input.Category?.Description;
            ActivityType = input.ActivityType?.Description;
            Milestone = input.Milestone?.Id;
            Description = input.Description;
            DescriptionNynorsk = input.DescriptionNynorsk;
            ValidFrom = input.ValidFrom;
            ValidTo = input.ValidTo;
            Updated = DateTime.Now;
            Rule = input.Rule;
            LawReferences = new LawReferenceViewModel().MapToEnumerable(input.LawReferences).ToList();
        }

        public TActivity DeepCopy<TActivity>(TActivity activity)
        {
            var settings = new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore };
            var aString = JsonConvert.SerializeObject(activity, settings);
            return JsonConvert.DeserializeObject<TActivity>(aString);
        }

        /// <summary>
        /// Updates the list of enterprise terms with the incoming list. 
        /// Preserves existing enterprise terms that are tracked by entity framework which still should be in the list. 
        /// </summary>
        /// <param name="updatedEnterpriseTerms"></param>
        public void UpdateEnterpriseTerms(List<EnterpriseTerm> updatedEnterpriseTerms)
        {
            var updatedEnterpriseTermsIds = updatedEnterpriseTerms.Select(et => et.Id).ToList();

            List<EnterpriseTerm> updatedListofEnterpriseTerms = new List<EnterpriseTerm>();

            foreach (var enterpriseTerm in EnterpriseTerms)
            {
                if (updatedEnterpriseTermsIds.Contains(enterpriseTerm.Id))
                {
                    updatedListofEnterpriseTerms.Add(enterpriseTerm);
                    updatedEnterpriseTerms.RemoveAll(et => et.Id == enterpriseTerm.Id);
                }
            }

            updatedListofEnterpriseTerms.AddRange(updatedEnterpriseTerms);
            EnterpriseTerms = updatedListofEnterpriseTerms;
        }

        /// <summary>
        /// Updates the list of law references with the incoming list. 
        /// Preserves existing law references that are tracked by entity framework which still should be in the list. 
        /// </summary>
        /// <param name="updatedLawReferences"></param>
        public void UpdateLawReferences(List<LawReference> updatedLawReferences)
        {
            var updatedLawReferenceIds = updatedLawReferences.Select(lr => lr.Id).ToList();

            List<LawReference> updatedListOfLawReferences = new List<LawReference>();

            foreach (var lawReference in LawReferences)
            {
                if (updatedLawReferenceIds.Contains(lawReference.Id))
                {
                    updatedListOfLawReferences.Add(updatedLawReferences.Find(lr => lr.Id == lawReference.Id));
                    updatedLawReferences.RemoveAll(lr => lr.Id == lawReference.Id);
                }
            }

            updatedListOfLawReferences.AddRange(updatedLawReferences);
            LawReferences = updatedListOfLawReferences;
        }

        /// <summary>
        /// Updates the list of actions with the incoming list. 
        /// Preserves existing actions that are tracked by entity framework which still should be in the list. 
        /// </summary>
        /// <param name="updatedActions"></param>
        public void UpdateActions(List<Action> updatedActions)
        {
            var updatedActionsIds = updatedActions.Select(et => et.Id).ToList();

            List<Action> updatedListOfActions = new List<Action>();

            foreach (var action in Actions)
            {
                if (updatedActionsIds.Contains(action.Id))
                {
                    updatedListOfActions.Add(updatedActions.Find(a => a.Id == action.Id));
                    updatedActions.RemoveAll(a => a.Id == action.Id);
                }
            }

            updatedListOfActions.AddRange(updatedActions);
            Actions = updatedListOfActions;
        }

        public void UpdateMetadata(List<ActivityMetadata> updatedMetadata, int updatedActivityId)
        {
            var updatedListOfMetadata = new List<ActivityMetadata>();

            if (updatedActivityId == Id)
            {
                foreach (var item in updatedMetadata)
                {
                    item.ActivityId = Id;
                    updatedListOfMetadata.Add(item);
                }
                Metadata = updatedListOfMetadata;
            }
            else
            {
                // *TODO test når aktuelt...  Skal de legges til eller oppdateres?
                //foreach (var item in updatedMetadata)
                //{
                //    if (item.CommonValue)
                //    {
                //        item.ActivityId = Id;
                //        Metadata.Add(item);
                //    }
                //}
            }
        }

        public void UpdatePublishedAction(List<Action> updatedActions)
        {
            // Fjern elementer fra publiserte actions som ikke finnes i oppdaterte actions
            Actions.RemoveAll(a => updatedActions.All(b => b.PublishedAction_Id != a.Id));

            // Oppdater eller legg til elementer fra oppdaterte actions til publiserte actions
            foreach (var updatedAction in updatedActions)
            {
                var existingAction = Actions.FirstOrDefault(a => a.Id == updatedAction.PublishedAction_Id);

                if (existingAction != null)
                {
                    // Oppdater eksisterende element i publisert action med verdier fra opppdaterte actions
                    existingAction.ActionTypeCode = updatedAction.ActionTypeCode;
                    existingAction.ActionValue = updatedAction.ActionValue;
                    existingAction.ActionType = updatedAction.ActionType;
                    existingAction.ContentType = updatedAction.ContentType;
                    existingAction.DescriptionNb = updatedAction.DescriptionNb;
                    existingAction.DescriptionNn = updatedAction.DescriptionNn;
                    existingAction.TitleNb = updatedAction.TitleNb;
                    existingAction.TitleNn = updatedAction.TitleNn;
                }
                else
                {
                    // Legg til element fra oppdaterte actions til publiserte actions
                    Actions.Add(updatedAction);
                }
            }
        }
    }



public enum StatusForActivity { Draft, Published, Archived }

}
