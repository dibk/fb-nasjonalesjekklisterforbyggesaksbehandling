using NationalChecklists.Models;
using NationalChecklists.ViewModels;

namespace NationalChecklists.Middleware.Models.Admin
{
    public class AdminActivityActionValidationRulesViewModel
    {
        public string ProcessCategory { get; set; }
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public string ActivityReferenceId { get; set; }
        public List<AdminActionValidationRulesViewModel> Actions { get; set; }

        public static List<AdminActivityActionValidationRulesViewModel> Map(List<Activity> activities)
        {
            var activityAndActionList = new List<AdminActivityActionValidationRulesViewModel>();
            foreach (var activity in activities)
            {
                activityAndActionList.Add(new AdminActivityActionValidationRulesViewModel()
                {
                    ProcessCategory = activity.Processcategory,
                    ActivityId = activity.Id,
                    ActivityName = activity.Name,
                    ActivityReferenceId = activity.ReferenceId,
                    Actions = activity.Actions.Select(a => new AdminActionValidationRulesViewModel()
                    {
                        ActionId = a.Id,
                        ActionValue = a.ActionValue,
                        ActionType = a.ActionType,
                        SupportingDataValidationRuleId = a.SupportiveValidationRules.Select(v =>
                                        new ValidationRulesViewModel()
                                        {
                                            Id = v.Id,
                                            ValidationRuleIds = v.ValidationRules.Select(r => r.ValidationRuleId).ToList()
                                        }).ToList()

                    }).ToList()
                });
            }

            return activityAndActionList;
        }

        public class AdminActionValidationRulesViewModel
        {
            public int ActionId { get; set; }
            public bool ActionValue { get; set; }
            public string ActionType { get; set; }

            public List<ValidationRulesViewModel> SupportingDataValidationRuleId { get; set; } = new();
        }
    }
}
