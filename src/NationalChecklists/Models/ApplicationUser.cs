﻿using Microsoft.AspNetCore.Identity;

namespace NationalChecklists.Models
{
    public class ApplicationUser : IdentityUser
    {
        /// <summary>
        /// Full name of the user
        /// </summary>
        public string FullName { get; set; }

    }
}
