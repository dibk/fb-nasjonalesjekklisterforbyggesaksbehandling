﻿namespace NationalChecklists.Middleware.Models.ExternalApiModels
{
    public class DynamiskeMetadata
    {
        public int TypeId { get; set; }
        public string Type { get; set; }
        public int MetadataId { get; set; }
        public string Value { get; set; }
        public string? MetadataCode { get; set; }

        public DynamiskeMetadata(int typeId, string type, string value, int metadataId, string? metadataCode)
        {
            TypeId = typeId;
            Type = type;
            MetadataId = metadataId;
            Value = value;
            MetadataCode = metadataCode;
        }
    }
}
