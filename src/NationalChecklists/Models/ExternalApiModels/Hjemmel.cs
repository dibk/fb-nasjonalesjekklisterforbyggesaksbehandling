﻿namespace NationalChecklists.Models.ExternalApiModels
{
    public class Hjemmel
    {
        /// <summary>
        /// Lovhjemmel
        /// </summary>
        public string Lovhjemmel { get; set; }

        /// <summary>
        /// Lovhjemmel webadresse
        /// </summary>
        public string LovhjemmelUrl { get; set; }
    }
}
