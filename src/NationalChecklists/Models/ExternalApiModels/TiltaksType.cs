﻿namespace NationalChecklists.Models.ExternalApiModels
{
    public class TiltaksType
    {
        /// <summary>
        /// Kodeverdi for en gitt tiltakstype
        /// </summary>
        public string Kode { get; set; }
    }
}
