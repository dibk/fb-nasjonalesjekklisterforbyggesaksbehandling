﻿using NationalChecklists.Middleware.Models.ExternalApiModels;

namespace NationalChecklists.Models.ExternalApiModels
{
    public class Sjekk
    {

        /// <summary>
        /// Intern id
        /// </summary>
        public int SjekkId { get; set; }

        /// <summary>
        /// Referanse Id til felles Id for sjekkpunkt som gjelder flere søknadstyper
        /// </summary>
        public string Id { get; set; }



        /// <summary>
        /// Gjelder evt kommune. Ikke angitt betyr gjelder alle
        /// </summary>
        public string Kommunenummer { get; set; }

        public string Eier { get; }

        /// <summary>
        /// Kategorisering av sjekk. Samme som sjekkpunkttype i eByggesak
        /// </summary>
        public string Sjekkpunkttype { get; set; }

        /// <summary>
        /// Sjekkpunkt
        /// </summary>
        public string Navn { get; set; }


        /// <summary>
        /// Sjekkpunkt navn nynorsk
        /// </summary>
        public string NavnNynorsk { get; set; }

        /// <summary>
        /// Veiledning/hjelpetekst
        /// </summary>
        public string Beskrivelse { get; set; }

        /// <summary>
        /// Dokumentasjon - Markdown
        /// </summary>
        public string Dokumentasjon { get; set; }

        /// <summary>
        /// Dokumentasjon - Markdown
        /// </summary>
        public string DokumentasjonNynorsk { get; set; }

        /// <summary>
        /// Veiledning/hjelpetekst nynorsk
        /// </summary>
        public string BeskrivelseNynorsk { get; set; }

        /// <summary>
        /// Tema
        /// </summary>
        public string Tema { get; set; }

        /// <summary>
        /// Lovhjemmel
        /// </summary>
        public List<Hjemmel> Lovhjemmel { get; set; }


        /// <summary>
        /// Prosesskategori/Søknadstype
        /// </summary>
        public string Prosesskategori { get; set; }

        /// <summary>
        /// Milepel sjekkpunkt hører til
        /// </summary>
        public string Milepel { get; set; }

        /// <summary>
        /// Om maskinlesbar regel er definert i Regel feltet
        /// </summary>
        public bool HarMaskinlesbarRegel { get; set; }

        /// <summary>
        /// Regel i bpel eller dmn, scematron, ocl? true/false ? 
        /// Eks. Sjekke vedlegg, hvis x=true så må også y=notEmpty()
        /// Eks. Hvis tiltakstype=påbygg/tilbygg så er bygningsnr påkrevd
        /// Eks. Hvis avfallsplan kreves - sjekk om vedleggstype avfallsplan er med
        /// </summary>
        public string Regel { get; set; }

        ///<summary>
        /// Tiltakstyper for sjekkpunktet
        /// </summary>
        public List<TiltaksType> Tiltakstyper { get; set; }

        /// <summary>
        /// Hvis regel/spørsmål/behandling=true/false så angis det her mulige utfall på behandlingen
        /// </summary>
        public List<Utfall> Utfall { get; set; }
        public List<Sjekk> Undersjekkpunkter { get; set; }

        public DateTime? GyldigFra { get; set; }

        public DateTime? GyldigTil { get; set; }

        public DateTime? Oppdatert { get; set; }

        /// <summary>
        /// Rekkefølge på sjekk - samme som radnr i excelarket
        /// </summary>
        public int Rekkefolge { get; set; }

        public List<DynamiskeMetadata> Metadata { get; set; }

        public Sjekk(Activity activity)
        {
            SjekkId = activity.Id;
            Id = activity.ReferenceId;
            Kommunenummer = activity.Municipality;
            Eier = GetOwnerName();
            Sjekkpunkttype = activity.ActivityType;
            Navn = activity.Name;
            NavnNynorsk = activity.NameNynorsk;
            Beskrivelse = activity.Description;
            Dokumentasjon = activity.Documentation;
            DokumentasjonNynorsk = activity.DocumentationNynorsk;
            BeskrivelseNynorsk = activity.DescriptionNynorsk;
            Tema = activity.Category;

            Prosesskategori = activity.Processcategory;
            Milepel = activity.Milestone;
            HarMaskinlesbarRegel = activity.HasRule;
            Regel = activity.Rule;
            if (activity.EnterpriseTerms != null)
                Tiltakstyper = MapToTiltakstyper(activity.EnterpriseTerms);
            else Tiltakstyper = null;
            Rekkefolge = activity.OrderNumber;

            Lovhjemmel = new List<Hjemmel>();

            if (activity.LawReferences != null)
            {                
                foreach (var item in activity.LawReferences)
                {
                    Lovhjemmel.Add(new Hjemmel() { Lovhjemmel = item.LawReferenceDescription, LovhjemmelUrl = item.LawReferenceUrl });
                }
            }

            GyldigFra = activity.ValidFrom;
            GyldigTil = activity.ValidTo;

            Undersjekkpunkter = new List<Sjekk>();
            if (activity.SubActivities != null)
            {
                Undersjekkpunkter = new List<Sjekk>();
                foreach (var item in activity.SubActivities)
                {
                    Undersjekkpunkter.Add(new Sjekk(item));
                }
            }

            if (activity.Actions != null)
            {
                Utfall = new List<Utfall>();
                foreach (var item in activity.Actions)
                {
                    var utfall = new Utfall(item.ActionValue, item.ActionType, item.ActionTypeCode, item.TitleNb,
                        item.DescriptionNb, item.TitleNn, item.DescriptionNn, item.ContentType);

                    Utfall.Add(utfall);
                }
            }

            if (activity.Metadata != null)
            {
                Metadata = new List<DynamiskeMetadata>();
                foreach (var metadata in activity.Metadata)
                {
                    if (metadata.MetadataItem != null)
                        Metadata.Add(new DynamiskeMetadata(metadata.MetadataItem.TypeId, metadata.MetadataItem?.Type?.Name, metadata.MetadataItem?.Value, metadata.MetadataItem.Id, metadata.MetadataItem.MetadataCode));
                }
            }

            Oppdatert = activity.Updated;
        }

        private string GetOwnerName()
        {
            if (Kommunenummer == null) return string.Empty;
            return Kommunenummer switch
            {
                "974760223" => "dibk",
                "971526920" => "kostra",
                _ => string.Empty
            };
        }

        private List<TiltaksType> MapToTiltakstyper(List<EnterpriseTerm> activityEnterpriseTerms)
        {
            var tiltaksTyper = new List<TiltaksType>();

            foreach (var activityEnterpriseTerm in activityEnterpriseTerms)
                tiltaksTyper.Add(new TiltaksType { Kode = activityEnterpriseTerm.Code });

            return tiltaksTyper.OrderBy(p => p.Kode).ToList();
        }
    }
}
