﻿using NationalChecklists.Middleware.Models.Metadata;

namespace NationalChecklists.Middleware.Models.ExternalApiModels
{
    public class MetadataTypeViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public List<MetadataViewModel> MetadataValues { get; set; }

        public List<MetadataTypeViewModel> Map(List<MetadataType> metadatas)
        {
            var metadataTypeViewModel = new List<MetadataTypeViewModel>();
            
            foreach (var metadata in metadatas)
            {
                var model = new MetadataTypeViewModel();
                model.Id = metadata.Id;
                model.Name = metadata.Name;
                if (!metadata.CanUpdateValues)
                {
                    model.MetadataValues = new List<MetadataViewModel>();
                    foreach (var values in metadata.MetadataValues)
                    {
                        var modelMetadata = new MetadataViewModel();
                        modelMetadata.Id = values.Id;
                        modelMetadata.MetadataCode = values.MetadataCode;
                        modelMetadata.Value = values.Value;
                        modelMetadata.MetadataCode = values.MetadataCode;
                        model.MetadataValues.Add(modelMetadata);
                    }
                }
                metadataTypeViewModel.Add(model);
            }

            return metadataTypeViewModel;
        }
    }
}
