﻿namespace NationalChecklists.Middleware.Models.ExternalApiModels
{
    public class MetadataViewModel
    {
        public int Id { get; set; }
        public string MetadataCode { get; set; }
        public string Value { get; set; }
    }
}
