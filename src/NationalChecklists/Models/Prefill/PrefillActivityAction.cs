﻿namespace NationalChecklists.Middleware.Models.Prefill
{
    public class PrefillActivityAction
    {
        public PrefillActivityAction()
        {
            SupportingDataValidationRuleId = new List<string>();
        }

        public string ChecklistReference { get; set; }
        public string ChecklistQuestion { get; set; } // Trenger vi denne? tror ikke det.  Hør med sak-og arkivsystemene

        //public string ProcessCategory { get; set; } // Søknadstype.. Trappetype..
        public bool YesNo { get; set; } // True/False

        public List<string> SupportingDataValidationRuleId { get; set; }
        public string SupportingDataXpathField { get; set; }
        public List<string> SupportingDataValues { get; set; }
    }

    public class PrefillActivityActionDemo
    {
        public string ChecklistReference { get; set; }
        public string ChecklistQuestion { get; set; } // Trenger vi denne? tror ikke det.  Hør med sak-og arkivsystemene

        //public string ProcessCategory { get; set; } // Søknadstype.. Trappetype..
        public bool YesNo { get; set; } // True/False

        public string ActionTypeCode { get; set; }
        public string ParentActivityAction { get; set; }
        public List<string> SupportingDataValidationRuleId { get; set; }
        public string SupportingDataXpathField { get; set; }
        public List<string> SupportingDataValues { get; set; }
    }
}