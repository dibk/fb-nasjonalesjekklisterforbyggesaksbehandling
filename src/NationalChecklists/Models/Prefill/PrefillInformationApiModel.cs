﻿using NationalChecklists.ViewModels;

namespace NationalChecklists.Middleware.Models.Prefill
{
    public class PrefillInformationApiModel
    {
        public List<ActivityAndValidationIdsViewModel> PrefillValidations { get; set; }
        public List<ActivityAndPrefillXpathViewModel> PrefillXpath { get; set; }
        public Dictionary<string, LinkModel> Links { get; set; } // Hypermedia-relasjoner


        public PrefillInformationApiModel(
            List<ActivityAndValidationIdsViewModel> prefillValidations,
            List<ActivityAndPrefillXpathViewModel> prefillXpath,
            string processCategory = null)
        {
            PrefillValidations = prefillValidations;
            PrefillXpath = prefillXpath;

            Links = new Dictionary<string, LinkModel>()
            {
                { "self", new LinkModel(){ Href = $"/api/sjekkliste/prefillinformation/{processCategory}", Rel = "Prefill informasjon", Method = "GET"} },
                { "Valideringsregler søknadstype", new LinkModel(){ Href = $"/api/sjekkliste/valideringsregler/{processCategory}", Rel = "Sjekkpunkt i valgt søknadstype med kobling til valideringsregler", Method = "GET"} }, 
            };
        }
    }

    public class LinkModel
    {
        public string Href { get; set; } // URL til det relaterte endepunktet
        public string Rel { get; set; } // Relasjonsnavn for å identifisere koblingen
        public string Method { get; set; } // HTTP-metoden som støttes (GET, POST, PUT, DELETE, osv.)
    }
}
