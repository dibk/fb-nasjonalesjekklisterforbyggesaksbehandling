﻿namespace NationalChecklists.Middleware.Models.Prefill
{
    public class PrefillChecklistInput
    {
        public string ProcessCategory { get; set; }
        public List<string> EnterpriseTerms { get; set; } = new();
        public string DataFormatId { get; set; }
        public string DataFormatVersion { get; set; }
        public List<string> Errors { get; set; } = new();
        public List<string> Warnings { get; set; } = new();
        public List<string> ExecutedValidations { get; set; } = new();
    }

    public class PrefillChecklistInputXpath
    {
        public string ProcessCategory { get; set; }
        public List<string> EnterpriseTerms { get; set; } = new();
        public string Form { get; set; }
    }
}