﻿using NationalChecklists.Models;

namespace NationalChecklists.Middleware.Models
{


    public class StaticMetadata
    {
        public const int ProcessCategory = 1;
        public const int Milestone = 2;
        public const int ActivityType = 3;
        public const int Category = 4;
        public const int EnterpriceTerms = 5;
        public const int Owner = 6;

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public List<Facet> Facets { get; set; }
        public bool ShowAsFacetFilter { get; set; }
        public bool ShowOnDetailPage { get; set; }
    }
}
