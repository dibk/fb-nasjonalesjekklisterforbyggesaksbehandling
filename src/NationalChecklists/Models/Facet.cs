﻿using NationalChecklists.Middleware.Models;

namespace NationalChecklists.Models
{
    public class Facet
    {
        public string Id { get; set; }
        public string Description { get; set; }
        public string Text { get; set; }
        public int OrderNumber { get; set; }
        public int StaticMetadataId { get; set; }
        public virtual StaticMetadata StaticMetadata { get; set; }
    }
}
