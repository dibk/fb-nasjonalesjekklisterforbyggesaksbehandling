﻿namespace NationalChecklists.Models.Facets
{
    public class Owner : Facet
    {
        public Owner()
        {
        }

        public Owner(string id, string description)
        {
            Id = id;
            Description = description;
        }
    }
}
