﻿namespace NationalChecklists.Models.Facets
{
    public class Category : Facet
    {
        public Category()
        {
        }

        public Category(string id, string description)
        {
            Id = id;
            Description = description;
        }

    }
}
