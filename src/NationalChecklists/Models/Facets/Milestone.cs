﻿namespace NationalChecklists.Models.Facets
{
    public class Milestone : Facet
    {
        public Milestone()
        {
        }

        public Milestone(string id, string description)
        {
            Id = id;
            Description = description;
        }
    }
}
