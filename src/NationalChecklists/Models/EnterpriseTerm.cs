﻿namespace NationalChecklists.Models
{
    /// <summary>
    /// Tiltakstype
    /// </summary>
    [Serializable]
    public class EnterpriseTerm
    {
        /// <summary>
        /// Tiltaktypens ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Tiltakstypens kodelistekode
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// ID for tiltakstypens sjekkpunkt
        /// </summary>
        public int ActivityId { get; set; }

        /// <summary>
        /// Tiltakstypens sjekkpunkt
        /// </summary>
        public virtual Activity Activity { get; set; }
    }
}
