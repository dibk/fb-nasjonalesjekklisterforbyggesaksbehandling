﻿namespace NationalChecklists.Models
{
    [Serializable]
    public class LawReference
    {
        public int Id { get; set; }

        /// <summary>
        /// Lovhjemmel
        /// </summary>
        public string LawReferenceDescription { get; set; }

        /// <summary>
        /// Lovhjemmel webadresse
        /// </summary>
        public string LawReferenceUrl { get; set; }

        public int ActivityId { get; set; }
        public virtual Activity Activity { get; set; }
    }
}
