﻿namespace NationalChecklists.Configuration
{
    public class CheckpointConfig
    {
        public static string SectionName = "CheckpointConfig";

        public bool CheckpointReferenceIdBasedOnCategory { get; set; }
        public bool GroupByCategory { get; set; }

        /// <summary>
        /// Name of metadataType used to xpath prefill
        /// </summary>
        public string PrefillXpathMetadataTypeName { get; set; }

        /// <summary>
        /// Name of metadataType used to prefill activities based on selected code value
        /// </summary>
        public string PrefillSelectedCodeValueMetadataTypeName { get; set; }

        /// <summary>
        /// Name of metadataType used to prefill checkpoint based on whether the element has value
        /// </summary>
        //public string PrefillHasValueMetadataTypeName { get; set; }
        //public List<string> DefaultOwnerFilter { get; set; } = new List<string>();
    }
}
