﻿using NationalChecklists.Models;

namespace NationalChecklists.ViewModels
{
    public class EnterpriseTermsViewModel : Mapper<EnterpriseTerm, EnterpriseTermsViewModel>
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public int ActivityId { get; set; }

        public override IEnumerable<EnterpriseTermsViewModel> MapToEnumerable(IEnumerable<EnterpriseTerm> inputs)
        {
            List<EnterpriseTermsViewModel> models = new List<EnterpriseTermsViewModel>();
            if (inputs != null)
                foreach (var input in inputs)
                {
                    models.Add(Map(input));
                }

            return models;
        }

        public override IEnumerable<EnterpriseTerm> MapToEnumerable(IEnumerable<EnterpriseTermsViewModel> inputs)
        {
            List<EnterpriseTerm> enterpriseTerms = new List<EnterpriseTerm>();
            foreach (var input in inputs)
            {
                enterpriseTerms.Add(Map(input));
            }

            return enterpriseTerms;
        }

        public override EnterpriseTermsViewModel Map(EnterpriseTerm input)
        {
            return new EnterpriseTermsViewModel
            {
                Id = input.Id,
                Code = input.Code,
                ActivityId = input.ActivityId
            };
        }

        public override EnterpriseTerm Map(EnterpriseTermsViewModel input)
        {
            return new EnterpriseTerm
            {
                Code = input.Code,
                Id = input.Id,
                ActivityId = input.ActivityId
            };
        }
    }
}
