﻿using NationalChecklists.Models;

namespace NationalChecklists.ViewModels
{
    public class LawReferenceViewModel : Mapper<LawReference, LawReferenceViewModel>
    {
        public int Id { get; set; }
        public string LawReferenceDescription { get; set; }
        public string LawReferenceUrl { get; set; }
        public int ActivityId { get; set; }

        public override IEnumerable<LawReferenceViewModel> MapToEnumerable(IEnumerable<LawReference> inputs)
        {
            List<LawReferenceViewModel> models = new List<LawReferenceViewModel>();
            if (inputs != null)
                foreach (var input in inputs)
                {
                    models.Add(Map(input));
                }

            return models;
        }

        public override IEnumerable<LawReference> MapToEnumerable(IEnumerable<LawReferenceViewModel> inputs)
        {
            List<LawReference> lawReferences = new List<LawReference>();
            foreach (var input in inputs)
            {
                lawReferences.Add(Map(input));
            }

            return lawReferences;
        }

        public override LawReferenceViewModel Map(LawReference input)
        {
            return new LawReferenceViewModel
            {
                Id = input.Id,
                LawReferenceDescription = input.LawReferenceDescription,
                LawReferenceUrl = input.LawReferenceUrl,
                ActivityId = input.ActivityId
            };
        }

        public override LawReference Map(LawReferenceViewModel input)
        {
            return new LawReference
            {
                Id = input.Id,
                LawReferenceDescription = input.LawReferenceDescription,
                LawReferenceUrl = input.LawReferenceUrl,
                ActivityId = input.ActivityId
            };
        }
    }
}
