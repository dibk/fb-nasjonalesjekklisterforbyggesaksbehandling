﻿namespace NationalChecklists.ViewModels
{
    /// <summary>
    ///     Mapping utility for converting between domain object and view model
    /// </summary>
    /// <typeparam name="TInput">the input object, e.g. Activity</typeparam>
    /// <typeparam name="TOutput">the output object, e.g. ActivitiesListItemViewModel</typeparam>
    public abstract class Mapper<TInput, TOutput>
    {
        public abstract IEnumerable<TOutput> MapToEnumerable(IEnumerable<TInput> inputs);
        public abstract IEnumerable<TInput> MapToEnumerable(IEnumerable<TOutput> inputs);

        public abstract TOutput Map(TInput input);
        public abstract TInput Map(TOutput input);

    }
}
