using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;

namespace NationalChecklists.ViewModels
{
    public class ActivityListItemViewModel : Mapper<Activity, ActivityListItemViewModel>
    {
        public int Id { get; set; }
        public string ReferenceId { get; set; }
        public string ParentReferenceId { get; set; }
        public string Name { get; set; }
        public string NameNynorsk { get; set; }
        public string Description { get; set; }
        public string DescriptionNynorsk { get; set; }
        public string Documentation { get; set; }
        public string DocumentationNynorsk { get; set; }
        public string Image { get; set; }
        public string ImageDescription { get; set; }
        public string ProcessCategory { get; set; }
        public string Category { get; set; }
        public string ActivityType { get; set; }
        public string Milestone { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public string ParentReferanceid { get; set; }
        public string Status { get; set; }
        public ActivityListItemViewModel DraftActivity { get; set; }
        public IEnumerable<ActivityListItemViewModel> SubActivities { get; set; }
        public IEnumerable<ActionViewModel> Actions { get; set; }
        public int OrderNumber { get; set; }

        public IEnumerable<MetadataViewModel> Metadata { get; set; }

        public override IEnumerable<ActivityListItemViewModel> MapToEnumerable(IEnumerable<Activity> inputs)
        {
            var viewModels = new List<ActivityListItemViewModel>();
            if (inputs != null)
            {
                foreach (var input in inputs)
                {
                    ActivityListItemViewModel listItemViewModel = Map(input);
                    viewModels.Add(listItemViewModel);
                }
            }

            return viewModels;
        }

        public override IEnumerable<Activity> MapToEnumerable(IEnumerable<ActivityListItemViewModel> inputs)
        {
            throw new NotImplementedException();
        }


        public override ActivityListItemViewModel Map(Activity input)
        {
            var viewModel = new ActivityListItemViewModel();
            viewModel.Id = input.Id;
            viewModel.ReferenceId = input.ReferenceId;
            viewModel.ParentReferenceId = input.ParentReferenceId;
            viewModel.Name = input.Name;
            viewModel.NameNynorsk = input.NameNynorsk;
            viewModel.Description = input.Description;
            viewModel.DescriptionNynorsk = input.DescriptionNynorsk;
            if (input.Image != null)
            {
                string startBase64 = !string.IsNullOrWhiteSpace(input.ImageMimeType)
                    ? input.ImageMimeType + ", "
                    : string.Empty;

                viewModel.Image = startBase64 + Convert.ToBase64String(input.Image);
                viewModel.ImageDescription = input.ImageDescription;
            }

            viewModel.ProcessCategory = input.Processcategory;
            viewModel.Category = input.Category;
            viewModel.ActivityType = input.ActivityType;
            viewModel.Milestone = input.Milestone;
            viewModel.ValidFrom = input.ValidFrom;
            viewModel.ValidTo = input.ValidTo;
            viewModel.ParentReferanceid = input.ParentReferenceId;
            viewModel.Status = input.Status.ToString();
            viewModel.DraftActivity = input.DraftActivity != null ? MapDraftActivity(input.DraftActivity) : null;
            viewModel.SubActivities = MapSubActivities(input.SubActivities).OrderBy(s => s.OrderNumber);
            viewModel.Actions = new ActionViewModel().MapToEnumerable(input.Actions);
            viewModel.OrderNumber = input.OrderNumber;
            viewModel.Documentation = input.Documentation;
            viewModel.DocumentationNynorsk = input.DocumentationNynorsk;
            viewModel.Metadata = MetadataViewModel.Map(input.Metadata);
            return viewModel;
        }

        private ActivityListItemViewModel MapDraftActivity(Activity inputDraftActivity)
        {
            return new ActivityListItemViewModel().Map(inputDraftActivity);
        }

        private string GetStatuses(DateTime? validFrom, DateTime? validTo)
        {
            if (validFrom.HasValue && !validTo.HasValue)
                return "Published";
            return "Draft";
        }


        public Activity MapToActivity(ActivityListItemViewModel model)
        {
            return new Activity
            {
                Id = model.Id,
                ReferenceId = model.ReferenceId,
                ParentReferenceId = model.ParentReferenceId,
                Name = model.Name,
                Description = model.Description,
                Category = model.Category,
                Processcategory = model.ProcessCategory,
                Milestone = model.Milestone,
                Actions = new ActionViewModel().MapToEnumerable(model.Actions).ToList(),
                Metadata = MapMetadata(model.Id, model.Metadata)
            };
        }

        private List<ActivityListItemViewModel> MapSubActivities(List<Activity> inputSubActivities)
        {
            List<ActivityListItemViewModel> subActivities = new List<ActivityListItemViewModel>();

            if (inputSubActivities?.Count > 0)
            {
                foreach (var subActivity in inputSubActivities)
                {
                    if (subActivity.Status == StatusForActivity.Published || subActivity.PublishedActivity_Id == null)
                    {
                        subActivities.Add(new ActivityListItemViewModel().Map(subActivity));
                    }
                }
            }

            return subActivities;
        }

        private List<ActivityMetadata> MapMetadata(int activityId, IEnumerable<MetadataViewModel> inputMetadatas)
        {
            List<ActivityMetadata> activitymetadatas = new List<ActivityMetadata>();

            if (inputMetadatas.Any())
            {
                foreach (var inputMetadata in inputMetadatas)
                {
                    activitymetadatas.Add(new ActivityMetadata().Map(activityId, inputMetadata));
                }
            }
            return activitymetadatas;
        }

        public override Activity Map(ActivityListItemViewModel input)
        {
            throw new NotImplementedException();
        }

        //private List<MetadataViewModel> MapMetadata(List<ActivityMetadata> activityMetadata)
        //{
        //    List<MetadataViewModel> metadataViewModels = new List<MetadataViewModel>();

        //    if (activityMetadata?.Count > 0)
        //    {
        //        foreach (var item in activityMetadata)
        //        {
        //            metadataViewModels.Add(new MetadataViewModel().Map(item));
        //        }
        //    }
        //    return metadataViewModels;

        //}
    }
}
