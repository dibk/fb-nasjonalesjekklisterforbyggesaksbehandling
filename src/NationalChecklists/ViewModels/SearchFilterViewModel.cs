﻿using NationalChecklists.ViewModels.ModelValidationAttributes;

namespace NationalChecklists.ViewModels;

public class SearchFilterViewModel
{
    public List<int> MetadataId { get; set; } = new List<int>();
    public List<int> MetadataTypeId { get; set; } = new List<int>();

    [StringElementsNotNull]
    public List<string> MetadataValue { get; set; } = new List<string>();

    public DateTime ValidDate { get; set; } = DateTime.Now;

    [StringElementsNotNull]
    public List<string> Tiltakstyper { get; set; } = new List<string>();

    [StringElementsNotNull]
    public List<string> Eier { get; set; } = new List<string>();

    public DateTime? OppdatertEtter { get; set; }
}