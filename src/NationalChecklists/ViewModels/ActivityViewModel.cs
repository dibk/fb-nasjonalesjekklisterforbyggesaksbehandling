﻿using NationalChecklists.Models;

namespace NationalChecklists.ViewModels
{
    public class ActivityViewModel : Mapper<Activity, ActivityViewModel>
    {
        public int Id { get; set; }
        public string ReferenceId { get; set; }
        public string ParentReferenceId { get; set; }
        public string Municipality { get; set; }
        public string Name { get; set; }
        public string NameNynorsk { get; set; }
        public string ProcessCategory { get; set; }
        public string Category { get; set; }
        public string ActivityType { get; set; }
        public string Milestone { get; set; }
        public string Description { get; set; }
        public string DescriptionNynorsk { get; set; }
        public string Documentation { get; set; }
        public string DocumentationNynorsk { get; set; }
        public string Image { get; set; }
        public string ImageDescription { get; set; }
        public bool HasRule { get; set; }
        public string Rule { get; set; }
        public string Status { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public int OrderNumber { get; set; }
        public int? PublishedActivity_Id { get; set; }
        public IEnumerable<ActionViewModel> Actions { get; set; }
        public IEnumerable<LawReferenceViewModel> LawReferences { get; set; }
        public IEnumerable<EnterpriseTermsViewModel> EnterpriseTerms { get; set; }
        public IEnumerable<MetadataViewModel> Metadata { get; set; }

        public override IEnumerable<ActivityViewModel> MapToEnumerable(IEnumerable<Activity> inputs)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Activity> MapToEnumerable(IEnumerable<ActivityViewModel> inputs)
        {
            throw new NotImplementedException();
        }

        public override ActivityViewModel Map(Activity input)
        {
            var viewModel = new ActivityViewModel();
            viewModel.Id = input.Id;
            viewModel.ReferenceId = input.ReferenceId;
            viewModel.ParentReferenceId = input.ParentReferenceId;
            viewModel.Name = input.Name;
            viewModel.NameNynorsk = input.NameNynorsk;
            viewModel.ProcessCategory = input.Processcategory;
            viewModel.Category = input.Category;
            viewModel.ActivityType = input.ActivityType;
            viewModel.Milestone = input.Milestone;
            viewModel.Description = input.Description;
            viewModel.DescriptionNynorsk = input.DescriptionNynorsk;
            viewModel.Documentation = input.Documentation;
            viewModel.DocumentationNynorsk = input.DocumentationNynorsk;

            if (input.Image != null)
            {
                string startBase64 = !string.IsNullOrWhiteSpace(input.ImageMimeType)
                    ? input.ImageMimeType + ", "
                    : string.Empty;

                viewModel.Image = startBase64 + Convert.ToBase64String(input.Image);
                viewModel.ImageDescription = input.ImageDescription;
            }



            viewModel.Municipality = input.Municipality;
            viewModel.HasRule = input.HasRule;
            viewModel.Rule = input.Rule;
            viewModel.Status = input.Status.ToString();
            viewModel.ValidFrom = input.ValidFrom;
            viewModel.ValidTo = input.ValidTo;
            viewModel.OrderNumber = input.OrderNumber;
            viewModel.PublishedActivity_Id = input.PublishedActivity_Id;
            viewModel.Actions = new ActionViewModel().MapToEnumerable(input.Actions);
            viewModel.LawReferences = new LawReferenceViewModel().MapToEnumerable(input.LawReferences);
            viewModel.EnterpriseTerms = new EnterpriseTermsViewModel().MapToEnumerable(input.EnterpriseTerms);
            viewModel.Metadata = MetadataViewModel.Map(input.Metadata);
            return viewModel;
        }

        public override Activity Map(ActivityViewModel input)
        {
            var activity = new Activity();
            activity.Id = input.Id;
            activity.ReferenceId = input.ReferenceId;
            activity.ParentReferenceId = input.ParentReferenceId;
            activity.Name = input.Name;
            activity.NameNynorsk = input.NameNynorsk;
            activity.Processcategory = input.ProcessCategory;
            activity.Category = input.Category;
            activity.ActivityType = input.ActivityType;
            activity.Milestone = input.Milestone;
            activity.Municipality = input.Municipality;
            activity.HasRule = input.HasRule;
            activity.Rule = input.Rule;
            activity.Status = (StatusForActivity)Enum.Parse(typeof(StatusForActivity), input.Status, true);
            activity.ValidFrom = input.ValidFrom;
            activity.ValidTo = input.ValidTo;
            activity.OrderNumber = input.OrderNumber;
            activity.PublishedActivity_Id = input.PublishedActivity_Id;
            activity.Description = input.Description;
            activity.Documentation = input.Documentation;
            activity.DocumentationNynorsk = input.DocumentationNynorsk;
            if (!string.IsNullOrEmpty(input.Image))
            {
                var imageBase64String = input.Image.Split(',');
                if (imageBase64String.Length > 1)
                {
                    try
                    {
                        activity.Image = Convert.FromBase64String(imageBase64String[1]);
                        activity.ImageMimeType = imageBase64String[0];
                        activity.ImageDescription = input.ImageDescription;
                    }
                    catch (Exception e)
                    {
                        // ignored
                    }
                }
            }

            activity.DescriptionNynorsk = input.DescriptionNynorsk;
            activity.Actions = new ActionViewModel().MapToEnumerable(input.Actions).ToList();
            activity.LawReferences = new LawReferenceViewModel().MapToEnumerable(input.LawReferences).ToList();
            activity.EnterpriseTerms = new EnterpriseTermsViewModel().MapToEnumerable(input.EnterpriseTerms).ToList();
            return activity;
        }

    }


}
