﻿using System.ComponentModel.DataAnnotations;

namespace NationalChecklists.ViewModels.ModelValidationAttributes;

public class StringElementsNotNullAttribute : ValidationAttribute
{
    public StringElementsNotNullAttribute()
    {
        ErrorMessage = "Query parameter '{0}' must have one or more elements when provided";
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        if (value == null)
            return ValidationResult.Success;

        var enumerable = value as IEnumerable<string>;

        foreach (var item in enumerable)
        {
            if (string.IsNullOrEmpty(item as string))
                return new ValidationResult(string.Format(ErrorMessage, validationContext.DisplayName.ToString()));
        }

        return ValidationResult.Success;
    }
}