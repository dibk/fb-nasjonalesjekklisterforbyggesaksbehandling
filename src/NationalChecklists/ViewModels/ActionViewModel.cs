﻿namespace NationalChecklists.ViewModels
{
    public class ActionViewModel : Mapper<Models.Action, ActionViewModel>
    {
        public int Id { get; set; }
        public bool ActionValue { get; set; }
        public string ActionType { get; set; }
        public string ActionTypeCode { get; set; }
        public int ActivityId { get; set; }
        public string ContentType { get; set; }

        public string TitleNb { get; set; }
        public string TitleNn { get; set; }
        public string DescriptionNb { get; set; }
        public string DescriptionNn { get; set; }
        public int? PublishedAction_Id { get; set; }


        public override IEnumerable<ActionViewModel> MapToEnumerable(IEnumerable<Models.Action> inputs)
        {
            var viewModels = new List<ActionViewModel>();
            if (inputs != null)
            {
                foreach (var input in inputs)
                {
                    ActionViewModel viewModel = Map(input);
                    viewModels.Add(viewModel);
                }
            }

            return viewModels;
        }

        public override IEnumerable<Models.Action> MapToEnumerable(IEnumerable<ActionViewModel> inputs)
        {
            var actions = new List<Models.Action>();
            if (inputs != null)
            {
                foreach (var input in inputs)
                {
                    Models.Action action = Map(input);
                    actions.Add(action);
                }
            }

            return actions;
        }


        public override ActionViewModel Map(Models.Action input)
        {
            return new ActionViewModel
            {
                Id = input.Id,
                ActionValue = input.ActionValue,
                ActionType = input.ActionType,
                ActionTypeCode = input.ActionTypeCode,
                ActivityId = input.ActivityId,
                ContentType = input.ContentType,
                TitleNb = input.TitleNb,
                TitleNn = input.TitleNn,
                DescriptionNb = input.DescriptionNb,
                DescriptionNn = input.DescriptionNn,
                PublishedAction_Id = input.PublishedAction_Id
            };
        }


        public override Models.Action Map(ActionViewModel input)
        {
            return new Models.Action
            {
                Id = input.Id,
                ActionValue = input.ActionValue,
                ActionType = input.ActionType,
                ActionTypeCode = input.ActionTypeCode,
                ActivityId = input.ActivityId,
                ContentType = input.ContentType,
                TitleNb = input.TitleNb,
                TitleNn = input.TitleNn,
                DescriptionNb = input.DescriptionNb,
                DescriptionNn = input.DescriptionNn,
                PublishedAction_Id = input.PublishedAction_Id
            };
        }
    }

    public class ActionTypeCodesWithValuesViewModel
    {
        public string ActionTypeCode { get; set; }
        public string ActionType { get; set; }
    }
}

