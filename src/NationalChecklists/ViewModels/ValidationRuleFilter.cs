﻿namespace NationalChecklists.ViewModels
{
    public class ValidationRuleFilter
    {
        public string ProcessCategory { get; set; }
        public List<string> EnterpriseTerms { get; set; }
        public DateTime? ValidDate { get; set; }
    }
}
