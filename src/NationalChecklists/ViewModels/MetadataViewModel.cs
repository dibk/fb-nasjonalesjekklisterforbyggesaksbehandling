﻿using NationalChecklists.Middleware.Models.Metadata;

namespace NationalChecklists.ViewModels
{
    public class MetadataViewModel
    {
        public int? MetadataId { get; set; }
        public string MetadataValue { get; set; }
        public string MetadataCode { get; set; }

        public int MetadataTypeId { get; set; }
        public string MetadataTypeValue { get; set; }

        public static IEnumerable<MetadataViewModel> Map(List<ActivityMetadata> metadataList)
        {
            var metadataViewModels = new List<MetadataViewModel>();
            if (metadataList != null)
            {
                foreach (var metadata in metadataList)
                {
                    var metadataViewModel = new MetadataViewModel();
                    metadataViewModel.MetadataId = metadata.MetadataId;
                    metadataViewModel.MetadataTypeId = metadata.MetadataTypeId;
                    if (metadata.MetadataItem != null)
                    {
                        metadataViewModel.MetadataValue = metadata.MetadataItem.Value;
                        metadataViewModel.MetadataCode = metadata.MetadataItem.MetadataCode;
                        metadataViewModel.MetadataTypeId = metadata.MetadataItem.Type.Id;
                        metadataViewModel.MetadataTypeValue = metadata.MetadataItem.Type.Name;

                    }

                    metadataViewModels.Add(metadataViewModel);
                }
            }
            return metadataViewModels;
        }

        public MetadataViewModel Map(ActivityMetadata activityMetadata) {
            return new MetadataViewModel
            {
                MetadataId = activityMetadata.MetadataId,
                MetadataValue = activityMetadata.MetadataItem.Value,
                MetadataCode = activityMetadata.MetadataItem.MetadataCode,
                MetadataTypeId = activityMetadata.MetadataItem.Type.Id,
                MetadataTypeValue = activityMetadata.MetadataItem.Type.Name
            };
        }
    }


}
