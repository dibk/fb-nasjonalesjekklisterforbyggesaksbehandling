﻿using NationalChecklists.Models;

namespace NationalChecklists.ViewModels
{
    public class ActivityListViewModel
    {
        public string Category { get; set; }
        public IEnumerable<ActivityListItemViewModel> Activities { get; set; }

        public List<ActivityListViewModel> Map(Dictionary<string, List<Activity>> inputs)
        {
            List<ActivityListViewModel> model = new List<ActivityListViewModel>();
            foreach (var input in inputs)
            {
                model.Add(new ActivityListViewModel
                {
                    Category = input.Key.TrimEnd(':', ','),
                    Activities = new ActivityListItemViewModel().MapToEnumerable(input.Value)
                });
            }

            return model;
        }
    }
}
