﻿namespace NationalChecklists.ViewModels
{
    public class ActivityActionValidationRulesViewModel
    {
        public ActivityActionValidationRulesViewModel()
        {
            ValidationRules = new List<ValidationRulesViewModel>();
        }

        public int ActivityId { get; set; }
        public int ActionId { get; set; }

        public List<ValidationRulesViewModel> ValidationRules { get; set; }
    }

    public class ActivityAndValidationIdsViewModel
    {
        public ActivityAndValidationIdsViewModel()
        {
            SupportingDataValidationRuleId = new List<ValidationRulesViewModel>();
        }

        public int ActivityId { get; set; }
        public int ActionId { get; set; }
        public string ProcessCategory { get; set; }
        public string ChecklistReference { get; set; }
        public bool ActionValue { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        public List<ValidationRulesViewModel> SupportingDataValidationRuleId { get; set; }
        public List<string> EnterpriseTerms { get; set; }
    }

    public class ValidationRulesViewModel
    {
        public ValidationRulesViewModel()
        {
            ValidationRuleIds = new List<string>();
        }

        public int Id { get; set; }
        public List<string> ValidationRuleIds { get; set; }
    }

    public class ActivityAndPrefillXpathViewModel
    {
        public int ActivityId { get; set; }
        public string ProcessCategory { get; set; }
        public string ChecklistReference { get; set; }
        public bool ActionValue { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }

        public string PrefillXpath { get; set; }
        public List<string> PrefillSelectedCodeValues { get; set; }
        public List<string> EnterpriseTerms { get; set; }
    }
}