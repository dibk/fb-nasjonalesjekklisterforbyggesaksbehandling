using System.ComponentModel.DataAnnotations;

namespace NationalChecklists.ViewModels
{
    public class ChecklistSearchViewModel : Mapper<ChecklistSearchViewModel, ChecklistSearchModel>
    {
        public List<string> EnterpriseTerms { get; set; }
        public List<string> Category { get; set; }
        public List<string> ActivityType { get; set; }
        public List<string> Milestone { get; set; }
        public List<string> Owner { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime? ValidDate { get; set; }
        public List<int> MetadataId { get; set; } = new List<int>();


        public override IEnumerable<ChecklistSearchModel> MapToEnumerable(IEnumerable<ChecklistSearchViewModel> inputs)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<ChecklistSearchViewModel> MapToEnumerable(IEnumerable<ChecklistSearchModel> inputs)
        {
            throw new NotImplementedException();
        }

        public override ChecklistSearchModel Map(ChecklistSearchViewModel input)
        {
            return new ChecklistSearchModel
            {
                EnterpriseTerms = input.EnterpriseTerms,
                Category = input.Category,
                ActivityType = input.ActivityType,
                Owner = input.Owner,
                Milestone = input.Milestone,
                ValidDate = input.ValidDate,
                MetadataId = input.MetadataId
            };
        }


        public override ChecklistSearchViewModel Map(ChecklistSearchModel input)
        {
            throw new NotImplementedException();
        }
    }

    public class ChecklistSearchModel
    {
        #nullable enable
        public List<string>? EnterpriseTerms { get; set; }
        public List<string>? Category { get; set; }
        public List<string>? ActivityType { get; set; }
        public List<string>? Milestone { get; set; }
        public List<string>? Owner { get; set; }
        public DateTime? ValidDate { get; set; }
        public List<int> MetadataId { get; set; } = new List<int>();

    }
}
