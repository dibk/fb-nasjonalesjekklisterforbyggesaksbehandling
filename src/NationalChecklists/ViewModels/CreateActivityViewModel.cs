using NationalChecklists.Models;
using NationalChecklists.Models.Facets;
using System.Collections;
using System.ComponentModel.DataAnnotations;
using ActivityType = NationalChecklists.Models.ActivityType;

namespace NationalChecklists.ViewModels
{
    public class CreateActivityViewModel
    {
        public string ParentReferenceId { get; set; }
        public string Name { get; set; }
        public string NameNynorsk { get; set; }
        [MinimumElements(1)]
        public List<string> ProcessCategories { get; set; } = new List<string>();
        //[NotNull]
        public Category Category { get; set; }
        public ActivityType ActivityType { get; set; }
        public Milestone Milestone { get; set; }
        public string Description { get; set; }
        public string DescriptionNynorsk { get; set; }
        public DateTime? ValidFrom { get; set; }
        public DateTime? ValidTo { get; set; }
        public IEnumerable<LawReferenceViewModel> LawReferences { get; set; } = new List<LawReferenceViewModel>();  
        public string Rule { get; set; }
        public IEnumerable<MetadataViewModel> Metadata { get; set; } = new List<MetadataViewModel>();

        public CreateActivityViewModel()
        {
            ProcessCategories = new List<string>();
        }

        public CreateActivityViewModel(Activity input)
        {
            ParentReferenceId = input.ParentReferenceId;
            Name = input.Name;
            if (input.Processcategory != null) ProcessCategories.Add(input.Processcategory);

            Description = input.Description;
            ValidFrom = input.ValidFrom;
            ValidTo = input.ValidTo;
            LawReferences = new LawReferenceViewModel().MapToEnumerable(input.LawReferences);
        }
    }

    public class MinimumElementsAttribute : ValidationAttribute
    {
        private readonly int minElements;

        public MinimumElementsAttribute(int minElements)
        {
            this.minElements = minElements;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var list = value as IList;

            var result = list?.Count >= minElements;

            return result
                ? ValidationResult.Success
                : new ValidationResult($"{validationContext.DisplayName} requires at least {minElements} element" + (minElements > 1 ? "s" : string.Empty));
        }
    }

    public class NotNull : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool result = false;
            
            if (value is Category category)
            {
                result = category.Id != null && category.Description != null;
            }

            return result
                ? ValidationResult.Success
                : new ValidationResult($"{validationContext.DisplayName} requires category");
        }
    }
}
