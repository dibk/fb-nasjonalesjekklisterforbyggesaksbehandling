using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NationalChecklists.ActionFilters;
using NationalChecklists.Middleware.Models.Prefill;
using NationalChecklists.Services;
using NationalChecklists.ViewModels;

namespace NationalChecklists.Controllers.ExternalApiControllers
{
    /// <summary>
    /// Prefill API for å hente ut sjekkpunkt som er relevante å preutfylle basert på søknadstype og tiltakstype(r)
    /// </summary>
    [Route("api/sjekkliste")]
    [JsonOutputFormatting]
    [EnableCors("AllowAll")]
    [ApiController]
    public class PrefillController : BaseController
    {
        private readonly IPrefillService _prefillService;

        public PrefillController(ILogger<PrefillController> logger, IPrefillService prefillService) : base(logger)
        {
            _prefillService = prefillService;
        }

        /// <summary>
        /// Preutfylling av sjekkpunkt der grunnlaget for prefill er valideringsresultatet fra ftb
        /// </summary>
        /// <param name="validationResult"></param>
        /// <returns></returns>
        [Route("prefill")]
        //[Authorize]
        [HttpPost]
        public async Task<List<PrefillActivityAction>> PrefillChecklist(PrefillChecklistInput validationResult)
        {
            var prefillOutcomes = await _prefillService.GetPrefill(null, validationResult);
            return prefillOutcomes;
        }

        /// <summary>
        /// Preutfylling av sjekkpunkt der grunnlaget for prefill ligger i søknaden.
        /// </summary>
        /// <param name="validationResult"></param>
        /// <returns></returns>
        [Route("prefill/xpath/{processCategory}")]
        //[Authorize]
        [HttpPost]
        public async Task<List<PrefillActivityAction>> PrefillChecklistByFormData(PrefillChecklistInputXpath formInformation)
        {
            ArgumentNullException.ThrowIfNull(formInformation, nameof(formInformation));
            ArgumentNullException.ThrowIfNull(formInformation.Form, nameof(formInformation.Form));
            ArgumentNullException.ThrowIfNull(formInformation.ProcessCategory, nameof(formInformation.ProcessCategory));

            var prefillOutcomes = await _prefillService.GetPrefill(formInformation.ProcessCategory, formInformation.EnterpriseTerms, formInformation.Form);
            return prefillOutcomes;
        }

        /// <summary>
        /// Koble sjekkpunkt og valideringsregel
        /// </summary>
        /// <param name="activityActionValidationRulesViewModel"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("valideringsregler")]
        [Authorize]
        [HttpPost]
        public ActionResult UpdateActivityActionTypeWithValidationId(List<ActivityActionValidationRulesViewModel> activityActionValidationRulesViewModel)
        {
            _prefillService.AddorUpdateValidationIds(activityActionValidationRulesViewModel);
            return Ok();
        }

        /// <summary>
        /// Alle sjekkpunkt som er koblet til en eller flere valideringsregler og dermed kan preutfylles ut fra valideringsresultatet i ftb.
        /// </summary>
        /// <param name="validationRuleFilter"></param>
        /// <param name="processCategory"></param>
        /// <returns></returns>
        [Route("valideringsregler/")]
        [Route("valideringsregler/{ProcessCategory}")]
        //[Authorize]
        [HttpGet]
        public List<ActivityAndValidationIdsViewModel> GetActivityActionTypeWithValidationId([FromQuery] ValidationRuleFilter validationRuleFilter, string processCategory)
        {
            if (processCategory != null) validationRuleFilter.ProcessCategory = processCategory;
            return _prefillService.GetActivitiesAndValidationRuleIds(validationRuleFilter);
        }

        /// <summary>
        /// Slett kobling mellom sjekkpunkt og valideringsregel
        /// </summary>
        /// <param name="actionValidationRuleId"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [Route("valideringsregler/")]
        [Authorize]
        [HttpDelete]
        public ActionResult DeleteActionValidationRule(int actionValidationRuleId)
        {
            _prefillService.DeleteActionValidationRuleById(actionValidationRuleId);
            return Ok();
        }

        /// <summary>
        /// Demo av preutfylling. Viser alle sjekkpunkt i den aktuelle prosesskategorien som kan preutfylles i Ftb.
        /// </summary>
        /// <param name="processCategory"></param>
        /// <returns>Liste over alle sjekkpunkt som har blitt preutfylt. Alle har fått positivt resultat.</returns>
        [Route("prefill-demo/{ProcessCategory}")]
        [HttpGet]
        public async Task<List<PrefillActivityActionDemo>> PrefillChecklistDemo(string processCategory)
        {
            var prefillOutcomes = await _prefillService.GetPrefillDemo(processCategory);
            return prefillOutcomes.OrderBy(c => c.ChecklistReference.Length).ThenBy(c => c.ChecklistReference).ToList();
        }

        /// <summary>
        /// Oversikt over alle sjekkpunkt som kan preutfylles i Ftb. Den viser hvilke utfall som kan oppstå og hvilke tiltakstyper sjekkpunktet gjelder for.
        /// </summary>
        /// <param name="processCategory"></param>
        /// <returns></returns>
        [Route("prefillinformation/{processCategory}")]
        [HttpGet]
        public async Task<PrefillInformationApiModel> PrefillChecklistInformation(string processCategory)
        {
            var prefillInformation = await _prefillService.GetPrefillInformation(processCategory);
            return prefillInformation;
        }
    }
}