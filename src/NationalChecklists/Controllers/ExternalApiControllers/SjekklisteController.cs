﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NationalChecklists.ActionFilters;
using NationalChecklists.Models.ExternalApiModels;
using NationalChecklists.Services;
using NationalChecklists.Services.ExternalApiServices;
using NationalChecklists.ViewModels;
using MetadataTypeViewModel = NationalChecklists.Middleware.Models.ExternalApiModels.MetadataTypeViewModel;

namespace NationalChecklists.Controllers.ExternalApiControllers
{
    /// <summary>
    ///     Åpne API for sjekklister fra fellestjenester bygg
    /// </summary>
    [JsonOutputFormatting]
    [EnableCors("AllowAll")]
    [ApiController]
    public class SjekklisteController : ControllerBase
    {
        private readonly IActivityServiceForExternalApi _activityServiceForExternalApi;
        private readonly IMetadataService _metadataService;
        private readonly ILogger _logger;


        public SjekklisteController(IActivityServiceForExternalApi activityServiceForExternalApi, ILogger<SjekklisteController> logger, IMetadataService metadataService)
        {
            _activityServiceForExternalApi = activityServiceForExternalApi;
            _logger = logger;
            _metadataService = metadataService;
        }

        /// <summary>
        /// Hente ut SAK10 sjekkliste for en søknadstype/prosesskategori, milepel og tiltakstyper
        /// </summary>
        /// <param name="searchFilterModel"></param>
        /// <param name="prosesskategori"></param>
        /// <param name="milepel"></param>
        /// <returns>Sjekkliste</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="404">NotFound - finner ikke sjekkliste for prosesskategori, milepel og tiltakstyper</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/sjekkliste")]
        [Route("api/sjekkliste/{prosesskategori}")]
        [HttpGet]
        public async Task<ActionResult<Sjekk[]>> GetSjekkliste([FromQuery] SearchFilterViewModel searchFilterModel, string prosesskategori = null)
        {


            try
            {
                _logger.LogDebug($"API request for filtering of checklists by processCategory {prosesskategori}");
                
                var activitiesList = await _activityServiceForExternalApi.GetActivities(prosesskategori, searchFilterModel.Tiltakstyper, string.Empty, searchFilterModel);

                var checklist = _activityServiceForExternalApi.ToCheckList(activitiesList);

                return checklist.ToArray();
            }

            catch (Exception ex)
            {
                _logger.LogError($"Exception i sjekkliste API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                throw;
            }
        }


        [Route("api/sjekkliste/{prosesskategori}/milepel/{milepel}")]
        [HttpGet]
        public async Task<ActionResult<Sjekk[]>> GetSjekklisteMilepel([FromQuery] SearchFilterViewModel searchFilterModel, string prosesskategori = null,
            string milepel = null)
        {
            try
            {
                _logger.LogDebug($"API request for filtering of checklists by processCategory {prosesskategori}, Milestone {milepel}");

                var activitiesList = await _activityServiceForExternalApi.GetActivities(prosesskategori, searchFilterModel.Tiltakstyper, milepel, searchFilterModel);

                var checklist = _activityServiceForExternalApi.ToCheckList(activitiesList);

                return checklist.ToArray();
            }

            catch (Exception ex)
            {
                _logger.LogError($"Exception i sjekkliste API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                throw;
            }
        }



        /// <summary>
        ///     Hente ut tiltakstyper for en søknadstype/prosesskategori og sjekkpunkt
        /// </summary>
        /// <returns>Liste med tiltakstyper</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/tiltakstyper/{prosesskategori}/sjekkpunkt/{pkt}")]
        [HttpGet]
        public ActionResult<string[]> GetTiltakstyper(string prosesskategori, string pkt)
        {
            try
            {
                var enterpriseTermsList = _activityServiceForExternalApi.GetActivityEnterpriseTerms(pkt, prosesskategori);

                return enterpriseTermsList;
            }

            catch (Exception ex)
            {
                _logger.LogError($"Exception i GetTiltakstyper API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                throw;
            }
        }

        /// <summary>
        ///     Hente ut informasjon om et sjekkpunkt
        /// </summary>
        /// <returns>Sjekkpunkt</returns>
        /// <response code="200">ok</response>
        /// <response code="400">BadRequest - ugyldig forespørsel</response>
        /// <response code="500">Error - intern feil</response>
        [Route("api/sjekkpunkt/{prosesskategori}/sjekkpunkt/{pkt}")]
        [HttpGet]
        public ActionResult<Sjekk> GetSjekkpunkt(string prosesskategori, string pkt)
        {
            try
            {
                var sjekkpkt = new Sjekk(_activityServiceForExternalApi.GetActivity(pkt, prosesskategori));

                return sjekkpkt;
            }

            catch (Exception ex)
            {
                _logger.LogError($"Exception i GetSjekkpunkt API kall: {ex.ToString()}, {ex.Message}, {ex.StackTrace}");
                throw;
            }
        }


        //[ApiExplorerSettings(IgnoreApi = true)]
        [Route("api/metadatatyper")]
        [HttpGet]
        public async Task<List<MetadataTypeViewModel>> GetMetadataTypes()
        {
            var metadata = await _metadataService.GetMetadataTypesAsync(null);

            return new MetadataTypeViewModel().Map(metadata);
        }
    }
}
