﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NationalChecklists.Middleware.Models;
using NationalChecklists.Middleware.Models.Admin;
using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models.Facets;
using NationalChecklists.Services;
using ActivityType = NationalChecklists.Models.Facets.ActivityType;

namespace NationalChecklists.Controllers
{
    /// <summary>
    /// Brukes for å konfigurere sjekklisteløsningen
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/admin")]
    [ApiController]
    [EnableCors("AllowAll")]
    public class AdminController : BaseController
    {
        private readonly IFacetService _facetService;
        private readonly IMetadataService _metadataService;
        private readonly IPrefillService _prefillService;

        public AdminController(
            IFacetService facetService, 
            IMetadataService metadataService,
            ILogger<AdminController> logger,
            IPrefillService prefillService) : base(logger)
        
        {
            _facetService = facetService;
            _metadataService = metadataService;
            _prefillService = prefillService;
        }

        
        [Route("ProcessCategories")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddProcessCategories(List<ProcessCategory> processCategories)
        {
            try
            {
                if (processCategories == null || !processCategories.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(processCategories);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        
        [Route("ProcessCategories")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateProcessCategories(List<ProcessCategory> processCategories)
        {
            try
            {
                if (processCategories == null || !processCategories.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(processCategories, StaticMetadata.ProcessCategory);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        
        [Route("Milestones")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddMilestones(List<Milestone> milestones)
        {
            try
            {
                if (milestones == null || !milestones.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(milestones);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

       
        [Route("Milestones")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateMilestones(List<Milestone> milestones)
        {
            try
            {
                if (milestones == null || !milestones.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(milestones, StaticMetadata.Milestone);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }


        [Route("ActivityTypes")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddActivityTypes(List<ActivityType> activityTypes)
        {
            try
            {
                if (activityTypes == null || !activityTypes.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(activityTypes);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("ActivityTypes")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateActivityTypes(List<ActivityType> activityTypes)
        {
            try
            {
                if (activityTypes == null || !activityTypes.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(activityTypes, StaticMetadata.ActivityType);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("Categories")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddCategories(List<Category> categories)
        {
            try
            {
                if (categories == null || !categories.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(categories);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("Categories")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateCategories(List<Category> categories)
        {
            try
            {
                if (categories == null || !categories.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(categories, StaticMetadata.Category);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("EnterpriseTerms")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddEnterpriseTerms(List<EnterpriseTermFacet> enterpriseTerms)
        {
            try
            {
                if (enterpriseTerms == null || !enterpriseTerms.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(enterpriseTerms);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("EnterpriseTerms")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateEnterpriseTerms(List<EnterpriseTermFacet> enterpriseTerms)
        {
            try
            {
                if (enterpriseTerms == null || !enterpriseTerms.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(enterpriseTerms, StaticMetadata.EnterpriceTerms);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("Owners")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddOwners(List<Owner> owners)
        {
            try
            {
                if (owners == null || !owners.Any())
                    return BadRequest();

                await _facetService.AddFacetsAsync(owners);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("Owners")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateOwners(List<Owner> owners)
        {
            try
            {
                if (owners == null || !owners.Any())
                    return BadRequest();

                await _facetService.UpdateFacetsAsync(owners, StaticMetadata.Owner);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        [Route("Facets")]
        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteFacets(List<string> facetIds)
        {
            try
            {
                if (facetIds == null || !facetIds.Any())
                    return BadRequest();

                await _facetService.DeleteFacetsAsync(facetIds);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Legg til metadataverdier for en gitt metadatatype
        /// </summary>
        /// <param name="metadatas"></param>
        /// <returns></returns>
        [Route("Metadatas")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddMetadatas(List<Metadata> metadatas)
        {
            try
            {
                if (metadatas == null || !metadatas.Any())
                    return BadRequest();

                await _metadataService.AddMetadatasAsync(metadatas);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Oppdater metadataverdier for en gitt metadatatype
        /// </summary>
        /// <param name="metadatas"></param>
        /// <returns></returns>
        [Route("Metadatas")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateMetadatas(List<Metadata> metadatas)
        {
            try
            {
                if (metadatas == null || !metadatas.Any())
                    return BadRequest();

                await _metadataService.UpdateMetadatasAsync(metadatas);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Slett metadataverdier for en gitt metadatatype
        /// </summary>
        /// <param name="metadataIds"></param>
        /// <returns></returns>
        [Route("Metadatas")]
        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteMetadatas(List<int> metadataIds)
        {
            try
            {
                if (metadataIds == null || !metadataIds.Any())
                    return BadRequest();

                await _metadataService.DeleteMetadatasAsync(metadataIds);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Legg til metadatatype
        /// </summary>
        /// <param name="metadataTypes"></param>
        /// <returns></returns>
        [Route("MetadataTypes")]
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddMetadataTypes(List<MetadataTypeViewModel> metadataTypes)
        {
            try
            {
                if (metadataTypes == null || !metadataTypes.Any())
                    return BadRequest();

                await _metadataService.AddMetadataTypesAsync(metadataTypes);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Oppdater metadatatype
        /// </summary>
        /// <param name="metadataTypes"></param>
        /// <returns></returns>
        [Route("MetadataTypes")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateMetadataTypes(List<MetadataTypeViewModel> metadataTypes)
        {
            try
            {
                if (metadataTypes == null || !metadataTypes.Any())
                    return BadRequest();

                await _metadataService.UpdateMetadataTypesAsync(metadataTypes);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Slett metadatatype
        /// </summary>
        /// <param name="metadataTypes"></param>
        /// <returns></returns>
        [Route("MetadataTypes")]
        [Authorize]
        [HttpDelete]
        public async Task<IActionResult> DeleteMetadataTypes(List<MetadataTypeViewModel> metadataTypes)
        {
            try
            {
                if (metadataTypes == null || !metadataTypes.Any())
                    return BadRequest();

                await _metadataService.DeleteMetadataTypesAsync(metadataTypes);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Oppdater faste metadata
        /// </summary>
        /// <param name="staticMetadatas"></param>
        /// <returns></returns>
        [Route("StaticMetadatas")]
        [Authorize]
        [HttpPut]
        public async Task<IActionResult> UpdateStaticMetadatas(List<StaticMetadata> staticMetadatas)
        {
            try
            {
                if (staticMetadatas == null || !staticMetadatas.Any())
                    return BadRequest();

                await _metadataService.UpdateStaticMetadatasAsync(staticMetadatas);

                return Ok();
            }
            catch (Exception exception)
            {
                var result = HandleException(exception);

                if (result != null)
                    return result;

                throw;
            }
        }

        /// <summary>
        /// Hent alle sjekkpunkt som har kobling til en eller flere valideringsregler
        /// </summary>
        /// <param name="processCategory"></param>
        /// <returns></returns>
        [Route("activities/actions")]
        [HttpGet]
        public async Task<List<AdminActivityActionValidationRulesViewModel>> GetActivityWithActions([FromQuery] string processCategory)
        {
            var activities = await _prefillService.GetActivitiesAndActions(processCategory);
            return AdminActivityActionValidationRulesViewModel.Map(activities);
        }
    }
}