using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using NationalChecklists.Logger;
using NationalChecklists.Middleware.Models;
using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;
using NationalChecklists.Services;
using NationalChecklists.Utils;
using NationalChecklists.ViewModels;

namespace NationalChecklists.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    [EnableCors("Internal")]
    public class ChecklistApiController : ControllerBase
    {
        private readonly IActivityServiceForInternalApi _activityServiceForInternalApi;
        private readonly Microsoft.Extensions.Logging.ILogger _logger;
        private readonly IFacetService _facetService;
        private readonly IMetadataService _metadataService;


        public ChecklistApiController(IActivityServiceForInternalApi activityServiceForInternalApi, ILogger<ChecklistApiController> logger, IFacetService facetService, IMetadataService metadataService)
        {
            _activityServiceForInternalApi = activityServiceForInternalApi;
            _logger = logger;
            _facetService = facetService;
            _metadataService = metadataService;
            //_logger = ApplicationLogging.CreateLogger();
        }

        /// <summary>
        /// S�knadstyper
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> Index()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();
            return new FilterDataForChecklist(staticMetadataTypes).ProcessCategories;
        }

        /// <summary>
        /// Alle faste filter for en sjekkliste
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/static-metadata-filters")]
        [HttpGet]
        public async Task<List<StaticMetadata>> GetStaticMetadataFilters()
        {
            return await _facetService.GetStaticMetadataFiltersAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="showAsFacetFilter">Filterdata som skal vises som et facet filter. </param>
        /// <param name="showOnDetailPage">Filter som skal vises p� detaljsiden</param>
        /// <returns></returns>
        [Route("checklist-api/filter-data")]
        [HttpGet]
        public async Task<List<FilterTypeForChecklist>> GetFilterData(bool? showAsFacetFilter = null, bool? showOnDetailPage = null)
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync(showAsFacetFilter, showOnDetailPage);
            var dynamicMetadataTypes = await _metadataService.GetMetadataTypesAsync(null, showAsFacetFilter);

            return new FilterDataForChecklist(staticMetadataTypes, dynamicMetadataTypes).FiltersData;
        }

        /// <summary>
        /// Alle filter
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/metadataTypes")]
        [HttpGet]
        public async Task<List<MetadataType>> GetMetadataTypes([FromQuery] MetadataFilter metadataFilter)
        {
            return await _metadataService.GetMetadataTypesAsync(metadataFilter);
        }

        /// <summary>
        /// Filter av typen "Tema"
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/categories")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> GetCategories()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();

            return new FilterDataForChecklist(staticMetadataTypes).Categories;
        }

        /// <summary>
        /// Filter av typen "Tiltakstype"
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/enterprise-terms")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> GetEnterpriseTerms()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();

            return new FilterDataForChecklist(staticMetadataTypes).EnterpriseTerms;
        }

        /// <summary>
        /// Filter av typen "Eier"
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/owner")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> GetOwners()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();

            return new FilterDataForChecklist(staticMetadataTypes).Owners;
        }

        /// <summary>
        /// Filter av typen "Type sjekk"
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/activity-types")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> GetActivityTypes()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();

            return new FilterDataForChecklist(staticMetadataTypes).ActivityTypes;
        }

        /// <summary>
        /// Filter av typen "Milepel"
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/milestones")]
        [HttpGet]
        public async Task<FilterTypeForChecklist> GetMilestones()
        {
            var staticMetadataTypes = await _facetService.GetStaticMetadataTypesAsync();

            return new FilterDataForChecklist(staticMetadataTypes).Milestone;
        }

        /// <summary>
        /// Resultat/Utfall
        /// </summary>
        /// <returns></returns>
        [Route("checklist-api/actions")]
        [HttpGet]
        public async Task<IEnumerable<ActionTypeCodesWithValuesViewModel>> GetActions()
        {
            return await _activityServiceForInternalApi.GetActions();
        }

        /// <summary>
        /// Gir alle sjekkpunkt knyttet til en s�knadstype. Mulighet for filtrering.
        /// </summary>
        /// <param name="searchModel">Filtrering</param>
        /// <param name="processCategory">S�knadstype kode</param>
        /// <returns></returns>
        [Route("checklist-api/{processCategory}")]
        [HttpGet]
        public async Task<IEnumerable<ActivityListViewModel>> GetAll([FromQuery] ChecklistSearchViewModel searchModel, string processCategory)
        {
            var viewModel = new List<ActivityListViewModel>();

            if (!string.IsNullOrEmpty(processCategory) && await _facetService.ProcessCategoryExistsAsync(processCategory))
            {
                _logger.LogDebug($"API request for filtering of checklists by processCategory {processCategory}, EnterpriseTerms {searchModel.EnterpriseTerms}," +
                                 $"Category {searchModel.Category}, ActivityType {searchModel.ActivityType}, Milestone {searchModel.Milestone}");

                Dictionary<string, List<Activity>> activities;

                var isAuthenticated = User.Identity.IsAuthenticated;

                using (var logger = new PerformanceTimeLogger(_logger, $"checklist-api/{processCategory} Data retrieval"))
                {
                    searchModel = await _facetService.MapIdToDescriptionAsync(searchModel);
                    activities = await _activityServiceForInternalApi.GetActivities(searchModel.Map(searchModel), processCategory, isAuthenticated);
                }

                using (var logger = new PerformanceTimeLogger(_logger, $"checklist-api/{processCategory} Mapping to view model"))
                {
                    viewModel = new ActivityListViewModel().Map(activities);
                }
            }

            return viewModel;
        }


        [Route("checklist-api/detail/{id}")]
        [HttpGet]
        public async Task<ActionResult<ActivityViewModel>> Detail(int id)
        {
            if (id <= 0)
                return new BadRequestResult();

            _logger.LogDebug($"API request for details of the checkpoint with Id {id}");
            var checklistItem = await _activityServiceForInternalApi.GetActivity(id);

            return checklistItem != null ? (ActionResult<ActivityViewModel>)new ActivityViewModel().Map(checklistItem) : new BadRequestResult();

        }

        [Route("checklist-api/draft")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<CreateActivityViewModel>> CreateNew(string parentReferenceId = null)
        {
            _logger.LogDebug($"API request to create a draft for a new checkpoint");
            var draft = new Activity(parentReferenceId);
            var draftVm = new CreateActivityViewModel(draft);

            if (parentReferenceId != null)
            {
                _logger.LogDebug($"create new checkpoit with parentreferenceId {parentReferenceId}");
                draftVm.ProcessCategories = _activityServiceForInternalApi.GetProcessCategoriesOnReferenceId(parentReferenceId);
                draftVm.Category = _activityServiceForInternalApi.GetCategoryByReferenceId(parentReferenceId);
            }
            return draftVm;
        }


        /// <summary>
        /// New activity. First step as draft...
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("checklist-api/draft")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        public async Task<ActionResult<List<ActivityViewModel>>> CreateNewDraft([FromBody] CreateActivityViewModel model)
        {
            if (model == null)
                return new BadRequestResult();
            using (var logger = new PerformanceTimeLogger(_logger, $"checklist-api/draft Create draft for new checkpints"))
            {
                var drafts = await _activityServiceForInternalApi.CreateDraftsForNewCheckpoit(model);

                if (drafts == null)
                {
                    return StatusCode(500);
                }

                var draftVm = new List<ActivityViewModel>();
                foreach (var draft in drafts)
                {
                    draftVm.Add(new ActivityViewModel().Map(draft));
                }

                logger.Dispose();

                return draftVm;
            }

        }

        [Route("checklist-api/drafts")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        [HttpGet]
        public async Task<ActionResult<List<ActivityViewModel>>> GetDrafts()
        {
            _logger.LogDebug($"API request for drafts");
            var drafts = await _activityServiceForInternalApi.GetDraftActivities();
            var draftsVm = new List<ActivityViewModel>();
            foreach (var draft in drafts)
            {
                draftsVm.Add(new ActivityViewModel().Map(draft));
            }

            return draftsVm;
        }

        [Route("checklist-api/draft/{id}")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        [HttpPost]
        public async Task<ActionResult<ActivityViewModel>> CreateDraft(int id)
        {
            _logger.LogDebug($"API request to create a draft for checkpoint with Id {id}");
            var createdDraft = await _activityServiceForInternalApi.CreateDraftForActivity(id);

            if (createdDraft == null)
            {
                return new BadRequestResult();
            }

            return new ActivityViewModel().Map(createdDraft);
        }

        [Route("checklist-api/draft/{id}")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        [HttpDelete]
        public async Task<ActionResult> DeleteDraft(int id)
        {
            if (id <= 0)
                return new BadRequestResult();

            _logger.LogDebug($"API request to delete the draft with Id {id}");
            await _activityServiceForInternalApi.DeleteDraft(id);
            return new OkResult();
        }

        [HttpPut]
        [Route("checklist-api/draft/{id}")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        public async Task<ActionResult<ActivityViewModel>> UpdateDraft(int id, [FromBody] ActivityViewModel model)
        {
            if (id <= 0 || model == null)
                return new BadRequestResult();

            var addOrUpdatedMetadata = await _metadataService.AddOrUpdateMetadataAsync(model.Metadata, id);
            var activityMetadata = _metadataService.MapToActivityMetadata(addOrUpdatedMetadata);
            var updatedDraft = new ActivityViewModel().Map(model);
            updatedDraft.Metadata = activityMetadata;
            updatedDraft.Updated = DateTime.Now;

            _logger.LogDebug($"API request for updating of the draft with Id {id} with {model} values");
            var activity = await _activityServiceForInternalApi.UpdateDraftActivity(updatedDraft);
            return new ActivityViewModel().Map(activity);
        }

        [HttpPut]
        [Route("checklist-api/activity/{draftActivityId}")]
        //[Authorize(Roles = ApplicationRole.Administrator)]
        [Authorize]
        public async Task<ActionResult<ActivityViewModel>> UpdatePublishedActivity(int draftActivityId)
        {
            _logger.LogDebug($"API request for published checkpoint updating with values from the draft checkpoint with Id {draftActivityId}");
            var activity = await _activityServiceForInternalApi.UpdatePublishedActivity(draftActivityId);
            if (activity != null)
                return new ActivityViewModel().Map(activity);

            return BadRequest();
        }
    }
}
