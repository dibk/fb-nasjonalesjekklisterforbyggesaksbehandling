﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class DeleteDeviationTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Activity_Deviation_DeviationId",
                table: "Activity");

            migrationBuilder.DropTable(
                name: "Deviation");

            migrationBuilder.DropIndex(
                name: "IX_Activity_DeviationId",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "DeviationId",
                table: "Activity");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DeviationId",
                table: "Activity",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Deviation",
                columns: table => new
                {
                    DeviationId = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Title = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Deviation", x => x.DeviationId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Activity_DeviationId",
                table: "Activity",
                column: "DeviationId",
                unique: true,
                filter: "[DeviationId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Activity_Deviation_DeviationId",
                table: "Activity",
                column: "DeviationId",
                principalTable: "Deviation",
                principalColumn: "DeviationId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
