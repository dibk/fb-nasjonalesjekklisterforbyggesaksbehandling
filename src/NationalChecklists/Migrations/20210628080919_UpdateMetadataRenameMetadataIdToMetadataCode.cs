﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class UpdateMetadataRenameMetadataIdToMetadataCode : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MetadataId",
                table: "Metadatas");

            migrationBuilder.AddColumn<string>(
                name: "MetadataCode",
                table: "Metadatas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "MetadataCode",
                table: "Metadatas");

            migrationBuilder.AddColumn<string>(
                name: "MetadataId",
                table: "Metadatas",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
