﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddCommonValueAndFacet : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CommonValue",
                table: "MetadataTypes",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Facet",
                table: "MetadataTypes",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CommonValue",
                table: "MetadataTypes");

            migrationBuilder.DropColumn(
                name: "Facet",
                table: "MetadataTypes");
        }
    }
}
