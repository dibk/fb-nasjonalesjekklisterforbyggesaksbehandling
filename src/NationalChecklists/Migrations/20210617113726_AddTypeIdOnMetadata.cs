﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddTypeIdOnMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Metadatas_MetadataTypes_TypeId",
                table: "Metadatas");

            migrationBuilder.AlterColumn<int>(
                name: "TypeId",
                table: "Metadatas",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Metadatas_MetadataTypes_TypeId",
                table: "Metadatas",
                column: "TypeId",
                principalTable: "MetadataTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Metadatas_MetadataTypes_TypeId",
                table: "Metadatas");

            migrationBuilder.AlterColumn<int>(
                name: "TypeId",
                table: "Metadatas",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Metadatas_MetadataTypes_TypeId",
                table: "Metadatas",
                column: "TypeId",
                principalTable: "MetadataTypes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
