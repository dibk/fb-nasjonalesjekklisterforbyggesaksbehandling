﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddActionTextContentType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ContentType",
                table: "Action",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ContentType",
                table: "Action");
        }
    }
}
