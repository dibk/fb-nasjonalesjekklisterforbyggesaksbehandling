﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class UpdateMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Name",
                table: "Metadatas");

            migrationBuilder.AlterColumn<int>(
                name: "MaxCountOfValues",
                table: "MetadataTypes",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Value",
                table: "Metadatas",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Value",
                table: "Metadatas");

            migrationBuilder.AlterColumn<int>(
                name: "MaxCountOfValues",
                table: "MetadataTypes",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddColumn<string>(
                name: "Name",
                table: "Metadatas",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
