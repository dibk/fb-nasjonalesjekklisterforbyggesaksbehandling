﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NationalChecklists.Migrations
{
    public partial class AddPublishedActionId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AddColumn<int>(
                name: "PublishedAction_Id",
                table: "Action",
                type: "int",
                nullable: true);

        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.DropColumn(
                name: "PublishedAction_Id",
                table: "Action");

        }
    }
}
