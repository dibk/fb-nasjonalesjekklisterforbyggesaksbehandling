﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddSupportiveValidationRulesToActions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ActionValidationRules",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ActionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActionValidationRules", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ActionValidationRules_Action_ActionId",
                        column: x => x.ActionId,
                        principalTable: "Action",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ValidationRule",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ValidationRuleId = table.Column<string>(nullable: false),
                    ActionValidationRulesId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidationRule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ValidationRule_ActionValidationRules_ActionValidationRulesId",
                        column: x => x.ActionValidationRulesId,
                        principalTable: "ActionValidationRules",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActionValidationRules_ActionId",
                table: "ActionValidationRules",
                column: "ActionId");

            migrationBuilder.CreateIndex(
                name: "IX_ValidationRule_ActionValidationRulesId",
                table: "ValidationRule",
                column: "ActionValidationRulesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ValidationRule");

            migrationBuilder.DropTable(
                name: "ActionValidationRules");
        }
    }
}
