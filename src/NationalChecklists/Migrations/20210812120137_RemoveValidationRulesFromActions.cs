﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class RemoveValidationRulesFromActions : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ValidationRule");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ValidationRule",
                columns: table => new
                {
                    ValidationRuleId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    VersionId = table.Column<int>(type: "int", nullable: false),
                    ActionId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ValidationRule", x => new { x.ValidationRuleId, x.VersionId });
                    table.ForeignKey(
                        name: "FK_ValidationRule_Action_ActionId",
                        column: x => x.ActionId,
                        principalTable: "Action",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ValidationRule_ActionId",
                table: "ValidationRule",
                column: "ActionId");
        }
    }
}
