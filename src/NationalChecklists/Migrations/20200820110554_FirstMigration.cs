﻿using System;
using System.Configuration;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.Extensions.Configuration;
using NationalChecklists.Configuration;

namespace NationalChecklists.Migrations
{
    public partial class FirstMigration : Migration
    {

        protected override void Up(MigrationBuilder migrationBuilder)
        {
            if (!Exists("dbo.Activity"))
            {
                migrationBuilder.CreateTable(
                    name: "Activity",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        ReferenceId = table.Column<string>(nullable: true),
                        ParentReferenceId = table.Column<string>(nullable: true),
                        Municipality = table.Column<string>(nullable: true),
                        Name = table.Column<string>(nullable: true),
                        ActivityType = table.Column<string>(nullable: true),
                        Description = table.Column<string>(nullable: true),
                        Category = table.Column<string>(nullable: true),
                        Processcategory = table.Column<string>(nullable: true),
                        Milestone = table.Column<string>(nullable: true),
                        Status = table.Column<int>(nullable: false),
                        HasRule = table.Column<bool>(nullable: false),
                        Rule = table.Column<string>(nullable: true),
                        Activity_Id = table.Column<int>(nullable: true),
                        PublishedActivity_Id = table.Column<int>(nullable: true),
                        ValidFrom = table.Column<DateTime>(nullable: true),
                        ValidTo = table.Column<DateTime>(nullable: true),
                        Updated = table.Column<DateTime>(nullable: false),
                        OrderNumber = table.Column<int>(nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Activity", x => x.Id);
                        table.ForeignKey(
                            name: "FK_Activity_Activity_Activity_Id",
                            column: x => x.Activity_Id,
                            principalTable: "Activity",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Restrict);
                    });
            }

            if (!Exists("dbo.Action"))
            {

                migrationBuilder.CreateTable(
                    name: "Action",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        ActionValue = table.Column<bool>(nullable: false),
                        ActionType = table.Column<string>(nullable: true),
                        ActionTypeCode = table.Column<string>(nullable: true),
                        ActivityId = table.Column<int>(nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_Action", x => x.Id);
                        table.ForeignKey(
                            name: "FK_Action_Activity_ActivityId",
                            column: x => x.ActivityId,
                            principalTable: "Activity",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade);
                    });
            }

            if (!Exists("dbo.EnterpriseTerm"))
            {
                migrationBuilder.CreateTable(
                    name: "EnterpriseTerm",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        Code = table.Column<string>(nullable: true),
                        ActivityId = table.Column<int>(nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_EnterpriseTerm", x => x.Id);
                        table.ForeignKey(
                            name: "FK_EnterpriseTerm_Activity_ActivityId",
                            column: x => x.ActivityId,
                            principalTable: "Activity",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade);
                    });
            }


            if (!Exists("dbo.LawReference"))
            {
                migrationBuilder.CreateTable(
                    name: "LawReference",
                    columns: table => new
                    {
                        Id = table.Column<int>(nullable: false)
                            .Annotation("SqlServer:Identity", "1, 1"),
                        LawReferenceDescription = table.Column<string>(nullable: true),
                        LawReferenceUrl = table.Column<string>(nullable: true),
                        ActivityId = table.Column<int>(nullable: false)
                    },
                    constraints: table =>
                    {
                        table.PrimaryKey("PK_LawReference", x => x.Id);
                        table.ForeignKey(
                            name: "FK_LawReference_Activity_ActivityId",
                            column: x => x.ActivityId,
                            principalTable: "Activity",
                            principalColumn: "Id",
                            onDelete: ReferentialAction.Cascade);
                    });

                migrationBuilder.CreateIndex(
                    name: "IX_Action_ActivityId",
                    table: "Action",
                    column: "ActivityId");

                migrationBuilder.CreateIndex(
                    name: "IX_Activity_Activity_Id",
                    table: "Activity",
                    column: "Activity_Id");

                migrationBuilder.CreateIndex(
                    name: "IX_EnterpriseTerm_ActivityId",
                    table: "EnterpriseTerm",
                    column: "ActivityId");

                migrationBuilder.CreateIndex(
                    name: "IX_LawReference_ActivityId",
                    table: "LawReference",
                    column: "ActivityId");
            }
        }

        private bool Exists(string tableName)
        {
            var options = new DbContextOptionsBuilder();
            options.UseSqlServer(DbConnectionstringProvider.ConnectionString);
            using (var context = new ApplicationDbContext(options.Options))
            {
                var count = context.Set<IntReturn>().FromSqlRaw("SELECT COUNT(OBJECT_ID(@p0, 'U')) as Value", tableName).AsEnumerable().First();
                return count.Value > 0;
            }
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Action");

            migrationBuilder.DropTable(
                name: "EnterpriseTerm");

            migrationBuilder.DropTable(
                name: "LawReference");

            migrationBuilder.DropTable(
                name: "Activity");
        }
    }
}
