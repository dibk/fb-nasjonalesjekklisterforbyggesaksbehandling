﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class CreateStaticMetadata : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "StaticMetadataId",
                table: "Facets",
                nullable: true,
                defaultValue: null);

            migrationBuilder.CreateTable(
                name: "StaticMetadata",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StaticMetadata", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Facets_StaticMetadataId",
                table: "Facets",
                column: "StaticMetadataId");

            migrationBuilder.AddForeignKey(
                name: "FK_Facets_StaticMetadata_StaticMetadataId",
                table: "Facets",
                column: "StaticMetadataId",
                principalTable: "StaticMetadata",
                principalColumn: "Id");

            string[] columns = {"Name" };
            

            // Legg til default verdier

            object[] processCategory = {"Søknadstype" };
            migrationBuilder.InsertData("StaticMetadata", columns, processCategory);

            object[] milestone = {"Milepel" };
            migrationBuilder.InsertData("StaticMetadata", columns, milestone);

            object[] activityType = {"Type sjekk" };
            migrationBuilder.InsertData("StaticMetadata", columns, activityType);

            object[] category = {"Tema" };
            migrationBuilder.InsertData("StaticMetadata", columns, category);

            object[] enterpriceTerm = {"Tiltakstype" };
            migrationBuilder.InsertData("StaticMetadata", columns, enterpriceTerm);

            object[] OwnerTerm = { "Eier" };
            migrationBuilder.InsertData("StaticMetadata", columns, OwnerTerm);


            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 1 WHERE Facets.Discriminator = 'ProcessCategory'");
            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 2 WHERE Facets.Discriminator = 'Milestone'");
            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 3 WHERE Facets.Discriminator = 'ActivityType'");
            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 4 WHERE Facets.Discriminator = 'Category'");
            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 5 WHERE Facets.Discriminator = 'EnterpriseTermFacet'");
            migrationBuilder.Sql("UPDATE Facets SET Facets.StaticMetadataId = 6 WHERE Facets.Discriminator = 'Owner'");


        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Facets_StaticMetadata_StaticMetadataId",
                table: "Facets");

            migrationBuilder.DropTable(
                name: "StaticMetadata");

            migrationBuilder.DropIndex(
                name: "IX_Facets_StaticMetadataId",
                table: "Facets");

            migrationBuilder.DropColumn(
                name: "StaticMetadataId",
                table: "Facets");


        }
    }
}
