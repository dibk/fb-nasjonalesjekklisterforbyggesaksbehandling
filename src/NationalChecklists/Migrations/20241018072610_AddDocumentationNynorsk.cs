﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace NationalChecklists.Migrations
{
    public partial class AddDocumentationNynorsk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DocumentationNynorsk",
                table: "Activity",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DocumentationNynorsk",
                table: "Activity");
        }
    }
}
