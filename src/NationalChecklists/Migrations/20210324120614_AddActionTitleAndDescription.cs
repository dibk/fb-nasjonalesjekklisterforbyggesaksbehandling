﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddActionTitleAndDescription : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionNb",
                table: "Action",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DescriptionNn",
                table: "Action",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TitleNb",
                table: "Action",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TitleNn",
                table: "Action",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DescriptionNb",
                table: "Action");

            migrationBuilder.DropColumn(
                name: "DescriptionNn",
                table: "Action");

            migrationBuilder.DropColumn(
                name: "TitleNb",
                table: "Action");

            migrationBuilder.DropColumn(
                name: "TitleNn",
                table: "Action");
        }
    }
}
