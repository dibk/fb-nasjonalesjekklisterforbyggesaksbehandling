﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NationalChecklists.Migrations
{
    public partial class AddSupportingNynorsk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DescriptionNynorsk",
                table: "Activity",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NameNynorsk",
                table: "Activity",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "IntReturn",
                columns: table => new
                {
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "IntReturn");

            migrationBuilder.DropColumn(
                name: "DescriptionNynorsk",
                table: "Activity");

            migrationBuilder.DropColumn(
                name: "NameNynorsk",
                table: "Activity");
        }
    }
}
