﻿using NationalChecklists.Middleware.Models;
using NationalChecklists.Middleware.Models.Metadata;

namespace NationalChecklists.Utils
{
    public class FilterDataForChecklist
    {
        private List<StaticMetadata> _staticMetadatas;
        private List<MetadataType> _dynamicmetadata;

        public FilterTypeForChecklist ProcessCategories => GetProcessCategories();
        public FilterTypeForChecklist Milestone => GetMilestones();
        public FilterTypeForChecklist ActivityTypes => GetActivityTypes();
        public FilterTypeForChecklist Categories => GetCategories();
        public FilterTypeForChecklist EnterpriseTerms => GetEnterpriseTerms();
        public FilterTypeForChecklist Owners => GetOwners();
        public List<FilterTypeForChecklist> DynamicMetadataFilter => GetDynamicMetadataFilters();
        public List<FilterTypeForChecklist> FiltersData => GetFilters();
       


        public FilterDataForChecklist(List<StaticMetadata> staticMetadatas, List<MetadataType> dynamicmetadata = null)
        {
            _staticMetadatas = staticMetadatas;
            _dynamicmetadata = dynamicmetadata;
        }


        private FilterTypeForChecklist GetEnterpriseTerms()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 5);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "EnterpriseTerms");
            return null;
        }

        private FilterTypeForChecklist GetActivityTypes()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 3);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "ActivityType");
            return null;
        }

        private FilterTypeForChecklist GetMilestones()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 2);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "Milestone");
            return null;
        }

        private FilterTypeForChecklist GetCategories()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 4);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "Category");
            return null;
        }

        private FilterTypeForChecklist GetProcessCategories()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 1);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "ProcessCategory");
            return null;
        }

        private FilterTypeForChecklist GetOwners()
        {
            var staticMetadataType = _staticMetadatas?.FirstOrDefault(s => s.Id == 6);
            if (staticMetadataType != null)
                return new FilterTypeForChecklist(staticMetadataType, "Owner");
            return null;
        }


        private List<FilterTypeForChecklist> GetFilters()
        {
            var filterValues = new List<FilterTypeForChecklist>()
            {
                Milestone,
                ActivityTypes,
                Categories,
                EnterpriseTerms,
                Owners
            };

            filterValues.AddRange(DynamicMetadataFilter);

            return filterValues.Where(f => f != null).OrderBy(o => o.OrderNumber).ToList();
        }

        private List<FilterTypeForChecklist> GetDynamicMetadataFilters()
        {
            var dynamicMetadataFilter = new List<FilterTypeForChecklist>();
            if (_dynamicmetadata != null)
                foreach (var metadataType in _dynamicmetadata) {
                    if (metadataType.Facet)
                    {
                        dynamicMetadataFilter.Add(new FilterTypeForChecklist(metadataType));
                    }
                }
                           
            return dynamicMetadataFilter.Where(f => f != null).OrderBy(o => o.OrderNumber).ToList();
        }
    }

    public class FilterTypeForChecklist
    {
        public string ModelName { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public bool ShowAsFacetFilter { get; set; }
        public bool ShowOnDetailPage { get; set; }

        public Dictionary<string, FilterValuesForChecklist> Filters { get; set; }


        public FilterTypeForChecklist()
        {

        }
        public FilterTypeForChecklist(StaticMetadata staticMetadata, string modelName)
        {
            ModelName = modelName;
            Name = staticMetadata.Name;
            Description = staticMetadata.Description;
            OrderNumber = staticMetadata.OrderNumber;
            ShowAsFacetFilter = staticMetadata.ShowAsFacetFilter;
            ShowOnDetailPage = staticMetadata.ShowOnDetailPage;

            var filterValues = new Dictionary<string, FilterValuesForChecklist>();

            if (staticMetadata.Facets != null)
                foreach (var facet in staticMetadata.Facets)
                {
                    filterValues.Add(facet.Id, new FilterValuesForChecklist()
                    {
                        Name = facet.Description,
                        Description = facet.Text,
                        Id = facet.Id,
                        Filters = new Dictionary<string, FilterValuesForChecklist>(),
                        OrderNumber = facet.OrderNumber,
                    });
                }

            Filters = filterValues;
        }

        public FilterTypeForChecklist(MetadataType metadataType)
        {
            ModelName = "metadataId";
            Name = metadataType.Name;
            Description = String.Empty;
            OrderNumber = metadataType.OrderNumber != null ? metadataType.OrderNumber.Value : 0;
            ShowAsFacetFilter = metadataType.Facet;
            ShowOnDetailPage = true;

            var filterValues = new Dictionary<string, FilterValuesForChecklist>();

            if (metadataType.MetadataValues != null)
                foreach (var facet in metadataType.MetadataValues)
                {
                    filterValues.Add(facet.Id.ToString(), new FilterValuesForChecklist()
                    {
                        Name = facet.Value,
                        Id = facet.Id.ToString(),
                        Description = facet.MetadataCode,
                        Filters = new Dictionary<string, FilterValuesForChecklist>(),
                        OrderNumber = facet.OrderNumber ?? 0,
                    });
                }

            Filters = filterValues;
        }

    }

    public class FilterValuesForChecklist
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int OrderNumber { get; set; }
        public Dictionary<string, FilterValuesForChecklist> Filters { get; set; }
    }
}
