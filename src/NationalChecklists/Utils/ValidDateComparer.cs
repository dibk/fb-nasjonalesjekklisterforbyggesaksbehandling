﻿namespace NationalChecklists.Utils
{
    public static class ValidDateComparer
    {
        public static bool IsValidOnDate(DateTime validDate, DateTime? activityValidFromDate, DateTime? activityValidToDate)
        {
            // Compare date 
            // 0 = Selected date
            // 0 < = earlier than selected date
            // 0 > = later than selected date
            var valueValidFrom = activityValidFromDate == null ? 0 : DateTime.Compare(validDate.Date, activityValidFromDate.Value.Date);
            var valueValidTo = activityValidToDate == null ? 0 : DateTime.Compare(validDate.Date, activityValidToDate.Value.Date);

            var isValidOnDate = false;

            if (activityValidToDate == null)
            {
                if (activityValidFromDate == null)
                {
                    isValidOnDate = true;
                }
                else
                {
                    if (valueValidFrom >= 0)
                    {
                        isValidOnDate = true;
                    }
                }
            }
            else
            {
                if (activityValidFromDate == null)
                {
                    if (valueValidTo <= 0)
                    {
                        isValidOnDate = true;
                    }
                }
                else
                {
                    if (valueValidFrom >= 0 && valueValidTo <= 0)
                    {
                        isValidOnDate = true;
                    }
                }
            }

            return isValidOnDate;
        }
    }
}
