using Microsoft.EntityFrameworkCore;
using NationalChecklists.Middleware.Models;
using NationalChecklists.Middleware.Models.Metadata;
using NationalChecklists.Models;
using NationalChecklists.Models.Facets;
using EnterpriseTerm = NationalChecklists.Models.EnterpriseTerm;

namespace NationalChecklists
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Activity> Activity { get; set; } = null!;
        public DbSet<Models.Action> Action { get; set; } = null!;
        public DbSet<LawReference> LawReference { get; set; } = null!;
        public DbSet<EnterpriseTerm> EnterpriseTerm { get; set; } = null!;
        public DbSet<Facet> Facets{ get; set; } = null!;
        public DbSet<ProcessCategory> ProcessCategories { get; set; } = null!;
        public DbSet<Milestone> Milestones { get; set; } = null!;
        public DbSet<Owner> Owners { get; set; } = null!;
        public DbSet<Models.Facets.ActivityType> ActivityTypes { get; set; } = null!;
        public DbSet<Category> Categories { get; set; } = null!;
        public DbSet<EnterpriseTermFacet> EnterpriseTerms { get; set; } = null!;
        public DbSet<MetadataType> MetadataTypes { get; set; } = null!;
        public DbSet<Metadata> Metadatas { get; set; } = null!;
        public DbSet<ActivityMetadata> ActivityMetadatas { get; set; } = null!;
        public DbSet<StaticMetadata> StaticMetadata { get; set; } = null!;
        public DbSet<ActionValidationRules> ActionValidationRules { get; set; } = null!;

        protected ApplicationDbContext()
        {
        }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Activity>().HasIndex(c => new { c.Processcategory, c.Status });

            modelBuilder.Entity<IntReturn>().HasNoKey();

            modelBuilder.Entity<Activity>().HasMany(a => a.Actions)
                .WithOne(action => action.Activity)
                .HasForeignKey(action => action.ActivityId);

            modelBuilder.Entity<Activity>().HasMany(a => a.LawReferences)
                .WithOne(lr => lr.Activity)
                .HasForeignKey(lr=> lr.ActivityId);

            modelBuilder.Entity<Activity>().HasMany(a => a.EnterpriseTerms)
                .WithOne(et => et.Activity)
                .HasForeignKey(et => et.ActivityId);

            modelBuilder.Entity<Activity>().HasMany(a => a.SubActivities)
                .WithOne(sa => sa.ParentActivity)
                .HasForeignKey(sa => sa.Activity_Id);

            modelBuilder.Entity<StaticMetadata>().HasMany(a => a.Facets)
                .WithOne(f => f.StaticMetadata)
                .HasForeignKey(f => f.StaticMetadataId);
                ;
            modelBuilder.Entity<MetadataType>().HasMany(a => a.MetadataValues);

            modelBuilder.Entity<ActivityMetadata>().HasKey(sc => new { sc.ActivityId, sc.MetadataId });
            modelBuilder.Entity<ActivityMetadata>().HasOne(a => a.MetadataItem);
            modelBuilder.Entity<Activity>().HasMany(a => a.Metadata);

            base.OnModelCreating(modelBuilder);


        }

        public T DetachEntity<T>(T entity) where T : class
        {
            Entry(entity).State = EntityState.Detached;
            if (entity.GetType().GetProperty("Id") != null)
            {
                entity.GetType().GetProperty("Id")?.SetValue(entity, 0);
            }
            if (entity.GetType().GetProperty("ActivityId") != null)
            {
                entity.GetType().GetProperty("ActivityId")?.SetValue(entity, 0);
            }
            return entity;
        }

        public List<T> DetachEntities<T>(List<T> entities) where T : class
        {
            foreach (var entity in entities)
            {
                this.DetachEntity(entity);
            }
            return entities;
        }

        public Task<int> SaveChanges(CancellationToken cancellationToken = new CancellationToken())
        {
            return base.SaveChangesAsync(cancellationToken);
        }

    }

    public class IntReturn
    {
        public int Value { get; set; }
}

}