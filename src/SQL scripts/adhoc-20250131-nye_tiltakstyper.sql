BEGIN TRY
    BEGIN TRANSACTION

    SELECT DISTINCT (ReferenceId)
    INTO #tempActivities
    FROM Activity a
    JOIN EnterpriseTerm e
        ON a.id = e.ActivityId
    WHERE a.Processcategory = 'ET'
        AND e.Code IN ('endringVAanlegg', 'endringVeianlegg', 'nyttByggBlandetFormalNaringBolig', 'nyttVAanlegg')
        AND a.[Status] = 1
    ORDER BY 1;

    DECLARE activityCur CURSOR
    FOR
    SELECT ReferenceId
    FROM #tempActivities;

    DECLARE @referenceId VARCHAR(10);

    OPEN activityCur;

    FETCH NEXT
    FROM activityCur
    INTO @referenceId;

    WHILE @@FETCH_STATUS = 0
    BEGIN
        DECLARE @newActivityId INT;

        PRINT 'Inserts new row for action';

        IF EXISTS (
                SELECT 1
                FROM Activity
                WHERE ReferenceId = @referenceId
                    AND Processcategory = 'RS'
                    AND [Status] = 1
                )
        BEGIN
            EXEC sp_copy_activity @activityRefid = @referenceId
                , @processCategory = 'RS';

            SET @newActivityId = (
                    SELECT Id
                    FROM Activity a
                    WHERE a.ReferenceId = @referenceId
                        AND a.Processcategory = 'RS'
                        AND a.[Status] = 1
                    );

            --Legger til nye tiltakstyper
            INSERT INTO EnterpriseTerm (
                Code
                , ActivityId
                )
            VALUES (
                'endringVAanlegg'
                , @newActivityId
                );

            INSERT INTO EnterpriseTerm (
                Code
                , ActivityId
                )
            VALUES (
                'endringVeianlegg'
                , @newActivityId
                );

            INSERT INTO EnterpriseTerm (
                Code
                , ActivityId
                )
            VALUES (
                'nyttByggBlandetFormalNaringBolig'
                , @newActivityId
                );

            INSERT INTO EnterpriseTerm (
                Code
                , ActivityId
                )
            VALUES (
                'nyttVAanlegg'
                , @newActivityId
                );
        END

        FETCH NEXT
        FROM activityCur
        INTO @referenceId;

        PRINT 'Fetched new row';
    END

    CLOSE activityCur;

    DEALLOCATE activityCur;

    --ROLLBACK TRANSACTION;
    COMMIT TRANSACTION;-- Save changes
END TRY

BEGIN CATCH
    ROLLBACK TRANSACTION;-- Undo changes if an error occurs

    CLOSE activityCur;

    DEALLOCATE activityCur;

    -- Optionally, log the error details
    PRINT 'Error: ' + ERROR_MESSAGE();
END CATCH
GO
