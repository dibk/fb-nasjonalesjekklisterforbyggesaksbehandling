CREATE
    OR

ALTER PROC sp_copy_activity @activityRefid VARCHAR(10)
    , @processCategory VARCHAR(5)
AS
DECLARE @originActivityId INT
DECLARE @newActivityId INT

BEGIN TRY
    -- TEMP LØSNING FOR Å KOPIERE EN AKTIVITET OG LEGGE TIL NYE TILTAKSTYPER
    -- Brukes slik som dette: EXECUTE sp_copy_activity @activityRefid='1.1',@processCategory='RS'
    --------------------------------------------------------------------------
    -- Henter ut sjekk punkt som skal kopieres, det skal kun eksistere ei rad
    PRINT '----------------------------------------------------'
    PRINT CONCAT (
            '----------- Starts copying '
            , @activityRefid
            , ' for '
            , @processCategory
            , ' ------------'
            )
    PRINT '----------------------------------------------------'

    SET @originActivityId = (
            SELECT id
            FROM Activity a
            WHERE a.ReferenceId = @activityRefid
                AND a.Processcategory = @processCategory
                AND a.STATUS = 1
            );

    PRINT CONCAT (
            'RowId for '
            , @activityRefid
            , ' is '
            , @originActivityId
            );

    SELECT [ReferenceId]
        , [ParentReferenceId]
        , [Municipality]
        , [Name]
        , [ActivityType]
        , [Description]
        , [Category]
        , [Processcategory]
        , [Milestone]
        , [Status]
        , [HasRule]
        , [Rule]
        , [Activity_Id]
        , [PublishedActivity_Id]
        , [ValidFrom]
        , [ValidTo]
        , [Updated]
        , [OrderNumber]
        , [DescriptionNynorsk]
        , [NameNynorsk]
        , [Documentation]
        , [Image]
        , [ImageMimeType]
        , [ImageDescription]
        , [DocumentationNynorsk]
    INTO #tempActivity
    FROM Activity a
    WHERE a.Id = @originActivityId;

    --Forbereder insert
    UPDATE #tempActivity
    SET STATUS = 1
        , Updated = GETDATE();

    INSERT INTO Activity
    SELECT *
    FROM #tempActivity;

    --Hente ny rad sin id
    SET @newActivityId = SCOPE_IDENTITY();

    PRINT CONCAT (
            'Added new activity row with id: '
            , @newActivityId
            );

    --Oppdatere forrige versjon
    UPDATE Activity
    SET STATUS = 2
        , updated = GETDATE()
        , PublishedActivity_Id = @newActivityId
    WHERE id = @originActivityId;

    PRINT CONCAT (
            'Updating old row with id: '
            , @originActivityId
            , ' with status 2 (archived)'
            );

    -- Må oppdatere alle under-sjekkpunkt med ny parent
    DECLARE @rowsWithParent INT;

    SET @rowsWithParent = (
            SELECT COUNT(*)
            FROM Activity a
            WHERE a.Activity_Id = @originActivityId
            );

    PRINT CONCAT (
            @rowsWithParent
            , ' activites has '
            , @originActivityId
            , ' as parent'
            );

    IF (@rowsWithParent > 0)
    BEGIN
        PRINT 'Updates children with new parent (adoption?)';

        UPDATE Activity
        SET Activity_Id = @newActivityId
        WHERE Activity_Id = @originActivityId;
    END;

    -- Henter ut tiltakstyper for sjekkpunktet
    PRINT '******************** EnterpriseTerm ********************';

    SELECT Code
        , ActivityId
    INTO #tempEnterpriseTerms
    FROM EnterpriseTerm e
    WHERE e.ActivityId = @originActivityId;

    --Flytter over til ny activity
    UPDATE #tempEnterpriseTerms
    SET ActivityId = @newActivityId;

    --Klargjøring før insert
    INSERT INTO EnterpriseTerm
    SELECT *
    FROM #tempEnterpriseTerms;

    PRINT 'Added enterpriseterm for new activity'
    -- Andre tabeller;
    ---- ActivityMetadatas
    PRINT '******************** ActivityMetadata ********************';

    SELECT 0 AS ActivityId
        , MetadataId
        , MetadataTypeId
    INTO #tempActivityMetadatas
    FROM ActivityMetadatas
    WHERE ActivityId = @originActivityId;

    IF (
            (
                SELECT count(*)
                FROM #tempActivityMetadatas
                ) > 0
            )
    BEGIN
        UPDATE #tempActivityMetadatas
        SET ActivityId = @newActivityId;

        INSERT INTO ActivityMetadatas
        SELECT *
        FROM #tempActivityMetadatas;

        PRINT CONCAT (
                'Copied and added ActivityMetadatas for new activity id: '
                , @newActivityId
                );
    END;
    ELSE
        PRINT CONCAT (
                'No ActivityMetadatas for id: '
                , @originActivityId
                );

    --- LawReference
    PRINT '******************** LawReference ********************';

    SELECT LawReferenceDescription
        , LawReferenceUrl
        , ActivityId
    INTO #tempLawReference
    FROM LawReference
    WHERE ActivityId = @originActivityId;

    IF (
            (
                SELECT count(*)
                FROM #tempLawReference
                ) > 0
            )
    BEGIN
        UPDATE #tempLawReference
        SET ActivityId = @newActivityId;

        INSERT INTO LawReference
        SELECT *
        FROM #tempLawReference;

        PRINT CONCAT (
                'Copied and added LawReference for id: '
                , @newActivityId
                );
    END;
    ELSE
        PRINT CONCAT (
                'No LawReference for id: '
                , @originActivityId
                );

    --- Action
    BEGIN
        PRINT '******************** ACTIONS ********************';
        PRINT CONCAT (
                'Handles Actions for id: '
                , @originActivityId
                );

        SELECT [Id]
            , [ActionValue]
            , [ActionType]
            , [ActionTypeCode]
            , [ActivityId]
            , [DescriptionNb]
            , [DescriptionNn]
            , [TitleNb]
            , [TitleNn]
            , [ContentType]
            , [PublishedAction_Id]
        INTO #tempAction
        FROM [Action]
        WHERE ActivityId = @originActivityId;

        IF (
                (
                    SELECT count(*)
                    FROM #tempAction
                    ) > 0
                )
        BEGIN
            DECLARE @id INT
                , @actionValue BIT
                , @actionType NVARCHAR(max)
                , @actionTypeCode NVARCHAR(max)
                , @activityId INT
                , @descriptionNb NVARCHAR(max)
                , @descriptionNn NVARCHAR(max)
                , @titleNb NVARCHAR(max)
                , @titleNn NVARCHAR(max)
                , @contentType NVARCHAR(max)
                , @publishedAction_Id INT

            DECLARE actionCursor CURSOR
            FOR
            SELECT [Id]
                , [ActionValue]
                , [ActionType]
                , [ActionTypeCode]
                , [ActivityId]
                , [DescriptionNb]
                , [DescriptionNn]
                , [TitleNb]
                , [TitleNn]
                , [ContentType]
                , [PublishedAction_Id]
            FROM #tempAction;

            OPEN actionCursor

            FETCH NEXT
            FROM actionCursor
            INTO @id
                , @actionValue
                , @actionType
                , @actionTypeCode
                , @activityId
                , @descriptionNb
                , @descriptionNn
                , @titleNb
                , @titleNn
                , @contentType
                , @publishedAction_Id

            WHILE @@FETCH_STATUS = 0
            BEGIN
                DECLARE @tempIdTable TABLE (ID INT);
                DECLARE @newActionId INT;

                PRINT 'Inserts new row for action'

                -- Insert ny rad for action
                INSERT INTO [Action] (
                    [ActionValue]
                    , [ActionType]
                    , [ActionTypeCode]
                    , [ActivityId]
                    , [DescriptionNb]
                    , [DescriptionNn]
                    , [TitleNb]
                    , [TitleNn]
                    , [ContentType]
                    , [PublishedAction_Id]
                    )
                VALUES (
                    @actionValue
                    , @actionType
                    , @actionTypeCode
                    , @newActivityId
                    , @descriptionNb
                    , @descriptionNn
                    , @titleNb
                    , @titleNn
                    , @contentType
                    , null
                    );

                -- Hent ID for action-raden            
                SET @newActionId = SCOPE_IDENTITY();

                PRINT CONCAT (
                        'Updates exising action with publishedAction_Id: '
                        , @newActionId
                        );

                UPDATE Action
                SET PublishedAction_Id = @newActionId
                WHERE id = @id;

                PRINT 'New actionId: ' + CAST(@newActionId AS NVARCHAR);

                -- Dersom det er koblet på en validation rule må denne også kopieres
                DECLARE @currentAVR INT;

                SET @currentAVR = (
                        SELECT r.Id
                        FROM ActionValidationRules r
                        WHERE r.ActionId = @id
                        );

                IF (@currentAVR > 0)
                BEGIN
                    PRINT 'Action has validation rules';

                    DECLARE @NewAVRId INT;

                    INSERT INTO ActionValidationRules (ActionId)
                    VALUES (@newActionId);

                    SET @NewAVRId = SCOPE_IDENTITY();

                    SELECT ValidationRuleId
                        , 0 AS ActionValidationRulesId
                    INTO #tmpValidationRules
                    FROM ValidationRule v
                    WHERE v.ActionValidationRulesId = @currentAVR;

                    UPDATE #tmpValidationRules
                    SET ActionValidationRulesId = @NewAVRId;

                    INSERT INTO ValidationRule
                    SELECT *
                    FROM #tmpValidationRules;
                END

                PRINT CONCAT (
                        'Finished action '
                        , @newActionId
                        );

                FETCH NEXT
                FROM actionCursor
                INTO @id
                    , @actionValue
                    , @actionType
                    , @actionTypeCode
                    , @activityId
                    , @descriptionNb
                    , @descriptionNn
                    , @titleNb
                    , @titleNn
                    , @contentType
                    , @publishedAction_Id

                PRINT 'Fetched new row';
            END

            CLOSE actionCursor;

            DEALLOCATE actionCursor;

            PRINT 'Finished handling actions';
        END;
        ELSE
            PRINT CONCAT (
                    'Ingen Action for '
                    , @activityRefid
                    );
    END

    PRINT '----------------------------------------------------'
    PRINT CONCAT (
            '----------- Finished copying '
            , @activityRefid
            , ' for '
            , @processCategory
            , ' ------------'
            )
    PRINT '----------------------------------------------------'
    PRINT ''
    PRINT ''
END TRY

BEGIN CATCH
    ROLLBACK TRANSACTION;-- Undo changes if an error occurs

    CLOSE activityCur;

    DEALLOCATE activityCur;

    PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
    PRINT CONCAT (
            'Failed for '
            , @activityRefid
            , ' for '
            , @processCategory
            );
    PRINT 'Error: ' + ERROR_MESSAGE();
    PRINT '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
END CATCH
GO


